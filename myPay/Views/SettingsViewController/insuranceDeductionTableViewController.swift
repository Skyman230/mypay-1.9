//
//  insuranceDeductionTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 6/4/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class insuranceDeductionTableViewController: UITableViewController {
    
    
    // MARK: - IBOutlets
    // MARK: UILabels
    @IBOutlet weak var infoTextLabel: UILabel!
    
    // MARK: UITextField
    @IBOutlet weak var insuranceAmountTextField: UITextField!
    
    // MARK: UIButtons
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    
    
    // MARK: - vars
    var itemToEdit: String?
    var itemToEditTitle: String?
    var item: String!
    var amount1ToEdit: String?
    var amount2ToEdit: String?
    var amount3ToEdit: String?
    var amount4ToEdit: String?
    var info: String?
    var whichDeduction: String?
    var insuranceAmount: String?

    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        infoTextLabel.text = info
        insuranceAmountTextField.becomeFirstResponder()
        if whichDeduction == "Medical" {
           insuranceAmountTextField.text = amount1ToEdit
            continueBtn.isHidden = true
            cancelBtn.isHidden = false
        }
        if whichDeduction == "Dental" {
            insuranceAmountTextField.text = amount3ToEdit
            continueBtn.isHidden = true
            cancelBtn.isHidden = false
        }
        if whichDeduction == "Medical Savings Acct" {
            insuranceAmountTextField.text = amount2ToEdit
            continueBtn.isHidden = true
            cancelBtn.isHidden = false
        }
        if whichDeduction == "Vision" {
            insuranceAmountTextField.text = amount4ToEdit
            continueBtn.isHidden = true
            cancelBtn.isHidden = false
        }
    }
    
    // MARK: - IBActions
    // MARK: UITextField
    @IBAction func insuranceAmountTextFieldEditingChanged(_ sender: UITextField) {
        
        if((sender.text?.count)! > 0 && !(sender.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            continueBtn.isHidden = false
            cancelBtn.isHidden = true
        }
        else{
            continueBtn.isHidden = true
            cancelBtn.isHidden = false
        }
    }
    
    // MARK: UIButton
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let Item = insuranceAmountTextField.text {
            item = Item
            let x = Double(item)
            let amount = convertDoubleToCurrency(amount: x!)
            insuranceAmount = amount
        }
        
    }
}
