//
//  editPositionTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 6/6/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class editPositionTableViewController: UIViewController {
    
    // MARK: - UIOutlet
    // MARK: UITableView
    @IBOutlet weak var positionTableView: UITableView!
    
    // MARK: UIView
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var footerContentView: UIView!
    @IBOutlet weak var backgroundView1: UIView!
    @IBOutlet weak var backgroundView2: UIView!
    @IBOutlet weak var backgroundView3: UIView!
    @IBOutlet weak var backgroundView4: UIView!
    @IBOutlet weak var backgroundView5: UIView!
    
    // MARK: UIButton
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    
    // MARK: - Var
    // MARK: String
    var selectedPosition: String?
    var selectedRate: String?
    var newPosition: String?
    var newRate: String?
    var positionNum: Int?
    var newPositionNum: Int?
    
    // MARK: Int
    var jobIndex = GlobalVariable.jobIndex
    var positionIndex = 0
    
    //MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()

        positionTableView.delegate = self
        positionTableView.dataSource = self
        changeBackgroundSize(whichBkg: Data.Jobs[jobIndex].positionModel.count)
    }
    
    // MARK: - func
    func changeBackgroundSize(whichBkg: Int) {
        if whichBkg <= 0 {
            backgroundView1.isHidden = false
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
        }
        if whichBkg == 1 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = false
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
        }
        if whichBkg == 2 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = false
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
        }
        if whichBkg == 3 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = false
            backgroundView5.isHidden = true
        }
        if whichBkg >= 4 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = false
        }
        
        
        
    }
    
    // MARK: - IBActions
    // MARK: Button
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func unwindFromEditShiftPositionVC(_ sender: UIStoryboardSegue) {
        if let editPositionVC = sender.source as? editShiftPositionViewController {
            let position = editPositionVC.newPosition
            let rate = editPositionVC.newRate
            PositionFunctions.updatePosition(at: positionIndex, position: position, rate: rate)
            positionTableView.reloadData()
        }
    }
    
    
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        newPositionNum = Data.Jobs[self.jobIndex].positionModel.count
        if let position = selectedPosition {
            newPosition = position
        }
        if segue.identifier == "toEditPositionCell" {
            if let editPositionVC = segue.destination as? editShiftPositionViewController {
                editPositionVC.shiftPosition = selectedPosition!
                let rate = convertCurrencyToDouble(input: selectedRate!)
                editPositionVC.shiftRate = "\(rate ?? 0)"
            }
        }
    }
}

extension editPositionTableViewController: UITableViewDelegate, UITableViewDataSource {

    
    // MARK: - TableView Data
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Data.Jobs[jobIndex].positionModel.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = positionTableView.dequeueReusableCell(withIdentifier: "jobPositionCell") as! PositionDetailCell
        let position = Data.Jobs[jobIndex].positionModel[indexPath.row].position
        let rate = Data.Jobs[jobIndex].positionModel[indexPath.row].hourlyRate
        
        cell.positionTextLabel.text = position
        cell.hourlyRateTextLabel.text = rate
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PositionDetailCell
        selectedPosition = cell.positionTextLabel.text
        selectedRate = cell.hourlyRateTextLabel.text
        positionIndex = indexPath.row
        performSegue(withIdentifier:"toEditPositionCell", sender: AnyClass.self)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (contextualAction, view, actionPerformed: (Bool) -> Void) in
            JobsFunctions.deletePosition(index: indexPath.row)
            tableView.reloadData()
            if Data.Jobs[self.jobIndex].positionModel.count == 0 {
                GlobalVariable.jobPositionArrayIsPopulated = false
            }
            actionPerformed(true)
            self.changeBackgroundSize(whichBkg: Data.Jobs[self.jobIndex].positionModel.count)
        }
        return UISwipeActionsConfiguration(actions: [delete])
    }
    

    // MARK: - Table view data source

    


    
}
