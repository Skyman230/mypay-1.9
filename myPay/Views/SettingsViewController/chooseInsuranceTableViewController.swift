//
//  chooseInsuranceTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 6/4/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class chooseInsuranceTableViewController: UITableViewController {
    
    var insuranceEditing = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    // MARK: - IBAction
    // MARK: Button
    @IBAction func insuranceMedicalBtnPressed(_ sender: UIButton) {
        insuranceEditing = 1
        GlobalVariable.insuranceEditing = 1
        performSegue(withIdentifier: "toAddInsuranceDeductionSegue", sender: Any?.self)
    }
    @IBAction func insuranceMedicalSavingsAccountPressed(_ sender: UIButton) {
        insuranceEditing = 2
        GlobalVariable.insuranceEditing = 2
        performSegue(withIdentifier: "toAddInsuranceDeductionSegue", sender: Any?.self)
    }
    @IBAction func insuranceDentalBtnPressed(_ sender: UIButton) {
        insuranceEditing = 3
        GlobalVariable.insuranceEditing = 3
        performSegue(withIdentifier: "toAddInsuranceDeductionSegue", sender: Any?.self)
    }
    @IBAction func insuranceVisionBtnPressed(_ sender: UIButton) {
        insuranceEditing = 4
        GlobalVariable.insuranceEditing = 4
        performSegue(withIdentifier: "toAddInsuranceDeductionSegue", sender: Any?.self)
    }
    
    
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddInsuranceDeductionSegue" {
            if let insuranceDeductionVC = segue.destination as? insuranceDeductionTableViewController {
                if insuranceEditing == 1 {
                    insuranceDeductionVC.info = "Enter Medical Insurance Amount"
                }
                if insuranceEditing == 2 {
                    insuranceDeductionVC.info = "Enter Amount to go in Medical Savings Acct"
                }
                if insuranceEditing == 3 {
                    insuranceDeductionVC.info = "Enter Dental Insurance Amount"
                }
                if insuranceEditing == 4 {
                    insuranceDeductionVC.info = "Enter Vision Insurance Amount"
                }
            }
        }
    }
}
