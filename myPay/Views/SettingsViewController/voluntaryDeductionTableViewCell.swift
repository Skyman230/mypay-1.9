//
//  voluntaryDeductionTableViewCell.swift
//  myPay
//
//  Created by Donald Scott on 6/5/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class voluntaryDeductionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var deductionNameTextLabel: UILabel!
    @IBOutlet weak var deductionAmountTextLabel: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
