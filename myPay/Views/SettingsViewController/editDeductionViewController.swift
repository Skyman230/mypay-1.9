//
//  editDeductionViewController.swift
//  myPay
//
//  Created by Donald Scott on 6/6/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class editDeductionViewController: UIViewController {
    
    // MARK: - UIOutlet
    // MARK: UITableView
    @IBOutlet weak var deductionTableView: UITableView!
    
    // MARK: UIView
    @IBOutlet weak var backgroundView1: UIView!
    @IBOutlet weak var backgroundView2: UIView!
    @IBOutlet weak var backgroundView3: UIView!
    @IBOutlet weak var backgroundView4: UIView!
    @IBOutlet weak var backgroundView5: UIView!
    @IBOutlet weak var backgroundView6: UIView!
    
    // MARK: UIButton
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    
    // MARK: - Var
    // MARK: String
    var selectedDeduction: String?
    var selectedAmount: String?
    var newDeduction: String?
    var newAmount: String?
    var deductionNum: Int?
    var newDeductionNum: Int?
    
    // MARK: Int
    var jobIndex = GlobalVariable.jobIndex
    var deductionIndex = 0
    
    //MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()

        deductionTableView.delegate = self
        deductionTableView.dataSource = self
        changeBackgroundSize(whichBkg: Data.Jobs[jobIndex].voluntaryDeductions.count)
    }
    
    // MARK: - func
    func changeBackgroundSize(whichBkg: Int) {
        if whichBkg <= 0 {
            backgroundView1.isHidden = false
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
            backgroundView6.isHidden = true
        }
        if whichBkg == 1 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = false
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
            backgroundView6.isHidden = true
        }
        if whichBkg == 2 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = false
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
            backgroundView6.isHidden = true
        }
        if whichBkg == 3 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = false
            backgroundView5.isHidden = true
            backgroundView6.isHidden = true
        }
        if whichBkg == 4 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = false
            backgroundView6.isHidden = true
        }
        if whichBkg >= 5 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
            backgroundView6.isHidden = false
        }
        
        
        
    }
    
    // MARK: - IBActions
    // MARK: Button
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func unwindFromEditJobDeductionVC(_ sender: UIStoryboardSegue) {
        if let editDeductionVC = sender.source as? editJobAdditionalDeductionViewController {
            let deduction = editDeductionVC.newDeduction
            let amount = editDeductionVC.newAmount
            DeductionFunction.updateDeduction(at: deductionIndex, deduction: deduction, amount: amount)
            deductionTableView.reloadData()
        }
    }
    

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        newDeductionNum = Data.Jobs[self.jobIndex].voluntaryDeductions.count
        if let deduction = selectedDeduction {
            newDeduction = deduction
        }
        if let amount = selectedAmount {
            newAmount = amount
        }
        if segue.identifier == "toEditJobDeductionsSegue" {
            if let editDeductionVC = segue.destination as? editJobAdditionalDeductionViewController {
                editDeductionVC.deduction = selectedDeduction!
                let amount = convertCurrencyToDouble(input: selectedAmount!)
                editDeductionVC.amount = "\(amount ?? 0)"
            }
        }
    }
    

}



extension editDeductionViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    // MARK: - TableView Data
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Data.Jobs[jobIndex].voluntaryDeductions.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = deductionTableView.dequeueReusableCell(withIdentifier: "DeductionCell") as! voluntaryDeductionTableViewCell
        let name = Data.Jobs[jobIndex].voluntaryDeductions[indexPath.row].Name
        let amount = Data.Jobs[jobIndex].voluntaryDeductions[indexPath.row].Amount
        
        cell.deductionNameTextLabel.text = name
        cell.deductionAmountTextLabel.text = amount
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! voluntaryDeductionTableViewCell
        selectedDeduction = cell.deductionNameTextLabel.text
        selectedAmount = cell.deductionAmountTextLabel.text
        deductionIndex = indexPath.row
        performSegue(withIdentifier:"toEditJobDeductionsSegue", sender: AnyClass.self)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (contextualAction, view, actionPerformed: (Bool) -> Void) in
            JobsFunctions.deleteDeduction(index: indexPath.row)
            tableView.reloadData()
            if Data.Jobs[self.jobIndex].voluntaryDeductions.count == 0 {
                GlobalVariable.DeductionArrayIsPopulated = false
            }
            actionPerformed(true)
            self.changeBackgroundSize(whichBkg: Data.Jobs[self.jobIndex].voluntaryDeductions.count)
        }
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    
    // MARK: - Table view data source
    
    
    
    
    
}
