//
//  401KTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 6/3/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class ret401KTableViewController: UITableViewController {

    // MARK: - IBOutlets
    // MARK: UITextField
    @IBOutlet weak var percentageTextField: UITextField!
    
    // MARK: Button
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var addPercentageBtn: UIButton!
    
    // MARK: - vars
    var deductionPercentage: String!
    var deductionAmount: String?
    
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        percentageTextField.becomeFirstResponder()
        
    }
    
    // MARK: - IBActions
    // MARK: Button
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true)
    }
    
    // MARK: TextFields
    @IBAction func percentageTextFieldEditingChanged(_ sender: UITextField) {
        if((sender.text?.count)! > 0 && !(sender.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            addPercentageBtn.isHidden = false
        }
        else{
           addPercentageBtn.isHidden = true
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let Percentage = (percentageTextField.text! as NSString).doubleValue
        let deductPercentage = String(format: "%.2f", Percentage/100)
        print(deductPercentage)
        let percent = (deductPercentage as NSString).doubleValue
        let grossWage = GlobalVariable.GrossWage
        let wage = convertCurrencyToDouble(input: grossWage!)
        
        let entry = percentageEntry(percentage: percent, grossWages: wage!)
        let Amount = retirement401KPercentage.percentageEntry(entry: entry)
        GlobalVariable.retirement401KAmount = convertDoubleToCurrency(amount: Amount)
        print(GlobalVariable.retirement401KAmount)
        deductionPercentage = percentageTextField.text
        GlobalVariable.deductionPercentage = percentageTextField.text
        deductionAmount = convertDoubleToCurrency(amount: Amount)
        GlobalVariable.retirement401KPercentage = deductPercentage
        CalculateSums.showFigures()
        
    }
}
