//
//  editShiftPositionViewController.swift
//  myPay
//
//  Created by Donald Scott on 6/6/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class editShiftPositionViewController: UITableViewController {
    
    // MARK: - UIOutlet
    // MARK: UITextField
    @IBOutlet weak var positionTextField: UITextField!
    @IBOutlet weak var hourlyRateTextField: UITextField!
    
    
    // MARK: - var
    // MARK: String
    var shiftRate = String()
    var shiftPosition = String()
    var newPosition = String()
    var newRate = String()

    // MARK: - load View
    override func viewDidLoad() {
        super.viewDidLoad()

        positionTextField.text = shiftPosition
        hourlyRateTextField.text = shiftRate
        positionTextField.becomeFirstResponder()
    }
    
    
    // MARK: - IBActions
    // MARK: Button
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        newPosition = positionTextField.text!
        let rate = (hourlyRateTextField.text! as NSString).doubleValue
        newRate = convertDoubleToCurrency(amount: rate)
        
    }
    
}
