//
//  addDeductionsViewController.swift
//  myPay
//
//  Created by Donald Scott on 6/5/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class addDeductionsViewController: UITableViewController {
    
    // MARK: - UIOutlet
    // MARK: UITextField
    @IBOutlet weak var deductionNameTextField: UITextField!
    @IBOutlet weak var deductionAmountTextField: UITextField!
    
    // MARK: UIButton
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    
    // MARK: - Var
    var jobID = GlobalVariable.UniqueID
    var jobModel: JobsModel?
    
    var doneSaving: (() -> ())?
    var jobIndex = GlobalVariable.jobIndex
    var deductionName = String()
    var deductionAmount = String()
    
    var deductionNameTextFieldPopulated = false
    var deductionAmountTextFieldPopulated = false
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()

        JobsFunctions.readJob(by: jobID) { [weak self](model) in
            guard let self = self else { return }
            self.jobModel = model
        }
        deductionNameTextField.becomeFirstResponder()
        continueBtn.isHidden = true
    }
    
    // MARK: - func
    func toggleContinueBtn() {
        if deductionNameTextFieldPopulated == true {
            if deductionAmountTextFieldPopulated == true{
                continueBtn.isHidden = false
            } else {
                continueBtn.isHidden = true
            }
        } else {
            continueBtn.isHidden = true
        }
        
    }
    
    // MARK: - IBActions
    // MARK: Textfields
    @IBAction func deductionNameTextFieldEditingChanged(_ sender: UITextField) {
        if((sender.text?.count)! > 0 && !(sender.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            self.deductionNameTextFieldPopulated = true
            toggleContinueBtn()
        }
        else{
            self.deductionNameTextFieldPopulated = false
            toggleContinueBtn()
        }
    }
    
    @IBAction func deductionAmountTextFieldEditingChanged(_ sender: UITextField) {
        if((sender.text?.count)! > 0 && !(sender.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            self.deductionAmountTextFieldPopulated = true
            toggleContinueBtn()
        }
        else{
            self.deductionAmountTextFieldPopulated = false
            toggleContinueBtn()
        }
    }
    
    // MARK: Buttons
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true)
        
    }
    @IBAction func saveBtnPressed(_ sender: UIButton) {if let doneSaving = doneSaving {
        let newDeduction = deductionNameTextField.text
        let newAmount = deductionAmountTextField.text
        
        newDeductionName = newDeduction
        let deductName = newDeduction!
        let Amount = ("$" + newAmount!)
        newDeductionAmount = Amount
        let deductAmount = Amount
        DeductionFunction.createDeductions(at: jobIndex, using: DeductionModel(Name: deductName, Amount: deductAmount))
        
        doneSaving()
        
        }
        GlobalVariable.voluntaryDeductionArrayIsPopulated = true
        dismiss(animated: true)
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
