//
//  editJobAdditionalDeductionViewController.swift
//  myPay
//
//  Created by Donald Scott on 6/7/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class editJobAdditionalDeductionViewController: UITableViewController {
    
    // MARK: - UIOutlet
    // MARK: UITextField
    @IBOutlet weak var deductionNameTextField: UITextField!
    @IBOutlet weak var deductionAmountRextField: UITextField!
    
    // MARK: UIButton
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    
    // MARK: - var
    // MARK: String
    var deduction = String()
    var amount = String()
    var newDeduction = String()
    var newAmount = String()
    
    // MARK: - load View
    override func viewDidLoad() {
        super.viewDidLoad()

        deductionNameTextField.text = deduction
        deductionAmountRextField.text = amount
        deductionNameTextField.becomeFirstResponder()
    }
    
    // MARK: - IBActions
    // MARK: Button
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        newDeduction = deductionNameTextField.text!
        let Amount = (deductionAmountRextField.text! as NSString).doubleValue
        newAmount = convertDoubleToCurrency(amount: Amount)
    }
    
    
}
