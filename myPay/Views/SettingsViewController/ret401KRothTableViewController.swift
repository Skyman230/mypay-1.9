//
//  ret401KRothTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 6/4/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class ret401KRothTableViewController: UITableViewController {
    
    // MARK: - IBOutlets
    // MARK: UITextField
    @IBOutlet weak var percentageTextField: UITextField!
    
    // MARK: Button
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var addPercentageBtn: UIButton!
    
    // MARK: - vars
    var deductionRothPercentage: String!
    var deductionRothAmount: String!
    
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        percentageTextField.becomeFirstResponder()

    }
    
    // MARK: - IBActions
    // MARK: Button
    @IBAction func cancelBtnPressed(_ sender: Any) {
        dismiss(animated: true)
    }
    
    // MARK: TextFields
    @IBAction func percentageTextFieldEditingChanged(_ sender: UITextField) {
        if((sender.text?.count)! > 0 && !(sender.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            addPercentageBtn.isHidden = false
        }
        else{
            addPercentageBtn.isHidden = true
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let Percentage = (percentageTextField.text! as NSString).doubleValue
        let deductPercentage = String(format: "%.2f", Percentage/100)
        let percent = (deductPercentage as NSString).doubleValue
        let grossWage = GlobalVariable.GrossWage
        let wage = convertCurrencyToDouble(input: grossWage!)
        let taxes = GlobalVariable.totalWithholdings
        let taxesWithheld = convertCurrencyToDouble(input: taxes!)
        
        let entry = percentage401KRothEntry(grossWages: wage!, taxesWithheld: taxesWithheld!, percent: percent)
        let Sum = retirement401KRothPercentage.percentageRothEntry(entry: entry)
        GlobalVariable.retirement401KRothAmount = convertDoubleToCurrency(amount: Sum)
        deductionRothPercentage = percentageTextField.text
        deductionRothAmount = convertDoubleToCurrency(amount: Sum)
        GlobalVariable.retirement401KRothPercentage = deductPercentage
        CalculateSums.showFigures()
    }
}
