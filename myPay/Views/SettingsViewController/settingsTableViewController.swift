//
//  settingsTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/31/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class settingsTableViewController: UITableViewController {
    
    
    // MARK: - UIOutlets
    // MARK: UIView
    @IBOutlet weak var contentView1: UIView!
    @IBOutlet weak var contentView2: UIView!
    @IBOutlet weak var contentView3: UIView!
    @IBOutlet weak var contentView4: UIView!
    @IBOutlet weak var contentView5: UIView!
    @IBOutlet weak var contentView6: UIView!
    @IBOutlet weak var contentView7: UIView!
    @IBOutlet weak var contentView8: UIView!
    @IBOutlet weak var contentView9: UIView!
    @IBOutlet weak var contentView10: UIView!
    @IBOutlet weak var contentView11: UIView!
    @IBOutlet weak var contentView12: UIView!
    @IBOutlet weak var contentView13: UIView!
    @IBOutlet weak var contentView14: UIView!
    @IBOutlet weak var contentView15: UIView!
    @IBOutlet weak var contentView16: UIView!
    @IBOutlet weak var contentView17: UIView!
    @IBOutlet weak var contentView18: UIView!
    @IBOutlet weak var contentView19: UIView!
    @IBOutlet weak var allowancesBtnView: UIView!
    @IBOutlet weak var fililgStatusView: UIView!
    @IBOutlet weak var Retirement401KView: UIView!
    @IBOutlet weak var retirement401KRothView: UIView!
    @IBOutlet weak var medicalInsuranceView: UIView!
    @IBOutlet weak var medicalSavingsAccountView: UIView!
    @IBOutlet weak var dentalInsuranceView: UIView!
    @IBOutlet weak var visionInsuranceView: UIView!
    
    // MARK: StackView
    @IBOutlet weak var filingStatusStackView: UIStackView!
    @IBOutlet weak var withholdingdStackView: UIStackView!
    
    // MARK: UILabel
    @IBOutlet weak var positionNumTextField: UILabel!
    @IBOutlet weak var deductionTextLabel: UILabel!
    @IBOutlet weak var retirement401kPercentageTextLabel: UILabel!
    @IBOutlet weak var retirement401kAmountTextLabel: UILabel!
    @IBOutlet weak var retirement401kRothPercentageTextLabel: UILabel!
    @IBOutlet weak var retirement401kRothAmountTextLabel: UILabel!
    @IBOutlet weak var retirementSumTextLabel: UILabel!
    @IBOutlet weak var retirementBtnTextLabel: UILabel!
    @IBOutlet weak var insuranceMedicalDeductionTextLabel: UILabel!
    @IBOutlet weak var insuranceMedicalSavingsAccountTextLabel: UILabel!
    @IBOutlet weak var insuranceDentalDeductionTextLabel: UILabel!
    @IBOutlet weak var insuranceVisionDeductionTextLabel: UILabel!
    @IBOutlet weak var insuranceSumTextLabel: UILabel!
    @IBOutlet weak var additionalDeductionSumTextLabel: UILabel!
    @IBOutlet weak var insuranceBtnTextLabel: UILabel!
    @IBOutlet weak var currentPayPeriodTextLabel: UILabel!
    @IBOutlet weak var lengthPayPeriodTextLabel: UILabel!
    @IBOutlet weak var startOfPayPeriodWeekTextLabel: UILabel!
    @IBOutlet weak var totalTaxesWithheldTextLabel: UILabel!
    @IBOutlet weak var stateOfResidenceTextLabel: UILabel!
    @IBOutlet weak var filingStatusTextLabel: UILabel!
    @IBOutlet weak var numberOfAllowancesTextLabel: UILabel!
    @IBOutlet weak var federalTaxesWithheldTextLabel: UILabel!
    @IBOutlet weak var socialSecurityWithheldTextLabel: UILabel!
    @IBOutlet weak var mediCareWithheldTextLabel: UILabel!
    @IBOutlet weak var stateTaxesWithheldTextLabel: UILabel!
    @IBOutlet weak var totalSumOfDeductionsTextLabel: UILabel!
    @IBOutlet weak var sumTotalWithholdingsTextLabel: UILabel!
    
    // MARK: IndexPath
    let contentView1CellIndexPath = IndexPath(row: 2, section: 0)
    let contentView2CellIndexPath = IndexPath(row: 4, section: 0)
    let contentView3CellIndexPath = IndexPath(row: 6, section: 0)
    let contentView4CellIndexPath = IndexPath(row: 13, section: 0)
    let contentView5CellIndexPath = IndexPath(row: 16, section: 0)
    let contentView6CellIndexPath = IndexPath(row: 17, section: 0)
    let contentView7CellIndexPath = IndexPath(row: 18, section: 0)
    let contentView8CellIndexPath = IndexPath(row: 19, section: 0)
    let contentView9CellIndexPath = IndexPath(row: 20, section: 0)
    let contentView10CellIndexPath = IndexPath(row: 21, section: 0)
    let contentView11CellIndexPath = IndexPath(row: 22, section: 0)
    let contentView12CellIndexPath = IndexPath(row: 24, section: 0)
    let contentView13CellIndexPath = IndexPath(row: 25, section: 0)
    let contentView14CellIndexPath = IndexPath(row: 26, section: 0)
    let contentView15CellIndexPath = IndexPath(row: 27, section: 0)
    let contentView16CellIndexPath = IndexPath(row: 28, section: 0)
    let contentView17CellIndexPath = IndexPath(row: 29, section: 0)
    let contentView18CellIndexPath = IndexPath(row: 9, section: 0)
    let contentView19CellIndexPath = IndexPath(row: 11, section: 0)
    let deductionIndexPath = IndexPath(row: 29, section: 0)
   
    var positionHeight = 38
    var deductionHeight = 38
    var currentHeight = 38
    var taxesHeight = 214
    var positionNum = 0
    var retirementHeight = 0
    var insuranceHeight = 0
    var insuranceEditing = 0
    
     // MARK: UITableView
    @IBOutlet weak var positionTableView: UITableView!
    @IBOutlet weak var deductionTableView: UITableView!
    @IBOutlet var staticTableView: UITableView!
    var dataSource = positionDataSource()
    var dataSource2 = deductionDataSource()
    
    // MARK: Button
    @IBOutlet weak var editStatoOfResidenceBtn: UIButton!
    @IBOutlet weak var editFiingStatusBtn: UIButton!
    @IBOutlet weak var editAllowancesBtn: UIButton!
    @IBOutlet weak var editPositionsBtn: UIButton!
    @IBOutlet weak var viewWithholdingsBtn: UIButton!
    @IBOutlet weak var addRetirementBtn: UIButton!
    @IBOutlet weak var addInsuranceBtn: UIButton!
    
    
    // MARK: Int
    var jobIndex = GlobalVariable.jobIndex
    
    // MARK: Bool
    var isContentView1Shown: Bool = false {
        didSet{
            contentView1.isHidden = !isContentView1Shown
        }
    }
    var isContentView2Shown: Bool = false {
        didSet{
            contentView2.isHidden = !isContentView2Shown
        }
    }
    var isContentView3Shown: Bool = false {
        didSet{
            contentView3.isHidden = !isContentView3Shown
        }
    }
    var isContentView4Shown: Bool = false {
        didSet{
            contentView4.isHidden = !isContentView4Shown
        }
    }
    var isContentView5Shown: Bool = false {
        didSet{
            contentView5.isHidden = !isContentView5Shown
        }
    }
    var isContentView6Shown: Bool = false {
        didSet{
            contentView6.isHidden = !isContentView6Shown
        }
    }
    var isContentView7Shown: Bool = false {
        didSet{
            contentView7.isHidden = !isContentView7Shown
        }
    }
    var isContentView8Shown: Bool = false {
        didSet{
            contentView8.isHidden = !isContentView8Shown
        }
    }
    var isContentView9Shown: Bool = false {
        didSet{
            contentView9.isHidden = !isContentView9Shown
        }
    }
    var isContentView10Shown: Bool = false {
        didSet{
            contentView10.isHidden = !isContentView10Shown
        }
    }
    var isContentView11Shown: Bool = false {
        didSet{
            contentView11.isHidden = !isContentView11Shown
        }
    }
    var isContentView12Shown: Bool = false {
        didSet{
            contentView12.isHidden = !isContentView12Shown
        }
    }
    var isContentView13Shown: Bool = false {
        didSet{
            contentView13.isHidden = !isContentView13Shown
        }
    }
    var isContentView14Shown: Bool = false {
        didSet{
            contentView14.isHidden = !isContentView14Shown
        }
    }
    var isContentView15Shown: Bool = false {
        didSet{
            contentView15.isHidden = !isContentView15Shown
        }
    }
    var isContentView16Shown: Bool = false {
        didSet{
            contentView16.isHidden = !isContentView16Shown
        }
    }
    var isContentView17Shown: Bool = false {
        didSet{
            contentView17.isHidden = !isContentView17Shown
        }
    }
    var isContentView18Shown: Bool = false {
        didSet{
            contentView18.isHidden = !isContentView18Shown
        }
    }
    var isContentView19Shown: Bool = false {
        didSet{
            contentView19.isHidden = !isContentView19Shown
        }
    }
    
    var editFilingStatusBtnPressed = false
    var editAllowancesBtnPressed = true
    var viewWithholdingsBtnPressed = true
    var retirement401Kadded = false
    var retirement401KRothadded = false
    var medicalInsuranceAdded = false
    var medicalSavingsAcountAdded = false
    var dentalInsuranceAdded = false
    var visionInsuranceAdded = false
    var loadingView = true
    
    // MARK: - Load Views
    override func viewDidLoad() {
        super.viewDidLoad()
        positionTableView.dataSource = dataSource
        positionTableView.delegate = dataSource
        deductionTableView.dataSource = dataSource2
        deductionTableView.delegate = dataSource2
        showFigures()
        countPositions()
        isContentView3Shown = false
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        positionTableView.dataSource = dataSource
        positionTableView.delegate = dataSource
        deductionTableView.dataSource = dataSource2
        deductionTableView.delegate = dataSource2
        showFigures()
        countPositions()
//        isContentView3Shown = false
    }
    
    // MARK: - func
    
    func showFigures(){
        CalculateSums.showFigures()
        countPositions()
        changePositionCellHeight(num: positionNum)
//        isContentView3Shown = false
        GlobalVariable.formatter.dateFormat = "MMMM dd, yyyy"
        let startDate = GlobalVariable.formatter.string(from: GlobalVariable.startOfPayPeriod)
        currentPayPeriodTextLabel.text = startDate
        switch GlobalVariable.PayPeriodLength {
        case 1:
            lengthPayPeriodTextLabel.text = "Monthly"
        case 2:
            lengthPayPeriodTextLabel.text = "Bi-Monthly"
        case 3:
            lengthPayPeriodTextLabel.text = "Every 2 Weeks"
        case 4:
            lengthPayPeriodTextLabel.text = "Weekly"
        default: break
        }
        switch GlobalVariable.filingStatus {
        case 1:
            filingStatusTextLabel.text = "Single"
            
        case 2:
            filingStatusTextLabel.text = "Married Filing Single"
            
        case 3:
            filingStatusTextLabel.text = "Married Filing Joint"
            
        case 4:
            filingStatusTextLabel.text = "Head Of Household"
            
        default: break
        }
        numberOfAllowancesTextLabel.text = "\(GlobalVariable.allowances)"
        
        switch GlobalVariable.stateOfResidence {
        case "AL":
            stateOfResidenceTextLabel.text = "Alabama"
        case "AK":
            stateOfResidenceTextLabel.text = "Alaska"
        case "AZ":
            stateOfResidenceTextLabel.text = "Arizona"
        case "AR":
            stateOfResidenceTextLabel.text = "Arkansas"
        case "CA":
            stateOfResidenceTextLabel.text = "Caloifornia"
        case "CO":
            stateOfResidenceTextLabel.text = "Colorado"
        case "CT":
            stateOfResidenceTextLabel.text = "Connecticut"
        case "DE":
            stateOfResidenceTextLabel.text = "Delaware"
        case "DC":
            stateOfResidenceTextLabel.text = "District Of Columbia"
        case "FL":
            stateOfResidenceTextLabel.text = "Florida"
        case "GA":
            stateOfResidenceTextLabel.text = "Georgia"
        case "HI":
            stateOfResidenceTextLabel.text = "Hawaii"
        case "ID":
            stateOfResidenceTextLabel.text = "Idaho"
        case "IL":
            stateOfResidenceTextLabel.text = "Illinois"
        case "IN":
            stateOfResidenceTextLabel.text = "Indiana"
        case "IA":
            stateOfResidenceTextLabel.text = "Iowa"
        case "KS":
            stateOfResidenceTextLabel.text = "Kansas"
        case "LA":
            stateOfResidenceTextLabel.text = "Louisiana"
        case "ME":
            stateOfResidenceTextLabel.text = "Maine"
        case "MD":
            stateOfResidenceTextLabel.text = "Maryland"
        case "MA":
            stateOfResidenceTextLabel.text = "Massachusetts"
        case "MI":
            stateOfResidenceTextLabel.text = "Michigan"
        case "MN":
            stateOfResidenceTextLabel.text = "Minnesota"
        case "Ms":
            stateOfResidenceTextLabel.text = "Mississippi"
        case "MO":
            stateOfResidenceTextLabel.text = "Missouri"
        case "MT":
            stateOfResidenceTextLabel.text = "Montana"
        case "NE":
            stateOfResidenceTextLabel.text = "Nebraska"
        case "NV":
            stateOfResidenceTextLabel.text = "Nevada"
        case "NH":
            stateOfResidenceTextLabel.text = "New Hampshire"
        case "NJ":
            stateOfResidenceTextLabel.text = "New Jersey"
        case "NM":
            stateOfResidenceTextLabel.text = "New Mexico"
        case "NY":
            stateOfResidenceTextLabel.text = "New York"
        case "NC":
            stateOfResidenceTextLabel.text = "North Carolina"
        case "ND":
            stateOfResidenceTextLabel.text = "North Dakota"
        case "OH":
            stateOfResidenceTextLabel.text = "Ohio"
        case "OK":
            stateOfResidenceTextLabel.text = "Oklahoma"
        case "OR":
            stateOfResidenceTextLabel.text = "Oregan"
        case "PA":
            stateOfResidenceTextLabel.text = "Pennsylvania"
        case "RI":
            stateOfResidenceTextLabel.text = "Rhode Islanda"
        case "SC":
            stateOfResidenceTextLabel.text = "South Dakota"
        case "SD":
            stateOfResidenceTextLabel.text = "Alaska"
        case "TN":
            stateOfResidenceTextLabel.text = "Tennessee"
        case "TX":
            stateOfResidenceTextLabel.text = "Texas"
        case "UT":
            stateOfResidenceTextLabel.text = "Utah"
        case "VT":
            stateOfResidenceTextLabel.text = "Vermont"
        case "VA":
            stateOfResidenceTextLabel.text = "Virginia"
        case "WA":
            stateOfResidenceTextLabel.text = "Washington"
        case "WV":
            stateOfResidenceTextLabel.text = "West Virginia"
        case "WI":
            stateOfResidenceTextLabel.text = "Wisconsin"
        case "WY":
            stateOfResidenceTextLabel.text = "Wyoming"
        default:
            break
        }
        federalTaxesWithheldTextLabel.text = GlobalVariable.federalIncomeTax
        socialSecurityWithheldTextLabel.text = GlobalVariable.SocialSecurity
        mediCareWithheldTextLabel.text = GlobalVariable.mediCare
        stateTaxesWithheldTextLabel.text = GlobalVariable.stateIncomeTax
        sumRetirement()
        sumInsurance()
        sumAdditionDeductions()
        totalSumOfDeductionsTextLabel.text = GlobalVariable.totalDeductions
        sumTotalWithholdingsTextLabel.text = GlobalVariable.totalWithholdings
        totalTaxesWithheldTextLabel.text = GlobalVariable.totalWithholdings
        show401kFigures()
    }
    
    func countPositions() {
        
        let position = Data.Jobs[jobIndex].positionModel
        if position.count > 0 {
            let numOfJobsArrays = position.map { $0.position}
            print(numOfJobsArrays)
            let size = numOfJobsArrays.count
            positionNumTextField.text = "\(size)"
            positionNum = size
        } else {
            positionNumTextField.text = "0"
        }
        
    }
    func changePositionCellHeight(num: Int) {
        switch num {
        case 0:
            positionHeight = 33
            GlobalVariable.positionHeight = 33
            editPositionsBtn.isHidden = true
        case 1:
            positionHeight = 92
            GlobalVariable.positionHeight = 92
            editPositionsBtn.isHidden = false
        case 2:
            positionHeight = 150
            GlobalVariable.positionHeight = 150
        case 3:
            positionHeight = 208
            GlobalVariable.positionHeight = 208
        case 4:
            positionHeight = 266
            GlobalVariable.positionHeight = 266
        case 5:
            positionHeight = 324
            GlobalVariable.positionHeight = 324
        default:
            break
        }
        updatePositionView()
    }
    func changeDeductionCellHeight() {
        let deductions = Data.Jobs[jobIndex].voluntaryDeductions
        let deductNum = deductions.count
        currentHeight = deductionHeight
        if currentHeight > ((deductNum * 50) + 38) {
            deductionHeight = currentHeight - 50
        } else {
            deductionHeight = ((deductNum * 50) + 38)
        }
        if currentHeight == 285 {
            deductionHeight = currentHeight
        }
        updateDeductionView()
        
    }
    func resizeDeductionCellHeight(){
        currentHeight = deductionHeight
        if currentHeight > 38 {
            deductionHeight = currentHeight - 50
        } else {
            deductionHeight =  38
        }
        isContentView17Shown = true
        staticTableView.beginUpdates()
        staticTableView.endUpdates()
        deductionTableView.reloadData()
    }
    
    
    func changeInsuranceCellHeight() {
        let cellheight1: Int
        let cellheight2: Int
        let cellheight3: Int
        let cellheight4: Int
        if medicalInsuranceAdded == true {
            cellheight1 = 50
        } else {
            cellheight1 = 0
        }
        if medicalSavingsAcountAdded == true {
            cellheight2 = 50
        } else {
            cellheight2 = 0
        }
        if dentalInsuranceAdded == true {
            cellheight3 = 50
        } else {
            cellheight3 = 0
        }
        if visionInsuranceAdded == true {
            cellheight4 = 50
        } else {
            cellheight4 = 0
        }
        insuranceHeight = ((cellheight1 + cellheight2) + (cellheight3 + cellheight4))
        updateInsuranceCellHeight()
    }
    
    func updateInsuranceCellHeight() {
        if medicalInsuranceAdded == true {
            medicalInsuranceView.isHidden = false
        } else {
            medicalInsuranceView.isHidden = true
        }
        if medicalSavingsAcountAdded == true {
            medicalSavingsAccountView.isHidden = false
        } else {
            medicalSavingsAccountView.isHidden = true
        }
        if dentalInsuranceAdded == true {
            dentalInsuranceView.isHidden = false
        } else {
            dentalInsuranceView.isHidden = true
        }
        if visionInsuranceAdded == true {
            visionInsuranceView.isHidden = false
        } else {
            visionInsuranceView.isHidden = true
        }
        isContentView15Shown = true
        staticTableView.beginUpdates()
        staticTableView.endUpdates()
    }
    
    func updatePositionView() {
        
        //        countPositions()
        //        changePositionCellHeight(num: positionNum)
        //        if GlobalVariable.jobPositionArrayIsPopulated == true {
        isContentView1Shown = false
        isContentView2Shown = false
        isContentView4Shown = false
        isContentView5Shown = false
        if loadingView == false {
            isContentView3Shown = true
        } else {
            isContentView3Shown = false
            isContentView3Shown = false
        }
        staticTableView.beginUpdates()
        staticTableView.endUpdates()
        positionTableView.reloadData()
        //        }
    }
    
    func updateDeductionView() {
        isContentView1Shown = false
        isContentView2Shown = false
        isContentView3Shown = false
        isContentView4Shown = false
        isContentView5Shown = false
        isContentView6Shown = false
        isContentView7Shown = false
        isContentView8Shown = false
        isContentView9Shown = false
        isContentView10Shown = false
        isContentView11Shown = false
        isContentView13Shown = false
        isContentView15Shown = false
        isContentView17Shown = true
        staticTableView.beginUpdates()
        staticTableView.endUpdates()
        deductionTableView.reloadData()
        
    }
    
    func updateRetirementCellHeight() {
        if retirement401Kadded == true {
        Retirement401KView.isHidden = false
        } else {
            Retirement401KView.isHidden = true
        }
        if retirement401KRothadded == true {
            retirement401KRothView.isHidden = false
        } else {
            retirement401KRothView.isHidden = true
        }
        isContentView13Shown = true
        staticTableView.beginUpdates()
        staticTableView.endUpdates()
    }
    
    func sumRetirement() {
        let retirAmount1 = retirement401kAmountTextLabel.text!
        GlobalVariable.retirement401KAmount = retirement401kAmountTextLabel.text ?? "$000.00"
        let retirAmount2 = retirement401kRothAmountTextLabel.text!
        GlobalVariable.retirement401KRothAmount = retirement401kRothAmountTextLabel.text ?? "$000.00"
        let retirement = [retirAmount1, retirAmount2]
        let sumArray = retirement.map{convertCurrencyToDouble(input: $0)}
        let retirementSumArray = sumArray.compactMap {$0}
        let Sum = retirementSumArray.reduce(0.0, {$0 + (Double($1) )})
        retirementSumTextLabel.text = convertDoubleToCurrency(amount: Sum)
        GlobalVariable.totalRetirementSum = convertDoubleToCurrency(amount: Sum)
        
    }
    
    func sumInsurance() {
        GlobalVariable.medicalInsurance = insuranceMedicalDeductionTextLabel.text ?? "$000.00"
        GlobalVariable.medicalSavingsAccount = insuranceMedicalSavingsAccountTextLabel.text ?? "$000.00"
        GlobalVariable.dentalInsurance = insuranceDentalDeductionTextLabel.text ?? "$000.00"
        GlobalVariable.visionInsurance = insuranceVisionDeductionTextLabel.text ?? "$000.00"
        let medical = insuranceMedicalDeductionTextLabel.text!
        let medicalSavings = insuranceMedicalSavingsAccountTextLabel.text!
        let dental = insuranceDentalDeductionTextLabel.text!
        let vision = insuranceVisionDeductionTextLabel.text!
        let insurance = [medical, medicalSavings, dental, vision]
        let sumArray = insurance.map{convertCurrencyToDouble(input: $0)}
        let insuranceSumArray = sumArray.compactMap {$0}
        let Sum = insuranceSumArray.reduce(0.0, {$0 + (Double($1) )})
        insuranceSumTextLabel.text = convertDoubleToCurrency(amount: Sum)
        GlobalVariable.totalInsuranceSum = convertDoubleToCurrency(amount: Sum)
    }
    
    func sumAdditionDeductions(){
        let additDeductionArray = Data.Jobs[jobIndex].voluntaryDeductions.map {$0.Amount}
        let sumArray = additDeductionArray.map{convertCurrencyToDouble(input: $0)}
        let AddiDeductSumArray = sumArray.compactMap {$0}
        let Sum = AddiDeductSumArray.reduce(0.0, {$0 + (Double($1) )})
        additionalDeductionSumTextLabel.text = convertDoubleToCurrency(amount: Sum)
        GlobalVariable.totalAdditionalDeductionSum = convertDoubleToCurrency(amount: Sum)
    }
    
    func show401kFigures() {
            retirement401kPercentageTextLabel.text = GlobalVariable.retirement401KPercentage
            retirement401kAmountTextLabel.text = GlobalVariable.retirement401KAmount
    }
    
    // MARK: Table View Data
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (contentView1CellIndexPath.section, contentView1CellIndexPath.row):
            if isContentView1Shown {
                return 40
            } else {
                return 0
            }
        case (contentView2CellIndexPath.section, contentView2CellIndexPath.row):
            if isContentView2Shown {
                return 40
            } else {
                return 0
            }
        case (contentView3CellIndexPath.section, contentView3CellIndexPath.row):
            if isContentView3Shown {
                return CGFloat(positionHeight)
            } else {
                return 0
            }
        case (contentView4CellIndexPath.section, contentView4CellIndexPath.row):
            if isContentView4Shown {
                return 86
            } else {
                return 0
            }
        case (contentView5CellIndexPath.section, contentView5CellIndexPath.row):
            if isContentView5Shown {
                return 54
            } else {
                return 0
            }
        case (contentView6CellIndexPath.section, contentView6CellIndexPath.row):
            if isContentView6Shown {
                return 54
            } else {
                return 0
            }
        case (contentView7CellIndexPath.section, contentView7CellIndexPath.row):
            if isContentView7Shown {
                return 184
            } else {
                return 0
            }
        case (contentView8CellIndexPath.section, contentView8CellIndexPath.row):
            if isContentView8Shown {
                return 54
            } else {
                return 0
            }
        case (contentView9CellIndexPath.section, contentView9CellIndexPath.row):
            if isContentView9Shown {
                return 48
            } else {
                return 0
            }
        case (contentView10CellIndexPath.section, contentView10CellIndexPath.row):
            if isContentView10Shown {
                return 54
            } else {
                return 0
            }
        case (contentView11CellIndexPath.section, contentView11CellIndexPath.row):
            if isContentView11Shown {
                return 188
            } else {
                return 0
            }
        case (contentView12CellIndexPath.section, contentView12CellIndexPath.row):
            if isContentView12Shown {
                return 54
            } else {
                return 0
            }
        case (contentView13CellIndexPath.section, contentView13CellIndexPath.row):
            if isContentView13Shown {
                return CGFloat(retirementHeight)
            } else {
                return 0
            }
        case (contentView14CellIndexPath.section, contentView14CellIndexPath.row):
            if isContentView14Shown {
                return 54
            } else {
                return 0
            }
        case (contentView15CellIndexPath.section, contentView15CellIndexPath.row):
            if isContentView15Shown {
                return CGFloat(insuranceHeight)
            } else {
                return 0
            }
        case (contentView16CellIndexPath.section, contentView16CellIndexPath.row):
            if isContentView16Shown {
                return 54
            } else {
                return 0
            }
        case (contentView17CellIndexPath.section, contentView17CellIndexPath.row):
            if isContentView17Shown {
                return CGFloat(deductionHeight)
            } else {
                return 0
            }
        case (contentView18CellIndexPath.section, contentView18CellIndexPath.row):
            if isContentView18Shown {
                return 38
            } else {
                return 0
            }
        case (contentView19CellIndexPath.section, contentView19CellIndexPath.row):
            if isContentView19Shown {
                return 38
            } else {
                return 0
            }

        default:
            if indexPath.row == 0 {
                return 61
            }
            if indexPath.row == 7 {
                return 61
            }
            if indexPath.row == 12 {
                return 61
            }
        }
        return 54
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == contentView1CellIndexPath.section &&
            indexPath.row == contentView1CellIndexPath.row - 1 {
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            isContentView18Shown = false
            isContentView19Shown = false
            if isContentView1Shown == true {
                isContentView1Shown = false
            } else {
                isContentView1Shown = true
                
            }
        }
        if indexPath.section == contentView2CellIndexPath.section &&
            indexPath.row == contentView2CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            isContentView18Shown = false
            isContentView19Shown = false
            if isContentView2Shown == true {
                isContentView2Shown = false
            } else {
                isContentView2Shown = true
                
            }
        }
        if indexPath.section == contentView3CellIndexPath.section &&
            indexPath.row == contentView3CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            isContentView18Shown = false
            isContentView19Shown = false
            if isContentView3Shown == true {
                isContentView3Shown = false
            } else {
                isContentView3Shown = true
                
            }
        }
        if indexPath.section == contentView4CellIndexPath.section &&
            indexPath.row == contentView4CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            isContentView18Shown = false
            isContentView19Shown = false
            if isContentView4Shown == true {
                isContentView4Shown = false
            } else {
                isContentView4Shown = true
                
            }
        }
        if indexPath.section == contentView5CellIndexPath.section &&
            indexPath.row == contentView5CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            isContentView18Shown = false
            isContentView19Shown = false
            if isContentView5Shown == true {
                isContentView5Shown = false
                isContentView6Shown = false
                isContentView7Shown = false
                isContentView8Shown = false
                isContentView9Shown = false
                isContentView10Shown = false
                isContentView11Shown = false
                isContentView12Shown = false
                isContentView17Shown = false
                isContentView18Shown = false
                isContentView19Shown = false
            } else {
                isContentView5Shown = true
                isContentView6Shown = true
                isContentView8Shown = true
                isContentView10Shown = true
            }
        }
        if indexPath.section == contentView5CellIndexPath.section &&
            indexPath.row == contentView5CellIndexPath.row {
            isContentView7Shown = false
            isContentView9Shown = false
            isContentView11Shown = false
            performSegue(withIdentifier: "toSetStateOfResidenceSegue", sender: AnyClass.self)
        }
        if indexPath.section == contentView7CellIndexPath.section &&
            indexPath.row == contentView7CellIndexPath.row - 1 {
            isContentView9Shown = false
            isContentView11Shown = false
            if isContentView7Shown == true {
               isContentView7Shown = false
            } else {
                isContentView7Shown = true
            }
        }
        if indexPath.section == contentView9CellIndexPath.section &&
            indexPath.row == contentView9CellIndexPath.row - 1 {
            isContentView7Shown = false
            isContentView11Shown = false
            if isContentView9Shown == true {
                isContentView9Shown = false
            } else {
                isContentView9Shown = true
            }
        }
        if indexPath.section == contentView11CellIndexPath.section &&
            indexPath.row == contentView11CellIndexPath.row - 1 {
            isContentView7Shown = false
            isContentView9Shown = false
            if isContentView11Shown == true {
                isContentView11Shown = false
            } else {
                isContentView11Shown = true
            }
        }
        if indexPath.section == contentView12CellIndexPath.section &&
            indexPath.row == contentView12CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            sumRetirement()
            showFigures()
            if isContentView12Shown == true {
                isContentView12Shown = false
                isContentView13Shown = false
                isContentView14Shown = false
                isContentView15Shown = false
                isContentView16Shown = false
                isContentView17Shown = false
            } else {
                isContentView12Shown = true
                isContentView14Shown = true
                isContentView16Shown = true
            }
        }
        if indexPath.section == contentView13CellIndexPath.section &&
            indexPath.row == contentView13CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            if isContentView13Shown == true {
                isContentView13Shown = false
            } else {
                isContentView13Shown = true
            }
        }
        if indexPath.section == contentView15CellIndexPath.section &&
            indexPath.row == contentView15CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            if isContentView15Shown == true {
                isContentView13Shown = false
                isContentView15Shown = false
            } else {
                isContentView15Shown = true
            }
        }
        if indexPath.section == contentView17CellIndexPath.section &&
            indexPath.row == contentView17CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            isContentView18Shown = false
            isContentView19Shown = false
            if isContentView17Shown == true {
                isContentView13Shown = false
                isContentView15Shown = false
                isContentView17Shown = false
            } else {
                isContentView17Shown = true
            }
        }
        if indexPath.section == contentView18CellIndexPath.section &&
            indexPath.row == contentView18CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            isContentView17Shown = false
            isContentView19Shown = false
            if isContentView18Shown == true {
                isContentView18Shown = false
            } else {
                isContentView18Shown = true
            }
        }
        if indexPath.section == contentView19CellIndexPath.section &&
            indexPath.row == contentView19CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            isContentView6Shown = false
            isContentView7Shown = false
            isContentView8Shown = false
            isContentView9Shown = false
            isContentView10Shown = false
            isContentView11Shown = false
            isContentView17Shown = false
            isContentView18Shown = false
            if isContentView19Shown == true {
                isContentView19Shown = false
            } else {
                isContentView19Shown = true
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x:0, y:0, width: tableView.frame.size.width, height: 70))
        footerView.backgroundColor = UIColor.orange
        return footerView
    }
    
    // MARK: - IBAction
    // MARK: Button
    @IBAction func addPositionBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "toAddPositionSegue", sender: Any?.self)
    }
    
    @IBAction func addRetirementBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "toRetirementDeductionSegue", sender: AnyClass.self)
        
    }
    @IBAction func addInsuranceBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "toInsuranceSegue", sender: AnyClass.self)
        
    }
    @IBAction func addDeductionBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "toAddAdditionalDeductionSegue", sender: Any?.self)
    }
    
    
    // MARK: UnWind
    @IBAction func unwindFromAddVC(_ sender: UIStoryboardSegue) {
        if let setStateOfResidenceVC = sender.source as? setStateOfResidenceViewController {
            GlobalVariable.stateOfResidenceSet = setStateOfResidenceVC.stateOfResedienceSet
        }
    }
    @IBAction func unwindFromEditPositionVC(_ sender: UIStoryboardSegue) {
        if let editPositionVC = sender.source as? editPositionTableViewController {
            countPositions()
            changePositionCellHeight(num: editPositionVC.newPositionNum ?? 10)
            print(editPositionVC.newPositionNum ?? 10)
        }
    }
    @IBAction func unwindFromEditDeductionVC(_ sender: UIStoryboardSegue) {
        if let editDeductionVC = sender.source as? editDeductionViewController {
            changeDeductionCellHeight()
            sumAdditionDeductions()
            showFigures()
            print(editDeductionVC.newDeductionNum ?? 10)
        }
    }
    @IBAction func unwindFrom401KVC(_ sender: UIStoryboardSegue) {
        guard let ret401kVC = sender.source as? ret401KTableViewController else {return}
        print(ret401kVC.deductionPercentage)
        show401kFigures()
        if retirement401Kadded == false {
            
            if retirementHeight == 0 {
                retirementHeight = 51
                GlobalVariable.retirementHeight = 51
                retirementBtnTextLabel.text = "Add/Edit"
            } else {
                retirementHeight = 102
                GlobalVariable.retirementHeight = 102
                retirementBtnTextLabel.text = "Edit"
            }
            retirement401Kadded = true
            GlobalVariable.retirement401Kadded = true
        }
        updateRetirementCellHeight()
        sumRetirement()
        showFigures()
    }
    @IBAction func unwindFrom401KRothVC(_ sender: UIStoryboardSegue) {
        guard let ret401kRothVC = sender.source as? ret401KRothTableViewController else {return}
        retirement401kRothPercentageTextLabel.text = ret401kRothVC.deductionRothPercentage
        retirement401kRothAmountTextLabel.text = ret401kRothVC.deductionRothAmount
        if retirement401KRothadded == false {
            
            if retirementHeight == 0 {
                retirementHeight = 51
                GlobalVariable.retirementHeight = 51
                retirementBtnTextLabel.text = "Add/Edit"
            } else {
                retirementHeight = 102
                GlobalVariable.retirementHeight = 102
                retirementBtnTextLabel.text = "Edit"
            }
            retirement401KRothadded = true
            GlobalVariable.retirement401KRothadded = true
        }
        updateRetirementCellHeight()
        sumRetirement()
        showFigures()
    }
    @IBAction func unwindFromInsuranceVC(_ sender: UIStoryboardSegue) {
        guard let insuranceDeductionVC = sender.source as? insuranceDeductionTableViewController else {return}
    
        if GlobalVariable.insuranceEditing == 1 {
            insuranceMedicalDeductionTextLabel.text = insuranceDeductionVC.insuranceAmount
            medicalInsuranceAdded = true
        }
        if GlobalVariable.insuranceEditing == 2 {
            insuranceMedicalSavingsAccountTextLabel.text = insuranceDeductionVC.insuranceAmount
            medicalSavingsAcountAdded = true
        }
        if GlobalVariable.insuranceEditing == 3 {
            insuranceDentalDeductionTextLabel.text = insuranceDeductionVC.insuranceAmount
            dentalInsuranceAdded = true
        }
        if GlobalVariable.insuranceEditing == 4 {
            insuranceVisionDeductionTextLabel.text = insuranceDeductionVC.insuranceAmount
            visionInsuranceAdded = true
        }
        if insuranceHeight == 0  {
            insuranceBtnTextLabel.text = "Add/Edit"
        } else if insuranceHeight == 200  {
            insuranceBtnTextLabel.text = "Edit"
        }
        changeInsuranceCellHeight()
        sumInsurance()
        showFigures()
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddPositionSegue" {
            if let addPositionVC = segue.destination as? addPositionsViewController {
                addPositionVC.setBtnTitle = 3
                addPositionVC.doneSaving = { [weak self] in
                    guard let self = self else { return }
                    self.countPositions()
                    self.changePositionCellHeight(num: self.positionNum)
                    self.showFigures()
                    
                    
                }
            }
        }
        if segue.identifier == "toAddAdditionalDeductionSegue" {
            if let addDeductionVC = segue.destination as? addDeductionsViewController {
                addDeductionVC.doneSaving = { [weak self] in
                    guard let self = self else { return }
                    self.changeDeductionCellHeight()
                    self.sumAdditionDeductions()
                    self.showFigures()
                    
                    
                }
            }
        }
        if segue.identifier == "toAddInsuranceDeductionSegue" {
            if let insuranceDeductionVC = segue.destination as? insuranceDeductionTableViewController {
                if insuranceEditing == 1 {
                    insuranceDeductionVC.info = "Enter Medical Insurance Amount"
                }
                if insuranceEditing == 2 {
                    insuranceDeductionVC.info = "Enter Amount to go in Medical Savings Acct"
                }
                if insuranceEditing == 3 {
                    insuranceDeductionVC.info = "Enter Dental Insurance Amount"
                }
                if insuranceEditing == 4 {
                    insuranceDeductionVC.info = "Enter Vision Insurance Amount"
                }
            }
        }
    }
}
    

class positionDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Int
    var jobIndex = GlobalVariable.jobIndex
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Data.Jobs[jobIndex].positionModel.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "jobPositionCell") as! PositionDetailCell
        let position = Data.Jobs[jobIndex].positionModel[indexPath.row].position
        let rate = Data.Jobs[jobIndex].positionModel[indexPath.row].hourlyRate
        
        cell.positionTextLabel.text = position
        cell.hourlyRateTextLabel.text = rate
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PositionDetailCell
        cell.positionTitleTextLabel.textColor = UIColor.white
        cell.positionTextLabel.textColor = UIColor.white
        cell.hourlyRateTitleLabel.textColor = UIColor.white
        cell.hourlyRateTextLabel.textColor = UIColor.white
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (contextualAction, view, actionPerformed: (Bool) -> Void) in
            JobsFunctions.deletePosition(index: indexPath.row)
            tableView.reloadData()
            if Data.Jobs[self.jobIndex].positionModel.count == 0 {
                GlobalVariable.jobPositionArrayIsPopulated = false

            }
            actionPerformed(true)
            settingsTableViewController().countPositions()
        }
        return UISwipeActionsConfiguration(actions: [delete])
    }
}

class deductionDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Int
    var jobIndex = GlobalVariable.jobIndex
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Data.Jobs[jobIndex].voluntaryDeductions.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addDeductionCell") as! voluntaryDeductionTableViewCell
        let name = Data.Jobs[jobIndex].voluntaryDeductions[indexPath.row].Name
        let amount = Data.Jobs[jobIndex].voluntaryDeductions[indexPath.row].Amount
        
        cell.deductionNameTextLabel.text = name
        cell.deductionAmountTextLabel.text = amount
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 51
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! voluntaryDeductionTableViewCell
        cell.deductionNameTextLabel.textColor = UIColor.white
        cell.deductionAmountTextLabel.textColor = UIColor.white
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (contextualAction, view, actionPerformed: (Bool) -> Void) in
            JobsFunctions.deleteDeduction(index: indexPath.row)
            tableView.reloadData()
            if Data.Jobs[self.jobIndex].voluntaryDeductions.count == 0 {
                GlobalVariable.voluntaryDeductionArrayIsPopulated = false
                
            }
            actionPerformed(true)
        }
        return UISwipeActionsConfiguration(actions: [delete])
    }
}

