//
//  OverviewViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/9/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit
import Charts

class OverviewViewController: UIViewController, ModalTransitionListener {
   
    
    
    
    // MARK: - UIOutlets
    // MARK: UILabels
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var payPeriodTipsLabel: UILabel!
    @IBOutlet weak var payPeriodWageLabel: UILabel!
    @IBOutlet weak var payPeriodHoursLabel: UILabel!
    
    @IBOutlet weak var monthTipLabel: UILabel!
    @IBOutlet weak var monthWageLabel: UILabel!
    @IBOutlet weak var monthHoursLabel: UILabel!
    
    @IBOutlet weak var grossWageLabel: UILabel!
    @IBOutlet weak var totalDeductionLabel: UILabel!
    @IBOutlet weak var taxesPaidLabel: UILabel!
    @IBOutlet weak var netEarningsLabel: UILabel!
    
    @IBOutlet weak var todaysShiftTimeLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    
    
    // MARK: UIView
    @IBOutlet weak var graphOutline: UIView!
    @IBOutlet weak var todaysShiftBkgOutline: UIView!
    
    
    // MARK: UIBarButtonItem
    @IBOutlet weak var addJobBtn: UIBarButtonItem!
    
    
    // MARK: UIButtons
    @IBOutlet weak var jobListBtn: UIButton!
    @IBOutlet weak var clockInBtn: UIButton!
    @IBOutlet weak var clockOutBtn: UIButton!
    @IBOutlet weak var enterTipsBtn: UIButton!
    
    // MARK: BarChart
    @IBOutlet weak var barChartView: BarChartView!
    
    // MARK: - DropDown Menu
    @IBOutlet weak var tblDropDown: UITableView!
    @IBOutlet weak var tblDropDownHC: NSLayoutConstraint!
    
    
    // MARK: - Variables & Let
    // MARK: BarChart Vars
    var months: [String]!
    var dataEntries: [BarChartDataEntry] = []
    var tips = Double()
    var wage =  Double()
    var hours =  Double()
    
    // MARK: DropDown Menu
    var isTableVisible = false
    var heightNum: Double?
    var isJobListShown = false
    
    var totalSec:Float = 0
    var timer = Timer()
    
    var jobID = UUID().uuidString
    var jobIndex = GlobalVariable.jobIndex
    
    
    // MARK: Let Statment
    let formatter = DateFormatter()
    
    
    // MARK: - Load Views
    override func viewDidLoad() {
        super.viewDidLoad()

        graphOutline.addLtGrayOutline()
        todaysShiftBkgOutline.addAccentColorOutline()
        tblDropDown.delegate = self
        tblDropDown.dataSource = self
        tblDropDownHC.constant = 0
        GlobalVariable.payPeriodID = jobID
        showJobInfo()
    }
    override func viewWillAppear(_ animated: Bool) {
        showJobInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("View did appeared triggered")
        introPopupWindows(popUp: 1)
        ModalTransitionMediator.instance.setListener(listener: self)
//        CalculateSums.showFigures()
    }
    
    
    
    // MARK: - Functions
    
    func initBarChart() {
        tips = convertCurrencyToDouble(input: GlobalVariable.PayPeriodTips!) ?? 0.0
        wage = convertCurrencyToDouble(input: GlobalVariable.PayPeriodWage!) ?? 0.0
        hours = (GlobalVariable.PayPeriodHours! as NSString).doubleValue
        months = ["Jan", "Feb", "Mar"]
        let unitsSold = [tips, wage, hours]
        setChart(dataPoints: months, values: unitsSold)
    }
    
    func showJobInfo() {
        CalculateSums.showFigures()
        toggleAddJobBtn()
        hideJobList()
        initBarChart()
        jobTitleLabel.text = GlobalVariable.jobName
        payPeriodTipsLabel.text = GlobalVariable.PayPeriodTips
        payPeriodWageLabel.text = GlobalVariable.PayPeriodWage
        payPeriodHoursLabel.text = GlobalVariable.PayPeriodHours
        monthTipLabel.text = GlobalVariable.MonthTips
        monthWageLabel.text = GlobalVariable.MonthWage
        monthHoursLabel.text = GlobalVariable.MonthHour
        grossWageLabel.text = GlobalVariable.GrossWage
        totalDeductionLabel.text = GlobalVariable.totalDeductions
        taxesPaidLabel.text = GlobalVariable.totalWithholdings
        netEarningsLabel.text = GlobalVariable.totalNet
        
    }
    
    func viewLoadSetup() {
        timerLabel.text = "00:00:00"
        todaysShiftTimeLabel.text = "00:00 - 00:00"
        positionLabel.text = "Position"
        jobTitleLabel.text = GlobalVariable.jobName
        showJobInfo()
        toggleAddJobBtn()
    }
    
    func popoverDismissed() {
        self.viewLoadSetup()
    }
    
    func introPopupWindows(popUp: Int){
        switch popUp {
        case 1:
            print("introPopupWindows case 1 triggered")
            print(GlobalVariable.PayPeriodLengthSet)
            if GlobalVariable.PayPeriodLengthSet == false {
                performSegue(withIdentifier: "toSetPayPeriodSegue", sender: AnyClass.self)
                GlobalVariable.PayPeriodLengthSet = true
            }
        case 2:
            if GlobalVariable.PayPeriodLength == 3 {
                if GlobalVariable.startOfPayPeriodSet == false {
                    popupWindows(popUp: 2)
                } else {
                    popupWindows(popUp: 3)
                }
            } else {
                popupWindows(popUp: 3)
            }
        case 3:
            if GlobalVariable.stateOfResidenceSet == false {
                popupWindows(popUp: popUp)
            }
        case 4:
            if GlobalVariable.numberOfAllowancesSet == false {
                popupWindows(popUp: popUp)
            }
        case 5:
            if GlobalVariable.filingStatusSet == false {
                popupWindows(popUp: popUp)
            }
        default:
            break
        }
    }
    
    func popupWindows(popUp: Int) {
        switch popUp {
        case 2:
            performSegue(withIdentifier: "toSetStartPayPeriodSegue", sender: AnyClass.self)
            GlobalVariable.startOfPayPeriodSet = true
        case 3:
            performSegue(withIdentifier: "toSetStateOfResidenceSegue", sender: AnyClass.self)
            GlobalVariable.stateOfResidenceSet = true
        case 4:
            performSegue(withIdentifier: "toSetAllowancesSegue", sender: AnyClass.self)
            GlobalVariable.numberOfAllowancesSet = true
        case 5:
            performSegue(withIdentifier: "toSetFilingStatusSegue", sender: AnyClass.self)
            GlobalVariable.filingStatusSet = true
        case 6:
            updateJobView()
            viewWillAppear(true)
        default:
            break
        }
    }
    func setDelay(popUp: Int) {
        let deadlineTime = DispatchTime.now() + 0.3 //Here is 0.3 second as per your requirement
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.introPopupWindows(popUp: popUp)
        }
    }
    
    func hideJobList() {
        if isJobListShown == false {
            if Data.JobName.count <= 1 {
                jobListBtn.isHidden = true
            } else if Data.JobName.count > 1 {
                jobListBtn.isHidden = false
                isJobListShown = true
            }
        }
    }
    
    func toggleAddJobBtn() {
        if Data.JobName.count == 4 {
            addJobBtn.isEnabled = false
        } else {
            addJobBtn.isEnabled = true
        }
    }
    
    func shiftClockIn() {
        if GlobalVariable.jobPositionArrayIsPopulated == false {
            performSegue(withIdentifier: "toAddPositionSegue", sender: AnyClass.self)
        } else {
            performSegue(withIdentifier: "toSelectPositionSegue", sender: AnyClass.self)
        }
        print("jobPositionArrayIsPopulated", GlobalVariable.jobPositionArrayIsPopulated)
    }
    
    func shiftClockOut() {
        timer.invalidate()
        clockOutTime = currentTime()
        todaysShiftTimeLabel.text = "\(clockInTime) - \(clockOutTime)"
        clockInBtn.isEnabled = true
        clockInBtn.alpha = 1
        clockOutBtn.isEnabled = false
        thisShiftClockOut = currentTime()
        performSegue(withIdentifier: "toCurrentShiftInfoSegue", sender: AnyClass.self)
    }
    
    func startTimerFunction() {
        clockInTime = self.currentTime()
        self.todaysShiftTimeLabel.text = ("\(clockInTime) - 00:00")
        thisShiftsDate = currentDate()
        thisShiftClockIn = self.currentTime()
        let startTime = Date()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
            let now = Date()
            let components = Calendar.current.dateComponents([.hour, .minute, .second], from: startTime, to: now)
            
            guard let hoursPortion = components.hour, let minutesPortion = components.minute, let secondsPortion = components.second
                else {
                    preconditionFailure("Should not happen")
            }
            let hours = String(format: "%02d", hoursPortion)
            let minutes = String(format: "%02d", minutesPortion)
            let seconds = String(format: "%02d", secondsPortion)
            self.timerLabel.text = "\(hours):\(minutes):\(seconds)"
            thisShiftHours = "\(hours).\(minutes)"
            self.clockInBtn.isEnabled = false
            self.clockInBtn.alpha = 0.50
            self.clockOutBtn.isEnabled = true
            
        }
    }
    
    func currentTime() -> String {
        formatter.dateFormat = "hh:mm a"
        let timeString = formatter.string(from: Date())
        return timeString
    }
    
    func currentDate() -> String {
        let date = Date()
        formatter.dateFormat = "MMMM dd,yyyy"
        
        let result = formatter.string(from: date)
        return result
    }
    
    
    // MARK: - UIActions
    //MARK: Buttons
    @IBAction func jobListBtnPressed(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            if self.isTableVisible == false {
                self.isTableVisible = true
//                self.blindCloseBtn.isHidden = false
                self.tblDropDownHC.constant = 30.0 * CGFloat(self.heightNum!)
            } else {
//                self.blindCloseBtn.isHidden = true
                self.tblDropDownHC.constant = 0
                self.isTableVisible = false
            }
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func clockInBtnPressed(_ sender: UIButton) {
        
        shiftClockIn()
    }
    
    @IBAction func addJobBtnPressed(_ sender: UIBarButtonItem) {
        updateJobView()
        performSegue(withIdentifier: "toAddJobSegues", sender: AnyClass.self)
    }
    
    @IBAction func clockOutBtnPressed(_ sender: UIButton) {
        
        shiftClockOut()
    }
    

    @IBAction func enterTipsBtnPressed(_ sender: UIButton) {
      
        if GlobalVariable.shiftTipsAdded == false {
            performSegue(withIdentifier: "toEnterShiftTipsSegue", sender: AnyClass.self)
        }
        
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddPositionSegue" {
            if let addPositionVC = segue.destination as? addPositionsViewController {
                addPositionVC.setBtnTitle = 1
                addPositionVC.doneSaving = { [weak self] in
                    guard let self = self else { return }
                    self.positionLabel.text = thisShiftPosition
                    self.startTimerFunction()
                }
            }
        }
        if segue.identifier == "toCurrentShiftInfoSegue" {
            GlobalVariable.currentShiftDate = currentDate()
        }
        if segue.identifier == "enterTipsSegue" {
            if let destinationVC = segue.destination as? shiftTipsTableViewController {
                destinationVC.sendingView = "Overview"
            }
        }
    }
    
    
    @IBAction func unwindFromAddVC(_ sender: UIStoryboardSegue) {
        if sender.source is setPayPeriodViewController {
            if let setPayPeriodVC = sender.source as? setPayPeriodViewController {
                print(setPayPeriodVC.payPeriodLengthStatus)
                GlobalVariable.shiftTipsAdded = false
                print("Length of Pay Period ",GlobalVariable.PayPeriodLength)
                print("Start of Pay Period ",GlobalVariable.startOfPayPeriodSet)
                setDelay(popUp: 2)
                
            }
        }
        if sender.source is changePositionViewController {
            if let setPositionVC = sender.source as? changePositionViewController {
                thisShiftPosition = setPositionVC.newPosition
                positionLabel.text = setPositionVC.newPosition
                startTimerFunction()
            }
        }
        if sender.source is setStartPayPeriodViewController {
            if let startPayPeriodVC = sender.source as? setStartPayPeriodViewController {
                GlobalVariable.startOfPayPeriodSet = startPayPeriodVC.startOfPayPeriodSet
                setDelay(popUp: 3)
            }
        }
        if let setStateOfResidenceVC = sender.source as? setStateOfResidenceViewController {
            GlobalVariable.stateOfResidenceSet = setStateOfResidenceVC.stateOfResedienceSet
            setDelay(popUp: 4)
        }
        if let allowancesVC = sender.source as? setAllowancesViewController {
            GlobalVariable.numberOfAllowancesSet = allowancesVC.numberOfAllowancesSet
            setDelay(popUp: 5)
            print("Pay Check unwind triggered")
        }
        if let filingStatusVC = sender.source as? setFilingStatusViewController {
            GlobalVariable.filingStatus = filingStatusVC.filingStatus
            popupWindows(popUp: 6)
        }
        if sender.source is shiftTipsTableViewController {
            if let shiftTipsVC = sender.source as? shiftTipsTableViewController {
                thisShiftChargeTip = shiftTipsVC.thisShiftCreditTip
                thisShiftCashTip = shiftTipsVC.thisShiftCashTip
                thisShiftTipOut = shiftTipsVC.thisShiftTipOut
                GlobalVariable.thisShiftsTipTotal = shiftTipsVC.thisShiftsTipTotal
                GlobalVariable.shiftTipsAdded = shiftTipsVC.ShiftTipsAdded
                GlobalVariable.isTipsEditing = shiftTipsVC.isTipsEditing
                updateJobView()
            }
        }
    }
}


// MARK: - Extensions
// MARK: Drop Menu
extension OverviewViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        heightNum = Double(Data.Jobs.count)
        return Data.Jobs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "jobNames") as! dropMenuTableViewCell
        
        let text = Data.Jobs[indexPath.row].Name
        print(Data.JobName)
        cell.jobNameTextLabel.text = text
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let jobIndexToEdit = GlobalVariable.jobIndex
        JobsFunctions.updateJobs(at: jobIndexToEdit, Name: GlobalVariable.jobName!)
        JobsFunctions.loadSelectedJob(index: indexPath.row)
        GlobalVariable.jobIndex = indexPath.row
        introPopupWindows(popUp: 1)
        viewWillAppear(true)
        
        UIView.animate(withDuration: 0.5) {
            self.tblDropDownHC.constant = 0
            self.isTableVisible = false
            self.view.layoutIfNeeded()
        }
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (contextualAction, view, actionPerformed: (Bool) -> ()) in
            //Perform delete
            JobsFunctions.deleteJobs(index: indexPath.row)
            tableView.reloadData()
            let jobIndexToEdit = GlobalVariable.jobIndex
            JobsFunctions.updateJobs(at: jobIndexToEdit, Name: GlobalVariable.jobName!)
            JobsFunctions.loadSelectedJob(index: indexPath.row)
            self.viewWillAppear(true)
            //tableView.deleteRows(at: [indexPath], with: .fade)
            
            UIView.animate(withDuration: 0.5) {
                self.tblDropDownHC.constant = 0
                self.isTableVisible = false
                self.view.layoutIfNeeded()
                self.hideJobList()
            }
            actionPerformed(true)
        }
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
}

// MARK: Chart Data
extension OverviewViewController {
    
    func setChart(dataPoints: [String], values: [Double]) {
        barChartView.noDataText = "You need to provide data for the chart."
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), yValues: [values[i]])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Pay Period")
        chartDataSet.colors = [UIColor.red]
        
        let chartData = BarChartData(dataSet: chartDataSet)
        
        barChartView.data = chartData
        barChartView.backgroundColor = UIColor.white
        
        chartData.notifyDataChanged()
        barChartView.notifyDataSetChanged()
        dataEntries.removeAll()
    }
    
}

