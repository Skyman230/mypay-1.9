//
//  dropMenuTableViewCell.swift
//  myPay
//
//  Created by Donald Scott on 5/9/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class dropMenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var jobNameTextLabel: UILabel!
    @IBOutlet weak var cellBkg: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
