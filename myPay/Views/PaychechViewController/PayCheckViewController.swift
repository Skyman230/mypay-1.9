//
//  PayCheckViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/27/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class PayCheckViewController: UIViewController, ModalTransitionListener {
    
    @IBOutlet weak var grossEarningsTotalTipsTextLabel: UILabel!
    @IBOutlet weak var grossEarningsTotalWageTextLabel: UILabel!
    @IBOutlet weak var overTimeTextLabel: UILabel!
    
    @IBOutlet weak var federalIncomeTaxesTextLabel: UILabel!
    @IBOutlet weak var socialSecurityTextLabel: UILabel!
    @IBOutlet weak var mediCareTextLabel: UILabel!
    @IBOutlet weak var stateIncomeTaxesTextLabel: UILabel!
    
    @IBOutlet weak var grossTotalWagesTextLabel: UILabel!
    @IBOutlet weak var totalTaxesTextLabel: UILabel!
    @IBOutlet weak var totalDeductionsTextLabel: UILabel!
    @IBOutlet weak var netPayTextLabel: UILabel!
    
    var warningText: String?
    
    @IBOutlet weak var summeryOutlineView: UIView!
    
    var allButtonsPressed = false {
        didSet{
            if let items = tabBarController?.tabBar.items {
                items[0].isEnabled = allButtonsPressed
                items[1].isEnabled = allButtonsPressed
                items[2].isEnabled = allButtonsPressed
                items[3].isEnabled = allButtonsPressed
                items[4].isEnabled = allButtonsPressed
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        summeryOutlineView.addDkGrayOutline()
        allButtonsPressed = true
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let deadlineTime = DispatchTime.now() + 0.2 //Here is 0.3 second as per your requirement
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.showFigures()
        }
        allButtonsPressed = true
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        ModalTransitionMediator.instance.setListener(listener: self)
    }

    func popoverDismissed() {
        self.showFigures()
    }
    
    func showFigures() {
        if payPeriodGrossTipArray.count == 0 {
//            errorWarningMessage(warningMessIndex: 1)
        }
        CalculateSums.showFigures()
        grossEarningsTotalTipsTextLabel.text = GlobalVariable.PayPeriodTips
        grossEarningsTotalWageTextLabel.text = GlobalVariable.PayPeriodWage
        overTimeTextLabel.text = ""
        
        federalIncomeTaxesTextLabel.text = GlobalVariable.federalIncomeTax
        socialSecurityTextLabel.text = GlobalVariable.SocialSecurity
        mediCareTextLabel.text = GlobalVariable.mediCare
        stateIncomeTaxesTextLabel.text = GlobalVariable.stateIncomeTax
        
        grossTotalWagesTextLabel.text = GlobalVariable.GrossWage
        totalTaxesTextLabel.text = GlobalVariable.totalWithholdings
        totalDeductionsTextLabel.text = GlobalVariable.totalDeductions
        netPayTextLabel.text = GlobalVariable.totalNet
        if GlobalVariable.Overtime.isEmpty == false {
            overTimeTextLabel.text = GlobalVariable.Overtime
        }
    }
    
    func errorWarningMessage(warningMessIndex: Int) {
        switch warningMessIndex {
        case 1:
            warningText = "Pease enter information in Shift Info to access this view. "
        case 2:
            warningText = "Please enter a Break Time"
        case 3:
            warningText = "Enter a Job in Settings"
        case 4:
            warningText = "Hourly Rate Needs to be Entered"
        case 5:
            warningText = "Hours Woprked Needs to be Entered"
        case 6:
            warningText = "Start Time Needs to be Entered"
        case 7:
            warningText = "End Time Needs to be Entered"
        default:
            return ()
        }
        performSegue(withIdentifier: "errorMessageSegue", sender: Any?.self)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
