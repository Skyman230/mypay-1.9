//
//  addJobViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class addJobViewController: UITableViewController {
    
    @IBOutlet weak var textFieldPlaceholder: UIView!
    @IBOutlet weak var jobTextField: UITextField!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    var doneSaving: (() -> ())?
    var jobIndexToEdit: Int?
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldPlaceholder.addOutline()
        jobTextField.becomeFirstResponder()
        
        if let index = jobIndexToEdit {
            let job = Data.Jobs[index]
            jobTextField.text = job.Name
        }
        saveBtn.isHidden = true
    }
    
    // MARK: - func
    func loadSelectedJob(index: Int) {
        GlobalVariable.jobName = Data.Jobs[index].Name
    }
    
    // MARK: - IBActions
    // MARK: TextField
    @IBAction func jobTextFieldEditingChanged(_ sender: UITextField) {
        if((sender.text?.count)! > 0 && !(sender.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            self.saveBtn.isHidden = false
        }
        else{
            self.saveBtn.isHidden = true
        }
        
    }
    
    // MARK: Buttons
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true)
    }
    
    @IBAction func saveBtnPressed(_ sender: UIButton) {
        if let doneSaving = doneSaving {
            
            guard jobTextField.hasValue, let newJob = jobTextField.text else {return}
            
            JobsFunctions.createJobs(JobsModel: JobsModel(Name: newJob, playInfo: true, currentShiftDate: "", jobsArrayPopulated: false, shiftTipsAdded: false, isTipsEditing: false, jobPositionArrayIsPopulated: false, PayPeriodTips: "$000.00", PayPeriodWage: "$000.00", PayPeriodHours: "00:00", MonthTips: "$000.00", MonthWage: "$000.00", MonthHour: "00:00", GrossWage: "$000.00", TotalDeductions: "$000.00", TotalWithholdings: "$000.00", TotalNet: "$000.00", PayPeriodLengthSet: false, PayPeriodLength: 0, startOfPayPeriodSet: false, stateOfResidenceSet: false, numberOfAllowancesSet: false, filingStatusSet: false, stateOfResidence: "", startOfPayPeriod: Date(), EndOfPayPeriod: Date(), StartOfWeekDate: Date(), newEndTime: Date(), clockInTime: Date(), filingStatus: 0, allowances: 0, thisShiftPosition: "", thisShiftRate: "$000.00", thisShiftClockIn: "00:00", thisShiftClockOut: "00:00", thisShiftHours: "00:00", thisShiftChargeTip: "$000.00", thisShiftCashTip: "$000.00", thisShiftTipOut: "$000.00", thisShiftTotal: "$000.00", thisShiftsHourlyWage: "$000.00", thisShiftsTipTotal: "$000.00", ShiftTipsAdded: "", clearSemiMonthlyArray: false, clearBiWeeklyArray: false, clearWeeklyArray: false, firstPayPeriodAdded: false, SemiMonthly: 0, WhichBiWeeklyPayPeriod: 0, Weekly: 0, startOfMonth: Date(), clearMonthlyTotals: false, monthlyTotals: 1, monthlyPayPeriodTotals: 1, federalIncomeTax: "$000.00", stateIncomeTax: "$000.00", mediCare: "$000.00", SocialSecurity: "$000.00", netGross: 00.00, netDeductions: 00.00, netWithholdings: 00.00, CurrentMonthEditing: 1, dateString: "", revisedEndTime: Date(), thisShiftNotes: "No Notes Added", ShiftNotesAdded: false, isShiftNotesEditing: false, Overtime: "$000.00", ArchiveMonthTips: "$000.00", ArchiveMonthWage: "$000.00", ArchiveMonthHour: "00.00", ArchiveMonthGrossWage: "$000.00", yearlyTotal: "$000.00", yearlyTips: "$000.00", yearlyWage: "$000.00", retirement401KPercentage: "", retirement401KAmount: "$000.00", retirement401KRothPercentage: "", retirement401KRothAmount: "$000.00", deductionPercentage: "0.00", totalRetirementSum: "$000.00", totalInsuranceSum: "$000.00", totalAdditionalDeductionSum: "$000.00", medicalInsurance: "$000.00", medicalSavingsAccount: "$000.00", dentalInsurance: "$000.00", visionInsurance: "$000.00", retirement401Kadded: false, retirement401KRothadded: false, voluntaryDeductionArrayIsPopulated: false, DeductionArrayIsPopulated: false, positionHeight: 0, taxesHeight: 0, positionNum: 0, retirementHeight: 0, insuranceEditing: 0, deductionHeight: 0))
            if GlobalVariable.jobsArrayPopulated == false {
                loadSelectedJob(index: 0)
                GlobalVariable.jobIndex = 0
                GlobalVariable.jobsArrayPopulated = true
                print(GlobalVariable.jobName!)
            }
            
            doneSaving()
        }
        
        dismiss(animated: true)
        
    }
}
