//
//  JobsViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class JobsViewController: UIViewController {
    
    // MARK: - UIOutlets
    // MARK: TableView
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: UIButtons
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    
    // MARK: UIView
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var backgroundView1: UIView!
    @IBOutlet weak var backgroundView2: UIView!
    @IBOutlet weak var backgroundView3: UIView!
    @IBOutlet weak var backgroundView4: UIView!
    
    // MARK: - Variables
    // MARK: Int
    var jobIndexToEdit: Int?
    
    
    // MARK: - Load Views
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        JobsFunctions.readJobNames(completion: { [weak self] in
            
            self?.tableView.reloadData()
            
        })
        showCancelBtn()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        showCancelBtn()
    }
    
    // MARK: - Functions
    
    func showCancelBtn() {
        let jobsNum = Data.Jobs.count
        switch jobsNum {
        case 0:
            cancelBtn.isHidden = false
            continueBtn.isHidden = true
            addBtn.isHidden = false
            backgroundView.isHidden = false
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
        case 1:
            cancelBtn.isHidden = true
            continueBtn.isHidden = false
            addBtn.isHidden = false
            backgroundView.isHidden = true
            backgroundView1.isHidden = false
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
        case 2:
            cancelBtn.isHidden = true
            continueBtn.isHidden = false
            addBtn.isHidden = false
            backgroundView.isHidden = true
            backgroundView1.isHidden = true
            backgroundView2.isHidden = false
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
        case 3:
            cancelBtn.isHidden = true
            continueBtn.isHidden = false
            addBtn.isHidden = false
            backgroundView.isHidden = true
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = false
            backgroundView4.isHidden = true
        case 4:
            addBtn.isHidden = true
            backgroundView.isHidden = true
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = false
        default:
            break
        }
    }
    
    // MARK: - UIActions
    // MARK: Buttons
    @IBAction func continueBtnPressed(_ sender: UIButton) {
        JobsFunctions.loadSelectedJob(index: GlobalVariable.jobIndex)
        performSegue(withIdentifier: "toJobSegue", sender: nil)
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddJobSegue" {
            let popup = segue.destination as! addJobViewController
            popup.doneSaving = { [weak self] in
                self?.tableView.reloadData()
                GlobalVariable.playInfo = false
                self?.showCancelBtn()
            }
        }
        if segue.identifier == "toEditJobSegue" {
            let popup = segue.destination as! editViewController
            popup.jobIndexToEdit = self.jobIndexToEdit
            popup.doneSaving = { [weak self] in
                self?.tableView.reloadData()
                self?.showCancelBtn()
            }
        }
    }
}

// MARK: - Extensions

extension JobsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.Jobs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "jobNameCell") as! JobsTableViewCell
        
        cell.setup(JobsModel: Data.Jobs[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (contextualAction, view, actionPerformed: (Bool) -> ()) in
            //Perform delete
            JobsFunctions.deleteJobs(index: indexPath.row)
            tableView.reloadData()
            //tableView.deleteRows(at: [indexPath], with: .fade)
            self.showCancelBtn()
            actionPerformed(true)
        }
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = UIContextualAction(style: .normal, title: "Edit", handler: { (contextualAction, view, actionPerformed: (Bool) -> ()) in
            self.jobIndexToEdit = indexPath.row
            self.performSegue(withIdentifier: "toEditJobSegue", sender: nil)
            actionPerformed(true)
        })
        
        edit.backgroundColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        return UISwipeActionsConfiguration(actions: [edit])
    }
}
