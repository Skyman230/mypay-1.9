//
//  JobsTableViewCell.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class JobsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        background.addOutline()
        titleLabel.font = UIFont(name: Theme.mainFontName, size: 20)
        
        // Configure the view for the selected state
    }
    
    func setup(JobsModel: JobsModel) {
        titleLabel.text = JobsModel.Name
    }
}
