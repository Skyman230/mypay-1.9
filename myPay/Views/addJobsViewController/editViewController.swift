//
//  editViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class editViewController: UITableViewController {
    
    @IBOutlet weak var textFieldPlaceholder: UIView!
    @IBOutlet weak var jobTextField: UITextField!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    var doneSaving: (() -> ())?
    var jobIndexToEdit: Int?
    var jobsIndex:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldPlaceholder.addOutline()
        jobTextField.becomeFirstResponder()
        
        if let index = jobIndexToEdit {
            let job = Data.Jobs[index]
            jobTextField.text = job.Name
        }
    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true)
    }
    
    @IBAction func saveBtnPressed(_ sender: UIButton) {
        if let doneSaving = doneSaving {
            
            guard jobTextField.text?.isEmpty == false, let newJob = jobTextField.text else {
                
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
                imageView.image = #imageLiteral(resourceName: "Warning")
                imageView.contentMode = .scaleAspectFill
                textFieldPlaceholder.layer.borderColor = Theme.Warning?.cgColor
                textFieldPlaceholder.layer.borderWidth = 2
                jobTextField.rightView = imageView
                jobTextField.rightViewMode = .unlessEditing
                
                return
            }
            JobsFunctions.updateJobs(at: jobIndexToEdit!, Name: newJob)
            
            doneSaving()
        }
        dismiss(animated: true)
    }
}
