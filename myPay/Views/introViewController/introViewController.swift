//
//  introViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class introViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        playInfo()
    }
    
    func playInfo() {
        if GlobalVariable.playInfo == true {
            performSegue(withIdentifier: "introSegue", sender: Any?.self)
        } else {
            performSegue(withIdentifier: "enterJobSegue", sender: Any?.self)
        }
    }}
