//
//  shiftNotesTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/22/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class shiftNotesTableViewController: UITableViewController, UITextViewDelegate {

    // MARK: - UIOutlets
    // MARK: TextView
    @IBOutlet weak var shiftNotesTextView: UITextView!
    @IBOutlet weak var headerText: UILabel!
    
    // MARK: Buttons
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var endEditModeBtn: UIButton!
    
    // MARK: - vars
    // MARK: String
    var shiftNotes: String?
    var shiftNotesStatus: String?
    
    // MARK: Bool
    var shiftNotesAdded = GlobalVariable.ShiftNotesAdded
    var isShiftNotesEditing = GlobalVariable.isShiftNotesEditing
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shiftNotesTextView.becomeFirstResponder()
        shiftNotesTextView.delegate = self
        editShiftNotes()
    }
    
    // MARK: - func
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.shiftNotesTextView.resignFirstResponder()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if((textView.text?.count)! > 0 && !(textView.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            self.saveBtn.isHidden = false
        }
        else{
            self.saveBtn.isHidden = true
        }
    }
    
    func editShiftNotes() {
        if isShiftNotesEditing == true {
            let shiftNotes = GlobalVariable.thisShiftNotes
            shiftNotesTextView.text = shiftNotes
            headerText.text = "Your Notes For This Shift"
            shiftNotesTextView.isEditable = false
            editBtn.isHidden = false
            editBtn.isEnabled = true
            saveBtn.isHidden = true
            endEditModeBtn.isHidden = true
            cancelBtn.isHidden = false
        } else {
            editBtn.isHidden = true
            endEditModeBtn.isHidden = true
            cancelBtn.isHidden = false
            saveBtn.isHidden = true
            headerText.text = "Enter Notes About The Shift"
        }
    }
    // MARK: - IBActions
    // MARK: Buttons
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func editBtnPressed(_ sender: UIButton) {
        headerText.text = "Edit Notes For This Shift"
        shiftNotesTextView.isEditable = true
        shiftNotesTextView.becomeFirstResponder()
        saveBtn.isHidden = false
        editBtn.alpha = 0.60
        editBtn.isEnabled = false
        endEditModeBtn.isHidden = false
        cancelBtn.isHidden = true
        saveBtn.isHidden = false
    }
    @IBAction func endEditModeBtnPressed(_ sender: UIButton) {
        headerText.text = "Your Notes For This Shift"
        shiftNotesTextView.isEditable = false
        shiftNotesTextView.resignFirstResponder()
        saveBtn.isHidden = true
        editBtn.alpha = 1
        editBtn.isEnabled = true
        endEditModeBtn.isHidden = true
        cancelBtn.isHidden = false
        saveBtn.isHidden = true
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if((shiftNotesTextView.text?.count)! > 0 && !(shiftNotesTextView.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            shiftNotesStatus = "Notes Added"
            shiftNotes = shiftNotesTextView.text
            GlobalVariable.thisShiftNotes = shiftNotesTextView.text
            isShiftNotesEditing = true
            GlobalVariable.isShiftNotesEditing = true
        } else {
            shiftNotesStatus = "No Notes"
        }
    }
    
}
