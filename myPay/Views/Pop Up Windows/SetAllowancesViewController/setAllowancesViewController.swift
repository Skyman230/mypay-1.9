//
//  setAllowancesViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/9/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class setAllowancesViewController: UIViewController {
    
    // MARK: - UIOutlets
    // MARK: UIButtons
    @IBOutlet weak var set1Allowance1Btn: UIButton!
    @IBOutlet weak var set2AllowancesBtn: UIButton!
    @IBOutlet weak var set3AllowancesBtn: UIButton!
    @IBOutlet weak var set4AllowancesBtn: UIButton!
    @IBOutlet weak var set5AllowancesBtn: UIButton!
    @IBOutlet weak var set6AllowancesBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    
    var allowances: Bool = false
    var numberOfAllowancesSet = false
    
    // MARK: - Load Views
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueBtn.isEnabled = false
        continueBtn.alpha = 0.60
        
    }
    
    // MARK: - functions
    
    func resetBtnColor(){
        set1Allowance1Btn.setTitleColor(.white, for: .normal)
        set2AllowancesBtn.setTitleColor(.white, for: .normal)
        set3AllowancesBtn.setTitleColor(.white, for: .normal)
        set4AllowancesBtn.setTitleColor(.white, for: .normal)
        set5AllowancesBtn.setTitleColor(.white, for: .normal)
        set6AllowancesBtn.setTitleColor(.white, for: .normal)
    }
    
    func setAllowances(num: Int) {
        resetBtnColor()
        allowances = true
        GlobalVariable.allowances = num
        GlobalVariable.numberOfAllowancesSet = true
        continueBtn.isEnabled = true
        continueBtn.alpha = 1
        switch num {
        case 1:
            set1Allowance1Btn.setTitleColor(Theme.Accent, for: .normal)
        case 2:
            set2AllowancesBtn.setTitleColor(Theme.Accent, for: .normal)
        case 3:
            set3AllowancesBtn.setTitleColor(Theme.Accent, for: .normal)
        case 4:
            set4AllowancesBtn.setTitleColor(Theme.Accent, for: .normal)
        case 5:
            set5AllowancesBtn.setTitleColor(Theme.Accent, for: .normal)
        case 6:
            set6AllowancesBtn.setTitleColor(Theme.Accent, for: .normal)
        default:
            break
        }
    }
    
    // MARK: - UIActions
    // MARK: Buttons
    @IBAction func set1AllowancesBtnPressed(_ sender: UIButton) {
        setAllowances(num: 1)
        
    }
    @IBAction func set2AllowancesBtnPressed(_ sender: UIButton) {
        setAllowances(num: 2)
        
    }
    @IBAction func set3AllowancesBtnPressed(_ sender: UIButton) {
        setAllowances(num: 3)
        
    }
    @IBAction func set4AllowancesBtnPressed(_ sender: UIButton) {
        setAllowances(num: 4)
        
    }
    @IBAction func set5AllowancesBtnPressed(_ sender: UIButton) {
        setAllowances(num: 5)
        
    }
    @IBAction func set6AllowancesBtnPressed(_ sender: UIButton) {
        setAllowances(num: 6)
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let allowancesStatus = allowances
        numberOfAllowancesSet = allowancesStatus
    }
}
