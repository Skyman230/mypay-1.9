//
//  shiftTipsTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/12/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class shiftTipsTableViewController: UITableViewController {
    
    
    // MARK: - UIOutlets
    // MARK: TextField
    @IBOutlet weak var chargeTipTextField: UITextField!
    @IBOutlet weak var cashTipTextField: UITextField!
    @IBOutlet weak var tipOutTextField: UITextField!
    // MARK: UIButton
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    
    // MARK: - Variables
    // MARK: String
    var sendingView: String?
    var thisShiftsTipTotal: String?
    var thisShiftCreditTip: String?
    var thisShiftCashTip: String?
    var thisShiftTipOut: String?
    // MARK: Double
    var charge: Double!
    var cash: Double!
    var tipOut: Double!
    // MARK: Bool
    var ShiftTipsAdded = GlobalVariable.shiftTipsAdded
    var isTipsEditing = GlobalVariable.isTipsEditing
    
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        chargeTipTextField.becomeFirstResponder()
        saveBtn.isHidden = true
        editTips()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        chargeTipTextField.becomeFirstResponder()
        editTips()
    }
    
    // MARK: - func
    func sumOfTips() {
        let chargeTip = chargeTipTextField.text
        let cashTip = cashTipTextField.text
        let tippedOut = tipOutTextField.text
        if chargeTip != nil {
            charge = (chargeTip! as NSString).doubleValue
        } else {
            charge = 000.00
        }
        if cashTip != nil {
            cash = (cashTip! as NSString).doubleValue
        } else {
            cash = 000.00
        }
        if tippedOut != nil {
            tipOut = (tippedOut! as NSString).doubleValue
        } else {
            tipOut = 000.00
        }
        
        let tipTotal = calculateTip(chargetip: charge, cashtip: cash, tipout: tipOut)
        totalTips = convertDoubleToCurrency(amount: tipTotal)
        let chargeTipTotal = convertDoubleToCurrency(amount: charge)
        GlobalVariable.thisShiftChargeTip = chargeTipTotal
        let cashTipTotal = convertDoubleToCurrency(amount: cash)
        GlobalVariable.thisShiftCashTip = cashTipTotal
        let tipOutTotal = convertDoubleToCurrency(amount: tipOut)
        GlobalVariable.thisShiftTipOut = tipOutTotal
    }
    
    func calculateTip(chargetip: Double, cashtip: Double, tipout: Double) -> Double {
        return (chargetip + cashtip) - tipout
    }
    func editTips() {
        if isTipsEditing == true {
            let chargeTips = GlobalVariable.thisShiftChargeTip
            let editChargeTip = convertCurrencyToDouble(input: chargeTips!)
            chargeTipTextField.text = String(format: "%.2f",editChargeTip ?? 000.00)
            let cashTips = GlobalVariable.thisShiftCashTip
            let editCashTip = convertCurrencyToDouble(input: cashTips!)
            cashTipTextField.text = String(format: "%.2f",editCashTip ?? 000.00)
            let tipOut = GlobalVariable.thisShiftTipOut
            let editTipOut = convertCurrencyToDouble(input: tipOut!)
            tipOutTextField.text = String(format: "%.2f",editTipOut ?? 000.00)
            saveBtn.isHidden = false
        }
    }
   
    // MARK: - UIActions
    // MARK: Buttons
    @IBAction func cancellBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true)
    }
    
    // MARK: textFields
    @IBAction func chargeTipFieldEditingDidBegin(_ sender: UITextField) {
         checkWhitespace(sender)
    }
    
    @IBAction func cashTextFieldEditingDidBegin(_ sender: UITextField) {
         checkWhitespace(sender)
        
    }
    
    fileprivate func checkWhitespace(_ sender: UITextField) {
        if((sender.text?.count)! > 0 && !(sender.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            self.saveBtn.isHidden = false
        }
        else{
            self.saveBtn.isHidden = true
        }
    }
    
    @IBAction func tippedOutTextFieldEditingDidBegin(_ sender: UITextField) {
        checkWhitespace(sender)
        
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        sumOfTips()
        let total = totalTips
        thisShiftsTipTotal = total
        self.thisShiftsTipTotal = total
        let charge = cashTipTextField.text
        let cash = cashTipTextField.text
        let tipOut = tipOutTextField.text
        thisShiftCreditTip = charge
        thisShiftCashTip = cash
        thisShiftTipOut = tipOut
        isTipsEditing = true
        isSaveBtnActive = true
        GlobalVariable.isTipsEditing = true
        if sendingView == "Overview" {
            ShiftTipsAdded = true
            thisShiftsTipTotal = totalTips
            thisShiftCreditTip = GlobalVariable.thisShiftChargeTip
            thisShiftCashTip = GlobalVariable.thisShiftCashTip
            thisShiftTipOut = GlobalVariable.thisShiftTipOut
        } else {
            ShiftTipsAdded = false
            thisShiftsTipTotal = totalTips
            thisShiftCreditTip = GlobalVariable.thisShiftChargeTip
            thisShiftCashTip = GlobalVariable.thisShiftCashTip
            thisShiftTipOut = GlobalVariable.thisShiftTipOut
        }
        view.endEditing(true)
    }
}
