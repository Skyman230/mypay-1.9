//
//  setFilingStatusViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/9/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class setFilingStatusViewController: UIViewController {
    
    
    @IBOutlet weak var setFilingSingleBtn: UIButton!
    @IBOutlet weak var setFilingMarreidSingleBtn: UIButton!
    @IBOutlet weak var setFilingMarriedJoint: UIButton!
    @IBOutlet weak var setFilingHeadOfHouseholdBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    
    // MARK: variables
    var filingStatusSet = false
    var filingStatusSelected = false
    var filingAs = 0
    var filingStatus = 0
    
    
    // MARK: - Load Views
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueBtn.isEnabled = false
        continueBtn.alpha = 0.60
    }
    
    // MARK: - Functions
    func resetBtnColor(){
        setFilingSingleBtn.setTitleColor(.white, for: .normal)
        setFilingMarreidSingleBtn.setTitleColor(.white, for: .normal)
        setFilingMarriedJoint.setTitleColor(.white, for: .normal)
        setFilingHeadOfHouseholdBtn.setTitleColor(.white, for: .normal)
        
    }
    
    func setFilingStatus(stat: Int) {
        resetBtnColor()
        GlobalVariable.filingStatus = stat
        filingStatusSelected = true
        GlobalVariable.filingStatusSet = filingStatusSelected
        continueBtn.isEnabled = true
        continueBtn.alpha = 1
        switch stat {
        case 1:
            setFilingSingleBtn.setTitleColor(Theme.Accent, for: .normal)
        case 2:
            setFilingMarreidSingleBtn.setTitleColor(Theme.Accent, for: .normal)
        case 3:
            setFilingMarriedJoint.setTitleColor(Theme.Accent, for: .normal)
        case 4:
            setFilingHeadOfHouseholdBtn.setTitleColor(Theme.Accent, for: .normal)
        default:
            break
        }
    }
    
    // MARK: - UIActions
    // MARK: Buttons
    @IBAction func setFilingSingleBtnPressed(_ sender: UIButton) {
        setFilingStatus(stat: 1)
        filingAs = 1
    }
    @IBAction func setFilingMarriedSingleBtnPressed(_ sender: UIButton) {
        setFilingStatus(stat: 2)
        filingAs = 2
    }
    @IBAction func setFilingMarriedJointBtnPressed(_ sender: UIButton) {
        setFilingStatus(stat: 3)
        filingAs = 3
    }
    @IBAction func setFilingHeadOfHouseholdBtnPressed(_ sender: UIButton) {
        setFilingStatus(stat: 4)
        filingAs = 4
    }
    
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        filingStatusSet = filingStatusSelected
        filingStatus = filingAs
    }
}
