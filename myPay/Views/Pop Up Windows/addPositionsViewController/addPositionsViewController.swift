//
//  addPositionsViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/12/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class addPositionsViewController: UITableViewController {
    
    // MARK: - UIOutlet
    // MARK: UIView
    @IBOutlet weak var positionTextFieldPlaceholder: UIView!
    @IBOutlet weak var hourlyRateTextFieldPlacholder: UIView!
    
    // MARK: UITextField
    @IBOutlet weak var poisitionTextField: UITextField!
    @IBOutlet weak var hourlyRateTextField: UITextField!
    
    // MARK: UIButton
    @IBOutlet weak var continueBn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    // MARK: - Var
    var jobID = GlobalVariable.UniqueID
    var jobModel: JobsModel?
    
    var doneSaving: (() -> ())?
    var jobIndex = GlobalVariable.jobIndex
    var shiftRate = String()
    var shiftPosition = String()
    
    var positionTextfieldPopulated = false
    var hourlyRateTextFieldPopulated = false
    
    var setBtnTitle = 1
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        JobsFunctions.readJob(by: jobID) { [weak self](model) in
            guard let self = self else { return }
            self.jobModel = model
        }
        changeBtnTitle()
        poisitionTextField.becomeFirstResponder()
        continueBn.isHidden = true
        
    }
    
    // MARK: - func
    func loadSelectedJob(index: Int) {
        GlobalVariable.jobName = Data.Jobs[index].Name
    }
    
    func toggleContinueBtn() {
        if positionTextfieldPopulated == true {
            if hourlyRateTextFieldPopulated == true{
                continueBn.isHidden = false
            } else {
                continueBn.isHidden = true
            }
        } else {
            continueBn.isHidden = true
        }
        
    }
    func changeBtnTitle() {
        switch setBtnTitle {
        case 1:
            continueBn.setTitle("Clock In", for: .normal)
        case 2:
            continueBn.setTitle("Continue", for: .normal)
        case 3:
            continueBn.setTitle("Add Position", for: .normal)
        default:
            break
        }
        
    }
    
    // MARK: - IBActions
    // MARK: Textfields
    @IBAction func positionTextFieldEditingChanged(_ sender: UITextField) {
        if((sender.text?.count)! > 0 && !(sender.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            self.positionTextfieldPopulated = true
            toggleContinueBtn()
        }
        else{
            self.positionTextfieldPopulated = false
            toggleContinueBtn()
        }
    }
    
    @IBAction func hourlyRateTextFieldEditingChanged(_ sender: UITextField) {
        if((sender.text?.count)! > 0 && !(sender.text!.trimmingCharacters(in: .whitespaces)).isEmpty){
            self.hourlyRateTextFieldPopulated = true
            toggleContinueBtn()
        }
        else{
            self.hourlyRateTextFieldPopulated = false
            toggleContinueBtn()
        }
    }
    
    // MARK: Buttons
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true)
    }
    
    @IBAction func saveBtnPressed(_ sender: UIButton) {
        if let doneSaving = doneSaving {
            let newPosition = poisitionTextField.text
            let newHourlyRate = hourlyRateTextField.text
            
            thisShiftPosition = newPosition
            shiftPosition = newPosition!
            let hourlyRate = ("$" + newHourlyRate!)
            thisShiftRate = hourlyRate
            shiftRate = hourlyRate
            PositionFunctions.createPositions(at: jobIndex, using: PositionModel(position: newPosition!, hourlyRate: thisShiftRate!))
            
            doneSaving()
            
        }
        GlobalVariable.jobPositionArrayIsPopulated = true
        dismiss(animated: true)
        
    }
    
}
