//
//  currentShiftInfoTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/12/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class currentShiftInfoTableViewController: UITableViewController {
    
    
    // MARK: - UIOutlets
    // MARK: UIView
    @IBOutlet weak var contentView1: UIView!
    @IBOutlet weak var contentView2: UIView!
    @IBOutlet weak var contentView3: UIView!
    
    // MARK: UIImageView
    @IBOutlet weak var tipsDownArrow: UIImageView!
    @IBOutlet weak var tipsUpArrow: UIImageView!
    @IBOutlet weak var breakDownArrow: UIImageView!
    @IBOutlet weak var breakUpArrow: UIImageView!
    @IBOutlet weak var shiftNotesDownArrow: UIImageView!
    @IBOutlet weak var shiftNotesUpArrow: UIImageView!
    
    // MARK: UILabel
    @IBOutlet weak var shiftDateLabel: UILabel!
    @IBOutlet weak var shiftTotalLabel: UILabel!
    @IBOutlet weak var shiftTipTotalLabel: UILabel!
    @IBOutlet weak var shiftPositionLabel: UILabel!
    @IBOutlet weak var shiftHourlyRateLabel: UILabel!
    @IBOutlet weak var shiftStartTimeLabel: UILabel!
    @IBOutlet weak var shiftEndTimeLabel: UILabel!
    @IBOutlet weak var shiftBreakLabel: UILabel!
    @IBOutlet weak var shiftBreakTimeLabel: UILabel!
    @IBOutlet weak var shiftHoursLabel: UILabel!
    @IBOutlet weak var shiftTotalHourlyWageLabel: UILabel!
    @IBOutlet weak var shiftNotesLabel: UILabel!
    @IBOutlet weak var tipsButtonLabel: UILabel!
    
    // MARK: UITableView
    @IBOutlet var shiftTableView: UITableView!
    
    // MARK: UIButton
    @IBOutlet weak var addTipsBtn: UIButton!
    @IBOutlet weak var finishClockOutBtn: UIButton!
    
    // MARK: IndexPath
    let contentView1CellIndexPath = IndexPath(row: 4, section: 0)
    let contentView2CellIndexPath = IndexPath(row: 12, section: 0)
    let contentView3CellIndexPath = IndexPath(row: 17, section: 0)
    
    // MARK: - Vars
    // Mark: String
    var thisShiftNotes = "No Notes"
    var thisShiftChargeTip: String?
    var thisShiftsDate = GlobalVariable.currentShiftDate
    
    
    // MARK: UUID
    var jobID = GlobalVariable.UniqueID
    var payPeriodID = GlobalVariable.UniqueID
    var jobModel: JobsModel?
    
//    var doneSaving: (() -> ())?
    
    // MARK: Int
    var addToPayPeriod = 1
    var jobIndex = GlobalVariable.jobIndex
    
    // MARK: DateFormatter
    let dateFormatter = DateFormatter()
    
    // MARK: Date
    let date = GlobalVariable.StartOfPayPeriod
    
    
    // MARK: Bool
    var isContentView1Shown: Bool = false {
        didSet{
            contentView1.isHidden = !isContentView1Shown
        }
    }
    var isContentView2Shown: Bool = false {
        didSet{
            contentView2.isHidden = !isContentView2Shown
        }
    }
    var isContentView3Shown: Bool = false {
        didSet{
            contentView3.isHidden = !isContentView3Shown
        }
    }
    
    // MARK: - Load Views
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shiftDateLabel.text = GlobalVariable.currentShiftDate
        getCurrentJob()
        isTipsEditing = GlobalVariable.isTipsEditing
        ShiftTipsAdded = GlobalVariable.shiftTipsAdded
        shiftTipTotalLabel.text = GlobalVariable.thisShiftsTipTotal
        shiftPositionLabel.text = thisShiftPosition
        shiftHourlyRateLabel.text = thisShiftRate
        shiftHoursLabel.text = thisShiftHours
        shiftStartTimeLabel.text = clockInTime
        shiftEndTimeLabel.text = clockOutTime
        updateFigures()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        shiftDateLabel.text = GlobalVariable.currentShiftDate
        getCurrentJob()
        isTipsEditing = GlobalVariable.isTipsEditing
        ShiftTipsAdded = GlobalVariable.shiftTipsAdded
        shiftPositionLabel.text = thisShiftPosition
        shiftHourlyRateLabel.text = thisShiftRate
        shiftHoursLabel.text = thisShiftHours
        shiftStartTimeLabel.text = clockInTime
        shiftEndTimeLabel.text = clockOutTime
        updateFigures()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        ModalTransitionMediator.instance.sendPopoverDismissed(modelChanged: true)
        
    }
    
    
    
    //MARK: - Height For Row At (Table View Data Source)
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (contentView1CellIndexPath.section, contentView1CellIndexPath.row):
            if isContentView1Shown {
                return 44
            } else {
                return 0
            }
        case (contentView2CellIndexPath.section, contentView2CellIndexPath.row):
            if isContentView2Shown {
                return 44
            } else {
                return 0
            }
        case (contentView3CellIndexPath.section, contentView3CellIndexPath.row):
            if isContentView3Shown {
                return 44
            } else {
                return 0
            }
        default:
            if indexPath.row == 2 {
                return 54
            }
            if indexPath.row == 5 {
                return 54
            }
            if indexPath.row == 8 {
                return 54
            }
            if indexPath.row == 15 {
                return 54
            }
            if indexPath.row == 18 {
                return 54
            }
        }
        return 44
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == contentView1CellIndexPath.section &&
            indexPath.row == contentView1CellIndexPath.row - 1 {
            isContentView2Shown = false
            isContentView3Shown = false
            if isContentView1Shown == true {
                isContentView1Shown = false
                resetArrows()
                tipsDownArrow.isHidden = false
            } else {
                isContentView1Shown = true
                resetArrows()
                tipsUpArrow.isHidden = false
                tipsDownArrow.isHidden = true
                if isTipsEditing == true {
                    tipsButtonLabel.text = "Edit Tips"
                } else {
                    tipsButtonLabel.text = "Add Tips"
                }
            }
        }
        if indexPath.section == contentView2CellIndexPath.section &&
            indexPath.row == contentView2CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView3Shown = false
            let elapsedTime = GlobalVariable.clockInTime.timeIntervalSince(GlobalVariable.clockInTime)
            let hours = floor(elapsedTime / 60 / 60)
            if hours > 1 {
                shiftBreakLabel.textColor = UIColor.white
                shiftBreakTimeLabel.textColor = UIColor.white
                breakDownArrow.isHidden = false
            } else {
                shiftBreakLabel.textColor = UIColor.darkGray
                shiftBreakTimeLabel.textColor = UIColor.darkGray
                shiftBreakTimeLabel.text = "No Break"
                breakDownArrow.isHidden = true
            }
            if isContentView2Shown == true {
                isContentView2Shown = false
                resetArrows()
                breakDownArrow.isHidden = false
            } else {
                if hours > 1 {
                isContentView2Shown = true
                resetArrows()
                breakUpArrow.isHidden = false
                breakDownArrow.isHidden = true
                }
            }
        }
        if indexPath.section == contentView3CellIndexPath.section &&
            indexPath.row == contentView3CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            if isContentView3Shown == true {
                isContentView3Shown = false
                resetArrows()
                shiftNotesDownArrow.isHidden = false
            } else {
                isContentView3Shown = true
                resetArrows()
                shiftNotesUpArrow.isHidden = false
                shiftNotesDownArrow.isHidden = true
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    // MARK: - func
    func updateFigures() {
        let elapsedTime = GlobalVariable.clockInTime.timeIntervalSince(GlobalVariable.clockInTime)
        let hours = floor(elapsedTime / 60 / 60)
        if hours > 1 {
            shiftBreakLabel.textColor = UIColor.white
            shiftBreakTimeLabel.textColor = UIColor.white
            breakDownArrow.isHidden = false
        } else {
            shiftBreakLabel.textColor = UIColor.darkGray
            shiftBreakTimeLabel.textColor = UIColor.darkGray
            shiftBreakTimeLabel.text = "No Break"
            breakDownArrow.isHidden = true
        }
        if isTipsEditing == true {
        calculateTotalWage()
        calculateTotalOfShift()
        }
        calculateTotalWage()
        calculateTotalOfShift()
    }
    
    func getCurrentJob() {
        JobsFunctions.readJob(by: jobID) { [weak self](model) in
            guard let self = self else { return }
            self.jobModel = model
        }
    }
    
    func insertBreakMin(bMin: Int) {
        
        GlobalVariable.formatter.dateFormat = "h:mma"
        let clockInTimeString = clockInTime
        let currentClockInTime = GlobalVariable.formatter.date(from: clockInTimeString)
        GlobalVariable.clockInTime = currentClockInTime!
        
        let currentEndTimeString = clockOutTime
        let currentEndTime = GlobalVariable.formatter.date(from: currentEndTimeString)
        switch bMin {
        case 0:
            let newEndTime = currentEndTime!.addingTimeInterval(TimeInterval(-0.0 * 60.0))
            GlobalVariable.newEndTime = newEndTime
        case 15:
            let newEndTime = currentEndTime!.addingTimeInterval(TimeInterval(-15.0 * 60.0))
            GlobalVariable.newEndTime = newEndTime
        case 30:
            let newEndTime = currentEndTime!.addingTimeInterval(TimeInterval(-30.0 * 60.0))
            GlobalVariable.newEndTime = newEndTime
        case 45:
            let newEndTime = currentEndTime!.addingTimeInterval(TimeInterval(-45.0 * 60.0))
            GlobalVariable.newEndTime = newEndTime
        case 60:
            let newEndTime = currentEndTime!.addingTimeInterval(TimeInterval(-60.0 * 60.0))
            GlobalVariable.newEndTime = newEndTime
        default:
            return ()
        }
        updateTimeView()
    }
    func updateTimeView() {
        
        let elapsedTime = GlobalVariable.newEndTime.timeIntervalSince(GlobalVariable.clockInTime)
        let hours = floor(elapsedTime / 60 / 60)
        let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
        if hours < 0 {
            let hour = 24 + hours
            shiftHoursLabel.text = ("\(Int(hour)).\(Int(minutes))")
        } else {
            shiftHoursLabel.text = ("\(Int(hours)).\(Int(minutes))")
        }
        GlobalVariable.thisShiftsTipTotal = shiftHoursLabel.text!
        calculateTotalWage()
        calculateTotalOfShift()
    }
    
    func calculateHourlyWage() {
        guard let hourlyRate = convertCurrencyToDouble(input: shiftHourlyRateLabel.text!) else {
            print("Not a Number")
            return
        }
        let hours = (shiftHoursLabel.text! as NSString).doubleValue
        let x = calculateWage(hourlyRate: hourlyRate, hours: hours)
        shiftTotalLabel.text = convertDoubleToCurrency(amount: x)
    }
    func calculateTotalWage() {
        let hours = (shiftHoursLabel.text! as NSString).doubleValue
        let rate = convertCurrencyToDouble(input: shiftHourlyRateLabel.text!)
        let x = calculateWage(hourlyRate: rate!, hours: hours)
        shiftTotalHourlyWageLabel.text = convertDoubleToCurrency(amount: x)
        GlobalVariable.thisShiftsHourlyWage = convertDoubleToCurrency(amount: x)
    }
    func calculateTotalOfShift() {
        print("This shifts total tips:", shiftTipTotalLabel.text ?? "$000.00")
        print("Total Shift Wages are ", shiftTotalHourlyWageLabel.text ?? "$00.00")
        let total = shiftTipTotalLabel.text ?? "$000.00"
        let wage = shiftTotalHourlyWageLabel.text ?? "$000.00"
        let shiftTotal = convertCurrencyToDouble(input: total)
        let shiftWage = convertCurrencyToDouble(input: wage)
        let x = calculateShiftTotal(tipTotal: shiftTotal!, wageTotal: shiftWage!)
        shiftTotalLabel.text = convertDoubleToCurrency(amount: x)
        thisShiftTotal = convertDoubleToCurrency(amount: x)
    }
    
    func calculateWage(hourlyRate: Double, hours: Double) -> Double {
        return (hourlyRate * hours)
    }
    func calculateShiftTotal(tipTotal: Double, wageTotal: Double) -> Double {
        return (tipTotal + wageTotal)
    }
    
    func resetArrows() {
        tipsUpArrow.isHidden = true
        breakUpArrow.isHidden = true
        shiftNotesUpArrow.isHidden = true
    }
    
    func closeTipWin(){
        isContentView1Shown = false
        resetArrows()
        tipsDownArrow.isHidden = false
        shiftTableView.beginUpdates()
        shiftTableView.endUpdates()
        
        
    }
    
    func closeAll(view: Int) {
        isContentView1Shown = false
        isContentView2Shown = false
        isContentView3Shown = false
        resetArrows()
        tableView.beginUpdates()
        tableView.endUpdates()
        switch view {
        case 1:
            tipsDownArrow.isHidden = false
        default:
            break
        }
    }
    
    // MARK: - UIButton Action
    @IBAction func tipTotalBtnPressed(_ sender: UIButton) {
        if GlobalVariable.shiftTipsAdded == false {
            performSegue(withIdentifier: "toAddTipsSegue", sender: AnyClass.self)
            
        }
    }
    
    @IBAction func pressed00BreakMin(_ sender: UIButton) {
        insertBreakMin(bMin: 0)
        closeAll(view: 0)
    }
    @IBAction func pressed15BreakMin(_ sender: UIButton) {
        insertBreakMin(bMin: 15)
        closeAll(view: 0)
    }
    @IBAction func pressed30BreakMin(_ sender: UIButton) {
        insertBreakMin(bMin: 30)
        closeAll(view: 0)
    }
    @IBAction func pressed45BreakMin(_ sender: UIButton) {
        insertBreakMin(bMin: 45)
        closeAll(view: 0)
    }
    @IBAction func pressed60BreakMin(_ sender: UIButton) {
        insertBreakMin(bMin: 60)
        closeAll(view: 0)
    }
  
    @IBAction func finishClockOutBtnPressed(_ sender: UIButton) {
        populatePayPeriod(Duration: GlobalVariable.PayPeriodLength)
    }
    
    
    // MARK: - Navigation
   
    @IBAction func unwindFromAddVC(_ sender: UIStoryboardSegue) {
        if sender.source is shiftTipsTableViewController {
            if let senderVC = sender.source as? shiftTipsTableViewController {
                shiftTipTotalLabel.text = senderVC.thisShiftsTipTotal
                thisShiftsTipTotal = senderVC.thisShiftsTipTotal
                closeAll(view: 1)
                calculateTotalWage()
                calculateTotalOfShift()
            }
        }
    }
    
}

