//
//  setStateOfResidenceViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/9/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class setStateOfResidenceViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var stateOfResidencePicker: UIPickerView!
    
    @IBOutlet weak var continueBtn: UIButton!
    
    
    // MARK: let statement
    let states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "District of Columbia", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
    
    // MARK: variables
    var stateSelected = String()
    var stateOfResedienceSet = false
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueBtn.isEnabled = false
        continueBtn.alpha = 0.60
    }
    
    // MARK: - Picker Data
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return states.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return states[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        stateSelected = states[row]
        continueBtn.isEnabled = true
        continueBtn.alpha = 1
    }
    
    
    func setStateAbbrev(state: String){
        switch state {
        case "Alabama":
            GlobalVariable.stateOfResidence = "AL"
        case "Alaska":
            GlobalVariable.stateOfResidence = "AK"
        case "Arizona":
            GlobalVariable.stateOfResidence = "AZ"
        case "Arkansas":
            GlobalVariable.stateOfResidence = "AR"
        case "California":
            GlobalVariable.stateOfResidence = "CA"
        case "Colorado":
            GlobalVariable.stateOfResidence = "CO"
        case "Connecticut":
            GlobalVariable.stateOfResidence = "CT"
        case "Delaware":
            GlobalVariable.stateOfResidence = "DE"
        case "District of Columbia":
            GlobalVariable.stateOfResidence = "DC"
        case "Florida":
            GlobalVariable.stateOfResidence = "FL"
        case "Georgia":
            GlobalVariable.stateOfResidence = "GA"
        case "Hawaii":
            GlobalVariable.stateOfResidence = "HI"
        case "Idaho":
            GlobalVariable.stateOfResidence = "ID"
        case "Illinois":
            GlobalVariable.stateOfResidence = "IL"
        case "Indiana":
            GlobalVariable.stateOfResidence = "IN"
        case "Iowa":
            GlobalVariable.stateOfResidence = "IA"
        case "Kansas":
            GlobalVariable.stateOfResidence = "KS"
        case "Louisiana":
            GlobalVariable.stateOfResidence = "LA"
        case "Maine":
            GlobalVariable.stateOfResidence = "ME"
        case "Maryland":
            GlobalVariable.stateOfResidence = "MD"
        case "Massachusetts":
            GlobalVariable.stateOfResidence = "MA"
        case "Michigan":
            GlobalVariable.stateOfResidence = "MI"
        case "Minnesota":
            GlobalVariable.stateOfResidence = "MN"
        case "Mississippi":
            GlobalVariable.stateOfResidence = "MS"
        case "Missouri":
            GlobalVariable.stateOfResidence = "MO"
        case "Montana":
            GlobalVariable.stateOfResidence = "MT"
        case "Nebraska":
            GlobalVariable.stateOfResidence = "NE"
        case "Nevada":
            GlobalVariable.stateOfResidence = "NV"
        case "New Hampshire":
            GlobalVariable.stateOfResidence = "NH"
        case "New Jersey":
            GlobalVariable.stateOfResidence = "NJ"
        case "New Mexico":
            GlobalVariable.stateOfResidence = "NM"
        case "New York":
            GlobalVariable.stateOfResidence = "NY"
        case "North Carolina":
            GlobalVariable.stateOfResidence = "NC"
        case "North Dakota":
            GlobalVariable.stateOfResidence = "ND"
        case "Ohio":
            GlobalVariable.stateOfResidence = "OH"
        case "Oklahoma":
            GlobalVariable.stateOfResidence = "OK"
        case "Oregon":
            GlobalVariable.stateOfResidence = "OR"
        case "Pennsylvania":
            GlobalVariable.stateOfResidence = "PA"
        case "Rhode Island":
            GlobalVariable.stateOfResidence = "RI"
        case "South Carolina":
            GlobalVariable.stateOfResidence = "SC"
        case "South Dakota":
            GlobalVariable.stateOfResidence = "SD"
        case "Tennessee":
            GlobalVariable.stateOfResidence = "TN"
        case "Texas":
            GlobalVariable.stateOfResidence = "TX"
        case "Utah":
            GlobalVariable.stateOfResidence = "UT"
        case "Vermont":
            GlobalVariable.stateOfResidence = "VT"
        case "Virginia":
            GlobalVariable.stateOfResidence = "VA"
        case "Washington":
            GlobalVariable.stateOfResidence = "WA"
        case "West Virginia":
            GlobalVariable.stateOfResidence = "WV"
        case "Wisconsin":
            GlobalVariable.stateOfResidence = "WI"
        case "Wyoming":
            GlobalVariable.stateOfResidence = "WY"
        default:
            break
        }
        
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        setStateAbbrev(state: stateSelected)
        let stateSet = true
        stateOfResedienceSet = stateSet
    }
}
