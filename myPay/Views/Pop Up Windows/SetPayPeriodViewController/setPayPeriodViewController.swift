//
//  setPayPeriodViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/9/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class setPayPeriodViewController: UIViewController {
    
    @IBOutlet weak var WeeklyBtn: UIButton!
    @IBOutlet weak var every2WeeksBtn: UIButton!
    @IBOutlet weak var biMonthlyBtn: UIButton!
    @IBOutlet weak var monthlyBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    
    
    // MARK: variables
    var payPeriodStatus = false
    var payPeriodLengthStatus = false
    
    
    // MARK: - Load Views
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueBtn.isEnabled = false
        continueBtn.alpha = 0.60
    }
    
    // MARK: - Functions
    func resetBtnColor(){
        WeeklyBtn.setTitleColor(.white, for: .normal)
        every2WeeksBtn.setTitleColor(.white, for: .normal)
        biMonthlyBtn.setTitleColor(.white, for: .normal)
        monthlyBtn.setTitleColor(.white, for: .normal)
    }
    
    func setPayPeriod(period: Int) {
        resetBtnColor()
        GlobalVariable.PayPeriodLength = period
        payPeriodStatus = true
        GlobalVariable.PayPeriodLengthSet = payPeriodStatus
        continueBtn.isEnabled = true
        continueBtn.alpha = 1
        switch period {
        case 4:
            WeeklyBtn.setTitleColor(Theme.Accent, for: .normal)
        case 2:
            biMonthlyBtn.setTitleColor(Theme.Accent, for: .normal)
        case 3:
            every2WeeksBtn.setTitleColor(Theme.Accent, for: .normal)
        case 1:
            monthlyBtn.setTitleColor(Theme.Accent, for: .normal)
        default:
            break
        }
    }
    
    // MARK: - UIActions
    // MARK: Buttons
    
    @IBAction func weeklyBtnPressed(_ sender: UIButton) {
        setPayPeriod(period: 4)
    }
    @IBAction func every2WeeksBtnPressed(_ sender: UIButton) {
        setPayPeriod(period: 3)
        
    }
    @IBAction func biMonthlyBtnPressed(_ sender: UIButton) {
        setPayPeriod(period: 2)
        
    }
    @IBAction func monthlyBtnPressed(_ sender: UIButton) {
        setPayPeriod(period: 1)
        
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        payPeriodLengthStatus = payPeriodStatus
    }
}
