//
//  setStartPayPeriodViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/9/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class setStartPayPeriodViewController: UIViewController {
    
    @IBOutlet weak var startOfPayPeriodPicker: UIDatePicker!
    
    var startOfPayPeriodSet = false
    
    @IBOutlet weak var continueBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueBtn.isEnabled = false
        continueBtn.alpha = 0.60
    }
    
    
    @IBAction func startOfPayPeriodPickerValueChanged(_ sender: UIDatePicker) {
        
        let calendar = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        comps.weekday = 0
        let maxDate = calendar.date(byAdding: comps, to: Date())
        comps.weekday = -14
        let minDate = calendar.date(byAdding: comps, to: Date())
        startOfPayPeriodPicker.maximumDate = maxDate
        startOfPayPeriodPicker.minimumDate = minDate
        
        let date: Date = startOfPayPeriodPicker.date
        GlobalVariable.startOfPayPeriod = date
        
        continueBtn.isEnabled = true
        continueBtn.alpha = 1
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let startOfPayPeriodAdded = true
        startOfPayPeriodSet = startOfPayPeriodAdded
    }
    
}
