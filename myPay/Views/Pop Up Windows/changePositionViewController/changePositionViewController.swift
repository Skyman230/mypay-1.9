//
//  changePositionViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/19/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class changePositionViewController: UIViewController  {
    
    // MARK: - UIOutlet
    // MARK: UITableView
    @IBOutlet weak var positionTableView: UITableView!
    
    // MARK: UIView
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var footerContentView: UIView!
    @IBOutlet weak var backgroundView1: UIView!
    @IBOutlet weak var backgroundView2: UIView!
    @IBOutlet weak var backgroundView3: UIView!
    @IBOutlet weak var backgroundView4: UIView!
    @IBOutlet weak var backgroundView5: UIView!
    
    // MARK: UIButton
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var selectJobBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var addPositionBtn: UIButton!
    
    // MARK: UILabel
    @IBOutlet weak var infoTextLabel: UILabel!
    
    // MARK: - Var
    // MARK: String
    var selectedPosition: String?
    var selectedRate: String?
    var selectedData = [String]()
    var newPosition: String?
    var newRate: String?
    var whichFunc = "select"
    
    // MARK: Int
     var jobIndex = GlobalVariable.jobIndex
    
    //MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        positionTableView.delegate = self
        positionTableView.dataSource = self
        toggleHeader(status: whichFunc)
        changeBackgroundSize(whichBkg: Data.Jobs[jobIndex].positionModel.count)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        toggleHeader(status: whichFunc)
    }
    
    // MARK: - func
    func toggleHeader(status: String) {
        switch status {
        case "select":
            infoTextLabel.text = "Select Position and Hourly Rate"
            headerContentView.isHidden = false
            footerContentView.isHidden = false
            closeBtn.isHidden = true
            cancelBtn.setTitle("Cancel", for: .normal)
            selectJobBtn.isEnabled = false
            selectJobBtn.alpha = 0.40
            addPositionBtn.isHidden = false
        case "view":
            if GlobalVariable.jobPositionArrayIsPopulated == false {
                headerContentView.isHidden = false
                infoTextLabel.text = "No Positions Have Been Added"
            } else if GlobalVariable.jobPositionArrayIsPopulated == true{
                headerContentView.isHidden = true
            }
            cancelBtn.isHidden = false
            cancelBtn.setTitle("Close Window", for: .normal)
            selectJobBtn.isHidden = true
            closeBtn.isHidden = true
            addPositionBtn.isHidden = false
        case "Settings":
            if GlobalVariable.jobPositionArrayIsPopulated == false {
                headerContentView.isHidden = false
                infoTextLabel.text = "No Positions Have Been Added"
                cancelBtn.isHidden = false
                cancelBtn.setTitle("Close Window", for: .normal)
                selectJobBtn.isHidden = true
                addPositionBtn.isHidden = true
            } else if GlobalVariable.jobPositionArrayIsPopulated == true{
                headerContentView.isHidden = false
                infoTextLabel.text = "Your Job Positions being Tracked"
                cancelBtn.isHidden = true
                selectJobBtn.isHidden = true
                closeBtn.isHidden = false
                addPositionBtn.isHidden = true
            }
        default:
            break
        }
    }
    
    func changeBackgroundSize(whichBkg: Int) {
        if whichBkg <= 0 {
            backgroundView1.isHidden = false
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
        }
        if whichBkg == 1 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = false
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
        }
        if whichBkg == 2 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = false
            backgroundView4.isHidden = true
            backgroundView5.isHidden = true
        }
        if whichBkg == 3 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = false
            backgroundView5.isHidden = true
        }
        if whichBkg >= 4 {
            backgroundView1.isHidden = true
            backgroundView2.isHidden = true
            backgroundView3.isHidden = true
            backgroundView4.isHidden = true
            backgroundView5.isHidden = false
        }
        
        
        
    }
    
    // MARK: - UIActions
    // MARK: Button
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let position = selectedPosition {
            newPosition = position
        }
        if let hourlyRate = selectedRate {
            let rate = (hourlyRate as NSString).doubleValue
            let thisShiftRate = convertDoubleToCurrency(amount: rate)
            newRate = thisShiftRate
            GlobalVariable.thisShiftRate = newRate
        }
        if segue.identifier == "addPositionSegue" {
            let popup = segue.destination as! addPositionsViewController
            popup.setBtnTitle = 2
            popup.doneSaving = { [weak self] in
                self?.positionTableView.reloadData()
                self?.changeBackgroundSize(whichBkg: Data.Jobs[self!.jobIndex].positionModel.count)
            }
            
        }
        
    }
}
extension changePositionViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Data.Jobs[jobIndex].positionModel.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = positionTableView.dequeueReusableCell(withIdentifier: "jobPositionCell") as! PositionDetailCell
        let position = Data.Jobs[jobIndex].positionModel[indexPath.row].position
        let rate = Data.Jobs[jobIndex].positionModel[indexPath.row].hourlyRate
        
        cell.positionTextLabel.text = position
        cell.hourlyRateTextLabel.text = rate
        
        if selectedData.contains(position) {
            cell.positionTitleTextLabel.textColor = UIColor.init(displayP3Red: 241/250, green: 161/250, blue: 80/250, alpha: 100)
            cell.positionTextLabel.textColor = UIColor.init(displayP3Red: 241/250, green: 161/250, blue: 80/250, alpha: 100)
            cell.hourlyRateTitleLabel.textColor = UIColor.init(displayP3Red: 241/250, green: 161/250, blue: 80/250, alpha: 100)
            cell.hourlyRateTextLabel.textColor = UIColor.init(displayP3Red: 241/250, green: 161/250, blue: 80/250, alpha: 100)
        } else {
            cell.positionTitleTextLabel.textColor = UIColor.white
            cell.positionTextLabel.textColor = UIColor.white
            cell.hourlyRateTitleLabel.textColor = UIColor.white
            cell.hourlyRateTextLabel.textColor = UIColor.white
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PositionDetailCell
        selectedPosition = cell.positionTextLabel.text
        selectedRate = cell.hourlyRateTextLabel.text
        
        if selectedData.contains(Data.Jobs[jobIndex].positionModel[indexPath.row].position) {
            selectedData.removeLast()
            cell.positionTitleTextLabel.textColor = UIColor.white
            cell.positionTextLabel.textColor = UIColor.white
            cell.hourlyRateTitleLabel.textColor = UIColor.white
            cell.hourlyRateTextLabel.textColor = UIColor.white
        } else {
            selectedData.removeAll()
            selectedData.append(Data.Jobs[jobIndex].positionModel[indexPath.row].position)
            cell.positionTitleTextLabel.textColor = UIColor.init(displayP3Red: 241/250, green: 161/250, blue: 80/250, alpha: 100)
            cell.positionTextLabel.textColor = UIColor.init(displayP3Red: 241/250, green: 161/250, blue: 80/250, alpha: 100)
            cell.hourlyRateTitleLabel.textColor = UIColor.init(displayP3Red: 241/250, green: 161/250, blue: 80/250, alpha: 100)
            cell.hourlyRateTextLabel.textColor = UIColor.init(displayP3Red: 241/250, green: 161/250, blue: 80/250, alpha: 100)
            selectJobBtn.alpha = 1
            selectJobBtn.isEnabled = true
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PositionDetailCell
        cell.positionTitleTextLabel.textColor = UIColor.white
        cell.positionTextLabel.textColor = UIColor.white
        cell.hourlyRateTitleLabel.textColor = UIColor.white
        cell.hourlyRateTextLabel.textColor = UIColor.white
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (contextualAction, view, actionPerformed: (Bool) -> Void) in
            JobsFunctions.deletePosition(index: indexPath.row)
            tableView.reloadData()
            if Data.Jobs[self.jobIndex].positionModel.count == 0 {
                GlobalVariable.jobPositionArrayIsPopulated = false
                self.dismiss(animated: true)
            }
            actionPerformed(true)
        }
        return UISwipeActionsConfiguration(actions: [delete])
    }
}
