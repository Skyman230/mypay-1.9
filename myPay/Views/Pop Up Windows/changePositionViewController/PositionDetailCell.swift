//
//  PositionDetailCell.swift
//  myPay
//
//  Created by Donald Scott on 5/19/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class PositionDetailCell: UITableViewCell {

    // MARK: - UIOutlet
    // MARK: UILabel
    @IBOutlet weak var positionTitleTextLabel: UILabel!
    @IBOutlet weak var positionTextLabel: UILabel!
    @IBOutlet weak var hourlyRateTitleLabel: UILabel!
    @IBOutlet weak var hourlyRateTextLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
