//
//  prevShiftTableViewCell.swift
//  myPay
//
//  Created by Donald Scott on 2/15/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class prevShiftTableViewCell: UITableViewCell {
    
    @IBOutlet weak var shiftDateTextLabel: UILabel!
    @IBOutlet weak var shiftTotalTextLabel: UILabel!
    @IBOutlet weak var shiftTotalTipsTextLabel: UILabel!
    @IBOutlet weak var shiftTotalWageTextLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
