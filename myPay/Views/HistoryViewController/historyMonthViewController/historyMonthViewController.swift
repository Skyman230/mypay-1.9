//
//  historyMonthViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/28/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit
import  Charts

class historyMonthViewController: UIViewController {
    
    
    var allButtonsPressed = false {
        didSet{
            if let items = tabBarController?.tabBar.items {
                items[0].isEnabled = allButtonsPressed
                items[1].isEnabled = allButtonsPressed
                items[2].isEnabled = allButtonsPressed
                items[3].isEnabled = allButtonsPressed
                items[4].isEnabled = allButtonsPressed
            }
        }
    }
    
    // MARK: - IBOutlets
    // MARK: lineChartView
    @IBOutlet weak var lineChartView1: LineChartView!
    @IBOutlet weak var lineChartView2: LineChartView!
    @IBOutlet weak var lineChartView3: LineChartView!
    
    // MARK: UITableView
    @IBOutlet weak var historyMonthTableView: UITableView!
    
    // MARK: UILabels
    @IBOutlet weak var grossMonthlyTextLabel: UILabel!
    @IBOutlet weak var grossMonthlyTipsTextLabel: UILabel!
    @IBOutlet weak var grossMonthlyWageTextLabel: UILabel!
    @IBOutlet weak var monthlyNoDataTextLabel: UILabel!
    @IBOutlet weak var monthTextLabel: UILabel!
    
    // MARK: - vars
    // MARK: String
    var shiftDate: String?
    
    var shiftTotal: String?
    var shiftTipTotal: String?
    var shiftTotalHourlyWage: String?
    var shift: ShiftModel?
    
    var shiftJobName = GlobalVariable.jobName
    var shiftPosition: String?
    var shiftHourlyRate: String?
    
    var shiftHours: String?
    var shiftClockIn: String?
    var shiftClockOut: String?
    var shiftNotes: String?
    var monthName: String?
    
    //MARK: Double
    var number1 = [Double]()
    var number2 = [Double]()
    var number3 = [Double]()
    var tips = Double()
    var wage =  Double()
    var hours =  Double()
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allButtonsPressed = false
        let sumArray = thisMonthsGrossTipArray.map{convertCurrencyToDouble(input: $0)}
        number1 = sumArray as! [Double]
        updateGraph()
        let wageSumArray = thisMonthsGrossWageArray.map{convertCurrencyToDouble(input: $0)}
        number2 = wageSumArray as! [Double]
        updateGraph2()
        let hoursSumArray = thisMonthsTotalHoursArray.map{Double($0) ?? 0}
        number3 = hoursSumArray
        updateGraph3()
        changeNavigationTitle()
        showFigures()
    }
    
    
    // MARK: - func
    func showFigures() {
        CalculateSums.showFigures()
        if thisMonthsGrossWageArray.count == 0 {
            grossMonthlyTextLabel.text = "No Data Available"
            grossMonthlyTextLabel.textColor = UIColor.gray
        } else {
            grossMonthlyTextLabel.text = GlobalVariable.MonthlyGross
            grossMonthlyTextLabel.textColor = UIColor.white
        }
        if thisMonthsGrossTipArray.count == 0 {
            grossMonthlyTipsTextLabel.text = "No Data Available"
            grossMonthlyTipsTextLabel.textColor = UIColor.gray
        } else {
            grossMonthlyTipsTextLabel.text = GlobalVariable.MonthTips
            grossMonthlyTipsTextLabel.textColor = UIColor.white
        }
        if thisMonthsGrossWageArray.count == 0 {
            grossMonthlyWageTextLabel.text = "No Data Available"
            grossMonthlyWageTextLabel.textColor = UIColor.gray
        } else {
            grossMonthlyWageTextLabel.text = GlobalVariable.MonthWage
            grossMonthlyWageTextLabel.textColor = UIColor.white
        }
    }
    
    func changeNavigationTitle() {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "L"
        let nameOfMonth = dateFormatter.string(from: now)
        let monthNum = Int(nameOfMonth)
        switch monthNum {
        case 1:
            monthTextLabel.text = "January"
            monthName = "January"
        case 2:
            monthTextLabel.text = "February"
            monthName = "February"
        case 3:
            monthTextLabel.text = "March"
            monthName = "March"
        case 4:
            monthTextLabel.text = "April"
            monthName = "April"
        case 5:
            monthTextLabel.text = "May"
            monthName = "May"
        case 6:
            monthTextLabel.text = "June"
            monthName = "June"
        case 7:
            monthTextLabel.text = "July"
            monthName = "July"
        case 8:
            monthTextLabel.text = "August"
            monthName = "January"
        case 9:
            monthTextLabel.text = "September"
            monthName = "August"
        case 10:
            monthTextLabel.text = "October"
            monthName = "October"
        case 11:
            monthTextLabel.text = "November"
            monthName = "November"
        case 12:
            monthTextLabel.text = "December"
            monthName = "December"
        default:
            break
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MonthlyInfoSegue" {
            if let shiftDetailVC = segue.destination as? shiftDetailTableViewController {
                shiftDetailVC.shiftDate = shiftDate
                
                shiftDetailVC.shiftTotal = shiftTotal
                shiftDetailVC.shiftTipTotal = shiftTipTotal
                shiftDetailVC.shiftTotalWage = shiftTotalHourlyWage
                
                shiftDetailVC.shiftJobName = shiftJobName
                shiftDetailVC.shiftPosition = shiftPosition
                shiftDetailVC.shiftRate = shiftHourlyRate
                
                shiftDetailVC.shiftHours = shiftHours
                shiftDetailVC.shiftStartTime = shiftClockIn
                shiftDetailVC.shiftEndTime = shiftClockOut
                
                shiftDetailVC.shiftNotes = shiftNotes
                
                
                
            }
        }
    }
}

// MARK: - LineChart Data
extension historyMonthViewController {
    
    func updateGraph(){
        var lineChartEntry = [ChartDataEntry]()
        
        for i in 0..<number1.count {
            
            let value = ChartDataEntry(x: Double(i), y: number1[i])
            
            lineChartEntry.append(value)
        }
        
        let line1 = LineChartDataSet(values: lineChartEntry, label: "Monthly Tips")
        
        line1.colors = [UIColor.blue]
        
        let data = LineChartData()
        
        data.addDataSet(line1)
        lineChartView1.data = data
        lineChartView1.chartDescription?.text = "My awsome chart"
    }
    
    func updateGraph2(){
        var lineChartEntry2 = [ChartDataEntry]()
        
        for i in 0..<number2.count {
            
            let value = ChartDataEntry(x: Double(i), y: number2[i])
            
            lineChartEntry2.append(value)
        }
        
        let line2 = LineChartDataSet(values: lineChartEntry2, label: "Monthly Wages")
        
        line2.colors = [UIColor.red]
        
        let data = LineChartData()
        
        data.addDataSet(line2)
        lineChartView2.data = data
        lineChartView2.chartDescription?.text = "My awsome chart"
    }
    
    func updateGraph3(){
        var lineChartEntry3 = [ChartDataEntry]()
        
        for i in 0..<number3.count {
            
            let value = ChartDataEntry(x: Double(i), y: number3[i])
            
            lineChartEntry3.append(value)
        }
        
        let line3 = LineChartDataSet(values: lineChartEntry3, label: "Monthly Hours")
        
        line3.colors = [UIColor.orange]
        
        let data = LineChartData()
        
        data.addDataSet(line3)
        lineChartView3.data = data
        lineChartView3.chartDescription?.text = "My awsome chart"
    }

    
}

// MARK: - TableView Data
extension historyMonthViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        monthlyTotalArray = monthlyTotalArray.reversed()
        return(monthlyTotalArray.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "currentShiftCell") as! currentShiftTableViewCell
            let date = monthlyTotalArray[indexPath.row].date
            let total = monthlyTotalArray[indexPath.row].shiftTotal
            let tips = monthlyTotalArray[indexPath.row].tipTotal
            let wage = monthlyTotalArray[indexPath.row].totalHourlyWage
            
            cell.shiftDateTextLabel.text = date
            cell.shiftTotalTextLabel.text = total
            cell.shiftTotalTipsTextLabel.text = tips
            cell.shiftTotalWageTextLabel.text = wage
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "previousShiftCell") as! prevShiftTableViewCell
            let date = monthlyTotalArray[indexPath.row].date
            let total = monthlyTotalArray[indexPath.row].shiftTotal
            let tips = monthlyTotalArray[indexPath.row].tipTotal
            let wage = monthlyTotalArray[indexPath.row].totalHourlyWage
            
            cell.shiftDateTextLabel.text = date
            cell.shiftTotalTextLabel.text = total
            cell.shiftTotalTipsTextLabel.text = tips
            cell.shiftTotalWageTextLabel.text = wage
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = editAction(at: indexPath)
        return UISwipeActionsConfiguration(actions:[edit])
    }
    
    func editAction(at indexPath: IndexPath) -> UIContextualAction {
        let shift = monthlyTotalArray[indexPath.row]
        let action = UIContextualAction(style: .normal, title: "Edit") { (action, view, completion) in
            print(shift)
            self.performSegue(withIdentifier: "editDetailsSegue", sender: nil)
            completion(true)
        }
        action.image = #imageLiteral(resourceName: "pencil")
        action.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        
        return action
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0) {
            monthlyNoDataTextLabel.isHidden = true
            return 150
        }
        monthlyNoDataTextLabel.isHidden = true
        return 86
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0) {
            let cell = tableView.cellForRow(at: indexPath) as! currentShiftTableViewCell
            shiftDate = cell.shiftDateTextLabel.text
            shiftTipTotal = cell.shiftTotalTipsTextLabel.text
            shiftTotal = cell.shiftTotalTextLabel.text
            shiftTotalHourlyWage = cell.shiftTotalWageTextLabel.text
            shiftHours = monthlyTotalArray[indexPath.row].Hours
            shiftNotes = monthlyTotalArray[indexPath.row].shiftNotes
            shiftClockIn = monthlyTotalArray[indexPath.row].clockIn
            shiftClockOut = monthlyTotalArray[indexPath.row].clockOut
            shiftJobName = GlobalVariable.jobName
            shiftPosition = monthlyTotalArray[indexPath.row].position
            shiftHourlyRate = monthlyTotalArray[indexPath.row].hourlyRate
            
            performSegue(withIdentifier: "MonthlyInfoSegue", sender: Any?.self)
        } else {
            
            let cell = tableView.cellForRow(at: indexPath) as! prevShiftTableViewCell
            shiftDate = cell.shiftDateTextLabel.text
            shiftTipTotal = cell.shiftTotalTipsTextLabel.text
            shiftTotal = cell.shiftTotalTextLabel.text
            shiftTotalHourlyWage = cell.shiftTotalWageTextLabel.text
            shiftHours = monthlyTotalArray[indexPath.row].Hours
            shiftNotes = monthlyTotalArray[indexPath.row].shiftNotes
            shiftClockIn = monthlyTotalArray[indexPath.row].clockIn
            shiftClockOut = monthlyTotalArray[indexPath.row].clockOut
            shiftJobName = GlobalVariable.jobName
            shiftPosition = monthlyTotalArray[indexPath.row].position
            shiftHourlyRate = monthlyTotalArray[indexPath.row].hourlyRate
            
            performSegue(withIdentifier: "MonthlyInfoSegue", sender: Any?.self)
            
        }
    }
}


