//
//  shiftDetailTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/28/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class shiftDetailTableViewController: UITableViewController {
    
    
    // MARK: - UIOutlets
    // MARK: UILabels
    @IBOutlet weak var shiftDateTextLabel: UILabel!
    @IBOutlet weak var shiftTotalsTextLabel: UILabel!
    @IBOutlet weak var shiftTipTotalTextLabel: UILabel!
    @IBOutlet weak var shiftWageTotalTextLabel: UILabel!
    @IBOutlet weak var shiftStartTimeTextLabel: UILabel!
    @IBOutlet weak var shiftEndTimeTextLabel: UILabel!
    @IBOutlet weak var shiftHoursWorkedTextLabel: UILabel!
    @IBOutlet weak var shiftPositionTextLabel: UILabel!
    @IBOutlet weak var shiftHourlyRateTextLabel: UILabel!
    
    // MARK: UITextView
    @IBOutlet weak var shiftNotesTextView: UITextView!
    
    // MARK: - var
    // MARK: String
    var shiftDate: String?
    var shiftJobName: String?
    var shiftTotal: String?
    var shiftTipTotal: String?
    var shiftTotalWage: String?
    var shiftStartTime: String?
    var shiftEndTime: String?
    var shiftHours: String?
    var shiftPosition: String?
    var shiftRate: String?
    var shiftNotes: String?
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateTableView()

    }
    
    // MARK: - func
    func populateTableView() {
        shiftDateTextLabel.text = shiftDate
        shiftTotalsTextLabel.text = shiftTotal
        shiftTipTotalTextLabel.text = shiftTipTotal
        shiftWageTotalTextLabel.text = shiftTotalWage
        shiftStartTimeTextLabel.text = shiftStartTime
        shiftEndTimeTextLabel.text = shiftEndTime
        shiftHoursWorkedTextLabel.text = shiftHours
        shiftPositionTextLabel.text = shiftPosition
        shiftHourlyRateTextLabel.text = shiftRate
        shiftNotesTextView.text = shiftNotes
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
