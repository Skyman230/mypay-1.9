//
//  payPeriodViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/28/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit
import Charts

class historyPayPeriodViewController: UIViewController {
    
    
    var allButtonsPressed = false {
        didSet{
            if let items = tabBarController?.tabBar.items {
                items[0].isEnabled = allButtonsPressed
                items[1].isEnabled = allButtonsPressed
                items[2].isEnabled = allButtonsPressed
                items[3].isEnabled = allButtonsPressed
                items[4].isEnabled = allButtonsPressed
            }
        }
    }
    
    // MARK: - IBOutlets
    // MARK: lineChartView
    @IBOutlet weak var lineChartView1: LineChartView!
    @IBOutlet weak var lineChartView2: LineChartView!
    @IBOutlet weak var lineChartView3: LineChartView!
    
    // MARK: UITableView
    @IBOutlet weak var historyPayPeriodTableView: UITableView!
    
    // MARK: UILabels
    @IBOutlet weak var grossPayPeriodTextLabel: UILabel!
    @IBOutlet weak var grossPayPeriodTipsTextLabel: UILabel!
    @IBOutlet weak var grossPayPeriodWageTextLabel: UILabel!
    @IBOutlet weak var payPeriodNoDataTextLabel: UILabel!
    
    // MARK: - vars
    // MARK: String
    var shiftDate: String?
    
    var shiftTotal: String?
    var shiftTipTotal: String?
    var shiftTotalHourlyWage: String?
    var shift: ShiftModel?
    
    var shiftJobName = GlobalVariable.jobName
    var shiftPosition: String?
    var shiftHourlyRate: String?
    
    var shiftHours: String?
    var shiftClockIn: String?
    var shiftClockOut: String?
    var shiftNotes: String?
    
    //MARK: Double
    var number1 = [Double]()
    var number2 = [Double]()
    var number3 = [Double]()
    var tips = Double()
    var wage =  Double()
    var hours =  Double()

    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()

        allButtonsPressed = false
        let sumArray = payPeriodGrossTipArray.map{convertCurrencyToDouble(input: $0)}
        number1 = sumArray as! [Double]
        updateGraph()
        let wageSumArray = payPeriodGrossWageArray.map{convertCurrencyToDouble(input: $0)}
        number2 = wageSumArray as! [Double]
        updateGraph2()
        let hoursSumArray = payPeriodTotalHoursArray.map{Double($0) ?? 0}
        number3 = hoursSumArray
        updateGraph3()
        showFigures()
    }
    
    //MARK: - func
    func showFigures() {
        CalculateSums.showFigures()
        if GlobalVariable.GrossWage!.isEmpty == true {
            grossPayPeriodTextLabel.text = "No Data Available"
            grossPayPeriodTextLabel.textColor = UIColor.gray
        } else {
            grossPayPeriodTextLabel.text = GlobalVariable.GrossWage
            grossPayPeriodTextLabel.textColor = UIColor.white
        }
        if payPeriodGrossTipArray.count == 0 {
            grossPayPeriodTipsTextLabel.text = "No Data Available"
            grossPayPeriodTipsTextLabel.textColor = UIColor.gray
        } else {
            grossPayPeriodTipsTextLabel.text = GlobalVariable.PayPeriodTips
            grossPayPeriodTipsTextLabel.textColor = UIColor.white
        }
        if payPeriodGrossWageArray.count == 0 {
            grossPayPeriodWageTextLabel.text = "No Data Available"
            grossPayPeriodWageTextLabel.textColor = UIColor.gray
        } else {
            grossPayPeriodWageTextLabel.text = GlobalVariable.PayPeriodWage
            grossPayPeriodWageTextLabel.textColor = UIColor.white
        }
    }
    
   
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "infoSegue" {
            if let shiftDetailVC = segue.destination as? shiftDetailTableViewController {
                shiftDetailVC.shiftDate = shiftDate
                
                shiftDetailVC.shiftTotal = shiftTotal
                shiftDetailVC.shiftTipTotal = shiftTipTotal
                shiftDetailVC.shiftTotalWage = shiftTotalHourlyWage
                
                shiftDetailVC.shiftJobName = shiftJobName
                shiftDetailVC.shiftPosition = shiftPosition
                shiftDetailVC.shiftRate = shiftHourlyRate
                
                shiftDetailVC.shiftHours = shiftHours
                shiftDetailVC.shiftStartTime = shiftClockIn
                shiftDetailVC.shiftEndTime = shiftClockOut
                
                shiftDetailVC.shiftNotes = shiftNotes
                
                
                
            }
        }
    }
    

}

// MARK: - LineChart Data
extension historyPayPeriodViewController {
    
    func updateGraph(){
        var lineChartEntry = [ChartDataEntry]()
        
        for i in 0..<number1.count {
            
            let value = ChartDataEntry(x: Double(i), y: number1[i])
            
            lineChartEntry.append(value)
        }
        
        let line1 = LineChartDataSet(values: lineChartEntry, label: "Pay Period Tips")
        
        line1.colors = [UIColor.blue]
        
        let data = LineChartData()
        
        data.addDataSet(line1)
        lineChartView1.data = data
        lineChartView1.chartDescription?.text = "My awsome chart"
    }
    
    func updateGraph2(){
        var lineChartEntry2 = [ChartDataEntry]()
        
        for i in 0..<number2.count {
            
            let value = ChartDataEntry(x: Double(i), y: number2[i])
            
            lineChartEntry2.append(value)
        }
        
        let line2 = LineChartDataSet(values: lineChartEntry2, label: "Pay Period Wages")
        
        line2.colors = [UIColor.red]
        
        let data = LineChartData()
        
        data.addDataSet(line2)
        lineChartView2.data = data
        lineChartView2.chartDescription?.text = "My awsome chart"
    }
    
    func updateGraph3(){
        var lineChartEntry3 = [ChartDataEntry]()
        
        for i in 0..<number3.count {
            
            let value = ChartDataEntry(x: Double(i), y: number3[i])
            
            lineChartEntry3.append(value)
        }
        
        let line3 = LineChartDataSet(values: lineChartEntry3, label: "Pay Period Hours")
        
        line3.colors = [UIColor.orange]
        
        let data = LineChartData()
        
        data.addDataSet(line3)
        lineChartView3.data = data
        lineChartView3.chartDescription?.text = "My awsome chart"
    }
}

// MARK: - TableView Data
extension historyPayPeriodViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        payPeriodArray = payPeriodArray.reversed()
        return(payPeriodArray.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "currentShiftCell") as! currentShiftTableViewCell
            let date = payPeriodArray[indexPath.row].date
            let total = payPeriodArray[indexPath.row].shiftTotal
            let tips = payPeriodArray[indexPath.row].tipTotal
            let wage = payPeriodArray[indexPath.row].totalHourlyWage
            
            cell.shiftDateTextLabel.text = date
            cell.shiftTotalTextLabel.text = total
            cell.shiftTotalTipsTextLabel.text = tips
            cell.shiftTotalWageTextLabel.text = wage
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "previousShiftCell") as! prevShiftTableViewCell
            let date = payPeriodArray[indexPath.row].date
            let total = payPeriodArray[indexPath.row].shiftTotal
            let tips = payPeriodArray[indexPath.row].tipTotal
            let wage = payPeriodArray[indexPath.row].totalHourlyWage
            
            cell.shiftDateTextLabel.text = date
            cell.shiftTotalTextLabel.text = total
            cell.shiftTotalTipsTextLabel.text = tips
            cell.shiftTotalWageTextLabel.text = wage
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = editAction(at: indexPath)
        return UISwipeActionsConfiguration(actions:[edit])
    }
    
    func editAction(at indexPath: IndexPath) -> UIContextualAction {
        let shift = payPeriodArray[indexPath.row]
        let action = UIContextualAction(style: .normal, title: "Edit") { (action, view, completion) in
            print(shift)
            self.performSegue(withIdentifier: "editDetailsSegue", sender: nil)
            completion(true)
        }
        action.image = #imageLiteral(resourceName: "pencil")
        action.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1)
        
        return action
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0) {
            payPeriodNoDataTextLabel.isHidden = true
            return 150
        }
        payPeriodNoDataTextLabel.isHidden = true
        return 86
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0) {
            let cell = tableView.cellForRow(at: indexPath) as! currentShiftTableViewCell
            shiftDate = cell.shiftDateTextLabel.text
            shiftTipTotal = cell.shiftTotalTipsTextLabel.text
            shiftTotal = cell.shiftTotalTextLabel.text
            shiftTotalHourlyWage = cell.shiftTotalWageTextLabel.text
            shiftHours = payPeriodArray[indexPath.row].Hours
            shiftNotes = payPeriodArray[indexPath.row].shiftNotes
            shiftClockIn = payPeriodArray[indexPath.row].clockIn
            shiftClockOut = payPeriodArray[indexPath.row].clockOut
            shiftJobName = GlobalVariable.jobName
            shiftPosition = payPeriodArray[indexPath.row].position
            shiftHourlyRate = payPeriodArray[indexPath.row].hourlyRate
            
            performSegue(withIdentifier: "infoSegue", sender: Any?.self)
        } else {
            
            let cell = tableView.cellForRow(at: indexPath) as! prevShiftTableViewCell
            shiftDate = cell.shiftDateTextLabel.text
            shiftTipTotal = cell.shiftTotalTipsTextLabel.text
            shiftTotal = cell.shiftTotalTextLabel.text
            shiftTotalHourlyWage = cell.shiftTotalWageTextLabel.text
            shiftHours = payPeriodArray[indexPath.row].Hours
            shiftNotes = payPeriodArray[indexPath.row].shiftNotes
            shiftClockIn = payPeriodArray[indexPath.row].clockIn
            shiftClockOut = payPeriodArray[indexPath.row].clockOut
            shiftJobName = GlobalVariable.jobName
            shiftPosition = payPeriodArray[indexPath.row].position
            shiftHourlyRate = payPeriodArray[indexPath.row].hourlyRate
            
            performSegue(withIdentifier: "infoSegue", sender: Any?.self)
            
        }
    }
}
