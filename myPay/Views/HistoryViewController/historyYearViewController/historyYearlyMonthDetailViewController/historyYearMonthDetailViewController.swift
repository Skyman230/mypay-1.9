//
//  historyYearMonthDetailViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/29/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class historyYearMonthDetailViewController: UIViewController  {
    
    
    // MARK: - UIOutlets
    // MARK: UITableView
    @IBOutlet weak var yearMonthDetailTableView: UITableView!
    
    // MARK: - vars
    // MARK: String
    var shiftDate: String?
    
    var shiftTotal: String?
    var shiftTipTotal: String?
    var shiftTotalHourlyWage: String?
    
    var shiftJobName: String?
    var shiftPosition: String?
    var shiftHourlyRate: String?
    
    var shiftHours: String?
    var shiftClockIn: String?
    var shiftClockOut: String?
    var shiftNotes: String?
    
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()

        changeNavigationTitle()
    }
    
    // MARK: - func
    func changeNavigationTitle() {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "L"
        let nameOfMonth = dateFormatter.string(from: now)
        let monthNum = Int(nameOfMonth)
        switch monthNum {
        case 1:
            self.navigationItem.title = "January"
        case 2:
            self.navigationItem.title = "February"
        case 3:
            self.navigationItem.title = "March"
        case 4:
            self.navigationItem.title = "April"
        case 5:
            self.navigationItem.title = "May"
        case 6:
            self.navigationItem.title = "June"
        case 7:
            self.navigationItem.title = "July"
        case 8:
            self.navigationItem.title = "August"
        case 9:
            self.navigationItem.title = "September"
        case 10:
            self.navigationItem.title = "October"
        case 11:
            self.navigationItem.title = "November"
        case 12:
            self.navigationItem.title = "December"
        default:
            break
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "yearlyMonthInfoSegue" {
            if let shiftDetailVC = segue.destination as? shiftDetailTableViewController {
                shiftDetailVC.shiftDate = shiftDate
                
                shiftDetailVC.shiftTotal = shiftTotal
                shiftDetailVC.shiftTipTotal = shiftTipTotal
                shiftDetailVC.shiftTotalWage = shiftTotalHourlyWage
                
                shiftDetailVC.shiftJobName = shiftJobName
                shiftDetailVC.shiftPosition = shiftPosition
                shiftDetailVC.shiftRate = shiftHourlyRate
                
                shiftDetailVC.shiftHours = shiftHours
                shiftDetailVC.shiftStartTime = shiftClockIn
                shiftDetailVC.shiftEndTime = shiftClockOut
                
                shiftDetailVC.shiftNotes = shiftNotes
                
                
                
            }
        }
    }
        
}

// MARK: - TableView Data
extension historyYearMonthDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        YearlyMonthDetailArray = YearlyMonthDetailArray.reversed()
        return(YearlyMonthDetailArray.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "currentShiftCell") as! currentShiftTableViewCell
            let date = YearlyMonthDetailArray[indexPath.row].date
            let total = YearlyMonthDetailArray[indexPath.row].shiftTotal
            let tips = YearlyMonthDetailArray[indexPath.row].tipTotal
            let wage = YearlyMonthDetailArray[indexPath.row].totalHourlyWage
            
            cell.shiftDateTextLabel.text = date
            cell.shiftTotalTextLabel.text = total
            cell.shiftTotalTipsTextLabel.text = tips
            cell.shiftTotalWageTextLabel.text = wage
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "previousShiftCell") as! prevShiftTableViewCell
            let date = YearlyMonthDetailArray[indexPath.row].date
            let total = YearlyMonthDetailArray[indexPath.row].shiftTotal
            let tips = YearlyMonthDetailArray[indexPath.row].tipTotal
            let wage = YearlyMonthDetailArray[indexPath.row].totalHourlyWage
            
            cell.shiftDateTextLabel.text = date
            cell.shiftTotalTextLabel.text = total
            cell.shiftTotalTipsTextLabel.text = tips
            cell.shiftTotalWageTextLabel.text = wage
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 0) {
            return 150
        }
        return 86
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0) {
            let cell = tableView.cellForRow(at: indexPath) as! currentShiftTableViewCell
            shiftDate = cell.shiftDateTextLabel.text
            shiftTipTotal = cell.shiftTotalTipsTextLabel.text
            shiftTotal = cell.shiftTotalTextLabel.text
            shiftTotalHourlyWage = cell.shiftTotalWageTextLabel.text
            shiftHours = YearlyMonthDetailArray[indexPath.row].Hours
            shiftNotes = YearlyMonthDetailArray[indexPath.row].shiftNotes
            shiftClockIn = YearlyMonthDetailArray[indexPath.row].clockIn
            shiftClockOut = YearlyMonthDetailArray[indexPath.row].clockOut
            shiftJobName = GlobalVariable.jobName
            shiftPosition = YearlyMonthDetailArray[indexPath.row].position
            shiftHourlyRate = YearlyMonthDetailArray[indexPath.row].hourlyRate
            
            performSegue(withIdentifier: "yearlyMonthInfoSegue", sender: Any?.self)
        } else {
            
            let cell = tableView.cellForRow(at: indexPath) as! prevShiftTableViewCell
            shiftDate = cell.shiftDateTextLabel.text
            shiftTipTotal = cell.shiftTotalTipsTextLabel.text
            shiftTotal = cell.shiftTotalTextLabel.text
            shiftTotalHourlyWage = cell.shiftTotalWageTextLabel.text
            shiftHours = YearlyMonthDetailArray[indexPath.row].Hours
            shiftNotes = YearlyMonthDetailArray[indexPath.row].shiftNotes
            shiftClockIn = YearlyMonthDetailArray[indexPath.row].clockIn
            shiftClockOut = YearlyMonthDetailArray[indexPath.row].clockOut
            shiftJobName = GlobalVariable.jobName
            shiftPosition = YearlyMonthDetailArray[indexPath.row].position
            shiftHourlyRate = YearlyMonthDetailArray[indexPath.row].hourlyRate
            
            performSegue(withIdentifier: "yearlyMonthInfoSegue", sender: Any?.self)
        }
    }
    
}
