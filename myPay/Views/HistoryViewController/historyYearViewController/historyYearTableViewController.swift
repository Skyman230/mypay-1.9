//
//  historyYearTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/29/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit
import  Charts

class historyYearTableViewController: UITableViewController {
    
    var allButtonsPressed = false {
        didSet{
            if let items = tabBarController?.tabBar.items {
                items[0].isEnabled = allButtonsPressed
                items[1].isEnabled = allButtonsPressed
                items[2].isEnabled = allButtonsPressed
                items[3].isEnabled = allButtonsPressed
                items[4].isEnabled = allButtonsPressed
            }
        }
    }
    
    // MARK: - IBOutlets
    // MARK: LineChartView
    @IBOutlet weak var lineChartView1: LineChartView!
    @IBOutlet weak var lineChartView2: LineChartView!
    @IBOutlet weak var lineChartView3: LineChartView!
    
    // MARK: UILabel
    @IBOutlet weak var yearlyTotalTextLabel: UILabel!
    @IBOutlet weak var yearlyTipTotalTextLabel: UILabel!
    @IBOutlet weak var yearlyWageTotalTextLabel: UILabel!
    
    @IBOutlet weak var januaryTextLabel: UILabel!
    @IBOutlet weak var februaryTextLabel: UILabel!
    @IBOutlet weak var marchTextLabel: UILabel!
    @IBOutlet weak var aprilTextLabel: UILabel!
    @IBOutlet weak var mayTextLabel: UILabel!
    @IBOutlet weak var juneTextLabel: UILabel!
    @IBOutlet weak var julyTextLabel: UILabel!
    @IBOutlet weak var augustTextLabel: UILabel!
    @IBOutlet weak var septemberTextLabel: UILabel!
    @IBOutlet weak var octoberTextLabel: UILabel!
    @IBOutlet weak var novemberTextLabel: UILabel!
    @IBOutlet weak var decemberTextLabel: UILabel!
    
    @IBOutlet weak var januaryTotalTextLabel: UILabel!
    @IBOutlet weak var februaryTotalTextLabel: UILabel!
    @IBOutlet weak var marchTotalTextLabel: UILabel!
    @IBOutlet weak var mayTotalTextLabel: UILabel!
    @IBOutlet weak var aprilTotalTextLabel: UILabel!
    @IBOutlet weak var juneTotalTextLabel: UILabel!
    @IBOutlet weak var julyTotalTextLabel: UILabel!
    @IBOutlet weak var augustTotalTextLabel: UILabel!
    @IBOutlet weak var septemberTotalTextLabel: UILabel!
    @IBOutlet weak var octoberTotalTextLabel: UILabel!
    @IBOutlet weak var novemberTotalTextLabel: UILabel!
    @IBOutlet weak var decemberTotalTextLabel: UILabel!
    
    // MARK: - var
    // MARK: Double
    var numbers1 = [Double]()
    var numbers2 = [Double]()
    var numbers3 = [Double]()
    
    // MARK: IndexPath
    let januaryCellIndexPath = IndexPath(row: 10, section: 0)
    let februaryCellIndexPath = IndexPath(row: 11, section: 0)
    let marchCellIndexPath = IndexPath(row: 12, section: 0)
    let aprilCellIndexPath = IndexPath(row: 13, section: 0)
    let mayCellIndexPath = IndexPath(row: 14, section: 0)
    let juneCellIndexPath = IndexPath(row: 15, section: 0)
    let julyCellIndexPath = IndexPath(row: 16, section: 0)
    let augustCellIndexPath = IndexPath(row: 17, section: 0)
    let septemberCellIndexPath = IndexPath(row: 18, section: 0)
    let octoberCellIndexPath = IndexPath(row: 19, section: 0)
    let novemberCellIndexPath = IndexPath(row: 20, section: 0)
    let decemberCellIndexPath = IndexPath(row: 21, section: 0)
    
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allButtonsPressed = false
        CalculateYearsTotals.showTotals()
        showFigures()
        lineChartInfo()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        allButtonsPressed = false
        CalculateYearsTotals.showTotals()
        showFigures()
        lineChartInfo()
    }
    
    //MARK: - func
    func showFigures() {
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        let nameOfMonth = dateFormatter.string(from: now)
        let monthNum = Int(nameOfMonth)
        
        CalculateSums.showFigures()
        CalculateYearsTotals.showTotals()
        if yearlyGrossArray.count == 0 {
            yearlyTotalTextLabel.text = "No Data Available"
            yearlyTotalTextLabel.textColor = UIColor.gray
        } else {
            yearlyTotalTextLabel.text = GlobalVariable.yearlyTotal
            yearlyTotalTextLabel.textColor = UIColor.white
        }
        if yearlyGrossArray.count == 0 {
            yearlyTipTotalTextLabel.text = "No Data Available"
            yearlyTipTotalTextLabel.textColor = UIColor.gray
        } else {
            yearlyTipTotalTextLabel.text = GlobalVariable.yearlyTips
            yearlyTipTotalTextLabel.textColor = UIColor.white
        }
        if yearlyGrossArray.count == 0 {
            yearlyWageTotalTextLabel.text = "No Data Available"
            yearlyWageTotalTextLabel.textColor = UIColor.gray
        } else {
            yearlyWageTotalTextLabel.text = GlobalVariable.yearlyWage
            yearlyWageTotalTextLabel.textColor = UIColor.white
        }
        if JanuaryArray.count == 0 {
            if monthNum! >= 1 {
                januaryTotalTextLabel.text = "No Data Available"
                januaryTotalTextLabel.textColor = UIColor.gray
                januaryTextLabel.textColor = UIColor.white
            } else {
                januaryTotalTextLabel.text = ""
            }
        } else {
            januaryTotalTextLabel.text = GlobalVariable.JanuaryTotal
            januaryTotalTextLabel.textColor = UIColor.white
            januaryTextLabel.textColor = UIColor.white
            print(JanuaryArray)
            print(FebruaryArray)
            print(MarchArray)
            print(AprilArray)
        }
        if FebruaryArray.count == 0 {
            if monthNum! >= 2 {
                februaryTotalTextLabel.text = "No Data Available"
                februaryTotalTextLabel.textColor = UIColor.gray
                februaryTextLabel.textColor = UIColor.white
            } else {
                februaryTotalTextLabel.text = ""
            }
        } else {
            februaryTotalTextLabel.text = GlobalVariable.FebruaryTotal
            februaryTotalTextLabel.textColor = UIColor.white
            februaryTextLabel.textColor = UIColor.white
        }
        if MarchArray.count == 0 {
            if monthNum! >= 3 {
                marchTotalTextLabel.text = "No Data Available"
                marchTotalTextLabel.textColor = UIColor.gray
                marchTextLabel.textColor = UIColor.white
            } else {
                marchTotalTextLabel.text = ""
            }
        } else {
            marchTotalTextLabel.text = GlobalVariable.MarchTotal
            marchTotalTextLabel.textColor = UIColor.white
            marchTextLabel.textColor = UIColor.white
        }
        if AprilArray.count == 0 {
            if monthNum! >= 4 {
                aprilTotalTextLabel.text = "No Data Available"
                aprilTotalTextLabel.textColor = UIColor.gray
                aprilTextLabel.textColor = UIColor.white
            } else {
                aprilTotalTextLabel.text = ""
            }
        } else {
            aprilTotalTextLabel.text = GlobalVariable.AprilTotal
            aprilTotalTextLabel.textColor = UIColor.white
            aprilTextLabel.textColor = UIColor.white
        }
        if MayArray.count == 0 {
            if monthNum! >= 5 {
                mayTotalTextLabel.text = "No Data Available"
                mayTotalTextLabel.textColor = UIColor.gray
                mayTextLabel.textColor = UIColor.white
            } else {
                mayTotalTextLabel.text = ""
            }
        } else {
            mayTotalTextLabel.text = GlobalVariable.MayTotal
            mayTotalTextLabel.textColor = UIColor.white
            mayTextLabel.textColor = UIColor.white
        }
        if JuneArray.count == 0 {
            if monthNum! >= 6 {
                juneTotalTextLabel.text = "No Data Available"
                juneTotalTextLabel.textColor = UIColor.gray
                juneTextLabel.textColor = UIColor.white
            } else {
                juneTotalTextLabel.text = ""
            }
        } else {
            juneTotalTextLabel.text = GlobalVariable.JuneTotal
            juneTotalTextLabel.textColor = UIColor.white
            juneTextLabel.textColor = UIColor.white
        }
        if JulyArray.count == 0 {
            if monthNum! >= 7 {
                julyTotalTextLabel.text = "No Data Available"
                julyTotalTextLabel.textColor = UIColor.gray
                julyTextLabel.textColor = UIColor.white
            } else {
                julyTotalTextLabel.text = ""
            }
        } else {
            julyTotalTextLabel.text = GlobalVariable.JulyTotal
            julyTotalTextLabel.textColor = UIColor.white
            julyTextLabel.textColor = UIColor.white
        }
        if AugustArray.count == 0 {
            if monthNum! >= 8 {
                augustTotalTextLabel.text = "No Data Available"
                augustTotalTextLabel.textColor = UIColor.gray
                augustTextLabel.textColor = UIColor.white
            } else {
                augustTotalTextLabel.text = ""
            }
        } else {
            augustTotalTextLabel.text = GlobalVariable.AugustTotal
            augustTotalTextLabel.textColor = UIColor.white
            augustTextLabel.textColor = UIColor.white
        }
        if SeptemberArray.count == 0 {
            if monthNum! >= 9 {
                septemberTotalTextLabel.text = "No Data Available"
                septemberTotalTextLabel.textColor = UIColor.gray
                septemberTextLabel.textColor = UIColor.white
            } else {
                septemberTotalTextLabel.text = ""
            }
        } else {
            septemberTotalTextLabel.text = GlobalVariable.SeptemberTotal
            septemberTotalTextLabel.textColor = UIColor.white
            septemberTextLabel.textColor = UIColor.white
        }
        if OctoberArray.count == 0 {
            if monthNum! >= 10 {
                octoberTotalTextLabel.text = "No Data Available"
                octoberTotalTextLabel.textColor = UIColor.gray
                octoberTextLabel.textColor = UIColor.white
            } else {
                octoberTotalTextLabel.text = ""
            }
        } else {
            octoberTotalTextLabel.text = GlobalVariable.OctoberTotal
            octoberTotalTextLabel.textColor = UIColor.white
            octoberTextLabel.textColor = UIColor.white
        }
        if NovemberArray.count == 0 {
            if monthNum! >= 11 {
                novemberTotalTextLabel.text = "No Data Available"
                novemberTotalTextLabel.textColor = UIColor.gray
                novemberTextLabel.textColor = UIColor.white
            } else {
                novemberTotalTextLabel.text = ""
            }
        } else {
            novemberTotalTextLabel.text = GlobalVariable.NovemberTotal
            novemberTotalTextLabel.textColor = UIColor.white
            novemberTextLabel.textColor = UIColor.white
        }
        if DecemberArray.count == 0 {
            if monthNum! >= 12 {
                decemberTotalTextLabel.text = "No Data Available"
                decemberTotalTextLabel.textColor = UIColor.gray
                decemberTextLabel.textColor = UIColor.white
            } else {
                decemberTotalTextLabel.text = ""
            }
        } else {
            decemberTotalTextLabel.text = GlobalVariable.DecemberTotal
            decemberTotalTextLabel.textColor = UIColor.white
            decemberTextLabel.textColor = UIColor.white
        }
    }
    
    func lineChartInfo() {
        var January = String()
        var February = String()
        var March = String()
        var April = String()
        var May = String()
        var June = String()
        var July = String()
        var August = String()
        var September = String()
        var October = String()
        var November = String()
        var December = String()
        
        if GlobalVariable.JanuaryTotal.isEmpty {
            January = "$00.00"
        } else {
            January = GlobalVariable.JanuaryTotal
        }
        if GlobalVariable.FebruaryTotal.isEmpty {
            February = "$00.00"
        } else {
            February = GlobalVariable.FebruaryTotal
        }
        if GlobalVariable.MarchTotal.isEmpty {
            March = "$00.00"
        } else {
            March = GlobalVariable.MarchTotal
        }
        if GlobalVariable.AprilTotal.isEmpty {
            April = "$00.00"
        } else {
            April = GlobalVariable.AprilTotal
        }
        if GlobalVariable.MayTotal.isEmpty {
            May = "$00.00"
        } else {
            May = GlobalVariable.MayTotal
        }
        if GlobalVariable.JuneTotal.isEmpty {
            June = "$00.00"
        } else {
            June = GlobalVariable.JuneTotal
        }
        if GlobalVariable.JulyTotal.isEmpty {
            July = "$00.00"
        } else {
            July = GlobalVariable.JulyTotal
        }
        if GlobalVariable.AugustTotal.isEmpty {
            August = "$00.00"
        } else {
            August = GlobalVariable.AugustTotal
        }
        if GlobalVariable.SeptemberTotal.isEmpty {
            September = "$00.00"
        } else {
            September = GlobalVariable.SeptemberTotal
        }
        if GlobalVariable.OctoberTotal.isEmpty {
            October = "$00.00"
        } else {
            October = GlobalVariable.OctoberTotal
        }
        if GlobalVariable.NovemberTotal.isEmpty {
            November = "$00.00"
        } else {
            November = GlobalVariable.NovemberTotal
        }
        if GlobalVariable.DecemberTotal.isEmpty {
            December = "$00.00"
        } else {
            December = GlobalVariable.DecemberTotal
        }
        let YearlyGrossArray = [January, February, March, April, May, June, July, August, September, October, November, December]
        let yearSumArray = YearlyGrossArray.map{convertCurrencyToDouble(input: $0)}
        numbers1 = yearSumArray as! [Double]
        updateGraph1()
        
        var JanuaryTips = String()
        var FebruaryTips = String()
        var MarchTips = String()
        var AprilTips = String()
        var MayTips = String()
        var JuneTips = String()
        var JulyTips = String()
        var AugustTips = String()
        var SeptemberTips = String()
        var OctoberTips = String()
        var NovemberTips = String()
        var DecemberTips = String()
        if GlobalVariable.JanuaryTips.isEmpty {
            JanuaryTips = "$00.00"
        } else {
            JanuaryTips = GlobalVariable.JanuaryTips
        }
        if GlobalVariable.FebruaryTips.isEmpty {
            FebruaryTips = "$00.00"
        } else {
            FebruaryTips = GlobalVariable.FebruaryTips
        }
        if GlobalVariable.MarchTips.isEmpty {
            MarchTips = "$00.00"
        } else {
            MarchTips = GlobalVariable.MarchTips
        }
        if GlobalVariable.AprilTips.isEmpty {
            AprilTips = "$00.00"
        } else {
            AprilTips = GlobalVariable.AprilTips
        }
        if GlobalVariable.MayTips.isEmpty {
            MayTips = "$00.00"
        } else {
            MayTips = GlobalVariable.MayTips
        }
        if GlobalVariable.JuneTips.isEmpty {
            JuneTips = "$00.00"
        } else {
            JuneTips = GlobalVariable.JuneTips
        }
        if GlobalVariable.JulyTips.isEmpty {
            JulyTips = "$00.00"
        } else {
            JulyTips = GlobalVariable.JulyTips
        }
        if GlobalVariable.AugustTips.isEmpty {
            AugustTips = "$00.00"
        } else {
            AugustTips = GlobalVariable.AugustTips
        }
        if GlobalVariable.SeptemberTips.isEmpty {
            SeptemberTips = "$00.00"
        } else {
            SeptemberTips = GlobalVariable.SeptemberTips
        }
        if GlobalVariable.OctoberTips.isEmpty {
            OctoberTips = "$00.00"
        } else {
            OctoberTips = GlobalVariable.OctoberTips
        }
        if GlobalVariable.NovemberTips.isEmpty {
            NovemberTips = "$00.00"
        } else {
            NovemberTips = GlobalVariable.NovemberTips
        }
        if GlobalVariable.DecemberTips.isEmpty {
            DecemberTips = "$00.00"
        } else {
            DecemberTips = GlobalVariable.DecemberTips
        }
        let yearlyTipsArray = [JanuaryTips, FebruaryTips, MarchTips, AprilTips, MayTips, JuneTips, JulyTips, AugustTips, SeptemberTips, OctoberTips, NovemberTips, DecemberTips]
        let yearlyTips = yearlyTipsArray.map{convertCurrencyToDouble(input: $0)}
        numbers2 = yearlyTips as! [Double]
        updateGraph2()
        
        var JanuaryWages = String()
        var FebruaryWages = String()
        var MarchWages = String()
        var AprilWages = String()
        var MayWages = String()
        var JuneWages = String()
        var JulyWages = String()
        var AugustWages = String()
        var SeptemberWages = String()
        var OctoberWages = String()
        var NovemberWages = String()
        var DecemberWages = String()
        if GlobalVariable.JanuaryWages.isEmpty {
            JanuaryWages = "$00.00"
        } else {
            JanuaryWages = GlobalVariable.JanuaryWages
        }
        if GlobalVariable.FebruaryWages.isEmpty {
            FebruaryWages = "$00.00"
        } else {
            FebruaryWages = GlobalVariable.FebruaryWages
        }
        if GlobalVariable.MarchWages.isEmpty {
            MarchWages = "$00.00"
        } else {
            MarchWages = GlobalVariable.MarchWages
        }
        if GlobalVariable.AprilWages.isEmpty {
            AprilWages = "$00.00"
        } else {
            AprilWages = GlobalVariable.AprilWages
        }
        if GlobalVariable.MayWages.isEmpty {
            MayWages = "$00.00"
        } else {
            MayWages = GlobalVariable.MayWages
        }
        if GlobalVariable.JuneWages.isEmpty {
            JuneWages = "$00.00"
        } else {
            JuneWages = GlobalVariable.JuneWages
        }
        if GlobalVariable.JulyWages.isEmpty {
            JulyWages = "$00.00"
        } else {
            JulyWages = GlobalVariable.JulyWages
        }
        if GlobalVariable.AugustWages.isEmpty {
            AugustWages = "$00.00"
        } else {
            AugustWages = GlobalVariable.AugustWages
        }
        if GlobalVariable.SeptemberWages.isEmpty {
            SeptemberWages = "$00.00"
        } else {
            SeptemberWages = GlobalVariable.SeptemberWages
        }
        if GlobalVariable.OctoberWages.isEmpty {
            OctoberWages = "$00.00"
        } else {
            OctoberWages = GlobalVariable.OctoberWages
        }
        if GlobalVariable.NovemberWages.isEmpty {
            NovemberWages = "$00.00"
        } else {
            NovemberWages = GlobalVariable.NovemberWages
        }
        if GlobalVariable.DecemberWages.isEmpty {
            DecemberWages = "$00.00"
        } else {
            DecemberWages = GlobalVariable.DecemberWages
        }
        let yearlyWageArray = [JanuaryWages, FebruaryWages, MarchWages, AprilWages, MayWages, JuneWages, JulyWages, AugustWages, SeptemberWages, OctoberWages, NovemberWages, DecemberWages]
        let yearlyWage = yearlyWageArray.map{convertCurrencyToDouble(input: $0)}
        numbers3 = yearlyWage as! [Double]
        updateGraph3()
        
    }
    // MARK: Table View Data
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == januaryCellIndexPath.section &&
            indexPath.row == januaryCellIndexPath.row {
            if JanuaryArray.count > 0 {
                YearlyMonthDetailArray.removeAll()
                YearlyMonthDetailArray = JanuaryArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print(JanuaryArray)
            print(YearlyMonthDetailArray)
        }
        if indexPath.section == februaryCellIndexPath.section &&
            indexPath.row == februaryCellIndexPath.row {
            if FebruaryArray.count > 0 {
                YearlyMonthDetailArray = FebruaryArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print(FebruaryArray)
            print(YearlyMonthDetailArray)
        }
        if indexPath.section == marchCellIndexPath.section &&
            indexPath.row == marchCellIndexPath.row {
            if MarchArray.count > 0 {
                YearlyMonthDetailArray = MarchArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("March Totals Pressed")
        }
        if indexPath.section == aprilCellIndexPath.section &&
            indexPath.row == aprilCellIndexPath.row {
            if AprilArray.count > 0 {
                YearlyMonthDetailArray = AprilArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("April Totals Pressed")
        }
        if indexPath.section == mayCellIndexPath.section &&
            indexPath.row == mayCellIndexPath.row {
            if MayArray.count > 0 {
                YearlyMonthDetailArray = MayArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("May Totals Pressed")
        }
        if indexPath.section == juneCellIndexPath.section &&
            indexPath.row == juneCellIndexPath.row {
            if JuneArray.count > 0 {
                YearlyMonthDetailArray = JuneArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("June Totals Pressed")
        }
        if indexPath.section == julyCellIndexPath.section &&
            indexPath.row == julyCellIndexPath.row {
            if JulyArray.count > 0 {
                YearlyMonthDetailArray = JulyArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("July Totals Pressed")
        }
        if indexPath.section == augustCellIndexPath.section &&
            indexPath.row == augustCellIndexPath.row {
            if AugustArray.count > 0 {
                YearlyMonthDetailArray = AugustArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("August Totals Pressed")
        }
        if indexPath.section == septemberCellIndexPath.section &&
            indexPath.row == septemberCellIndexPath.row {
            if SeptemberArray.count > 0 {
                YearlyMonthDetailArray = SeptemberArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("September Totals Pressed")
        }
        if indexPath.section == octoberCellIndexPath.section &&
            indexPath.row == octoberCellIndexPath.row {
            if OctoberArray.count > 0 {
                YearlyMonthDetailArray = OctoberArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("October Totals Pressed")
        }
        if indexPath.section == novemberCellIndexPath.section &&
            indexPath.row == novemberCellIndexPath.row {
            if NovemberArray.count > 0 {
                YearlyMonthDetailArray = NovemberArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("November Totals Pressed")
        }
        if indexPath.section == decemberCellIndexPath.section &&
            indexPath.row == decemberCellIndexPath.row {
            if DecemberArray.count > 0 {
                YearlyMonthDetailArray = DecemberArray
                performSegue(withIdentifier: "monthDetailSegue", sender: Any?.self)
            }
            print("December Totals Pressed")
        }
    }
}

// MARK: - lineChart Data
extension historyYearTableViewController {
    
    func updateGraph1(){
        var lineChartEntry = [ChartDataEntry]()
        
        for i in 0..<numbers1.count {
            
            let value = ChartDataEntry(x: Double(i), y: numbers1[i])
            
            lineChartEntry.append(value)
        }
        
        let line1 = LineChartDataSet(values: lineChartEntry, label: "Yearly Totals")
        
        line1.colors = [UIColor.blue]
        
        let data = LineChartData()
        
        data.addDataSet(line1)
        lineChartView1.data = data
        lineChartView1.chartDescription?.text = ""
    }
    
    func updateGraph2(){
        var lineChartEntry2 = [ChartDataEntry]()
        
        for i in 0..<numbers2.count {
            
            let value = ChartDataEntry(x: Double(i), y: numbers2[i])
            
            lineChartEntry2.append(value)
        }
        
        let line2 = LineChartDataSet(values: lineChartEntry2, label: "Yearly Tips")
        
        line2.colors = [UIColor.red]
        
        let data = LineChartData()
        
        data.addDataSet(line2)
        lineChartView2.data = data
        lineChartView2.chartDescription?.text = ""
    }
    
    func updateGraph3(){
        var lineChartEntry3 = [ChartDataEntry]()
        
        for i in 0..<numbers3.count {
            
            let value = ChartDataEntry(x: Double(i), y: numbers3[i])
            
            lineChartEntry3.append(value)
        }
        
        let line3 = LineChartDataSet(values: lineChartEntry3, label: "Yearly Wges")
        
        line3.colors = [UIColor.orange]
        
        let data = LineChartData()
        
        data.addDataSet(line3)
        lineChartView3.data = data
        lineChartView3.chartDescription?.text = ""
    }
}
