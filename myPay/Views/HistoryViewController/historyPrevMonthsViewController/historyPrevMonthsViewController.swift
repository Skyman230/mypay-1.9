//
//  historyPrevMonthsViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/29/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class historyPrevMonthsViewController: UIViewController {
    
    // MARK: - UIOutlets
    // MARK: UIView
    @IBOutlet weak var januaryViewContainer: UIView!
    @IBOutlet weak var februaryViewContainer: UIView!
    @IBOutlet weak var marchViewContainer: UIView!
    @IBOutlet weak var aprilViewContainer: UIView!
    @IBOutlet weak var mayViewContainer: UIView!
    @IBOutlet weak var juneViewContainer: UIView!
    @IBOutlet weak var julyViewContainer: UIView!
    @IBOutlet weak var augustViewContainer: UIView!
    @IBOutlet weak var septemberViewContainer: UIView!
    @IBOutlet weak var octoberViewContainer: UIView!
    @IBOutlet weak var novemberViewContainer: UIView!
    @IBOutlet weak var decemberViewContainer: UIView!
    
    // MARK: - vars
    // MARK: DateComponent
    var dateComponents = DateComponents()
    
    // MARK: Int
    var selectedMonth = Int()
    var selectedDay = Int()
    var SelectedYear = Int()
    
    
    // MARK: - Load View
    override func viewDidLoad() {
        super.viewDidLoad()

        hideMonths()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        hideMonths()
        
    }
    
    // MARK: - func
    func hideMonths() {
        januaryViewContainer.isHidden = true
        februaryViewContainer.isHidden = true
        marchViewContainer.isHidden = true
        aprilViewContainer.isHidden = true
        mayViewContainer.isHidden = true
        juneViewContainer.isHidden = true
        julyViewContainer.isHidden = true
        augustViewContainer.isHidden = true
        septemberViewContainer.isHidden = true
        octoberViewContainer.isHidden = true
        novemberViewContainer.isHidden = true
        decemberViewContainer.isHidden = true
        showMonthLegend()
    }
    
    func showMonthLegend() {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "L"
        let nameOfMonth = dateFormatter.string(from: now)
        let monthNum = Int(nameOfMonth)
        let legendMonth = monthNum! - 1
        switch legendMonth {
        case 1:
            januaryViewContainer.isHidden = false
        case 2:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
        case 3:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
        case 4:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
            aprilViewContainer.isHidden = false
        case 5:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
            aprilViewContainer.isHidden = false
            mayViewContainer.isHidden = false
        case 6:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
            aprilViewContainer.isHidden = false
            mayViewContainer.isHidden = false
            juneViewContainer.isHidden = false
        case 7:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
            aprilViewContainer.isHidden = false
            mayViewContainer.isHidden = false
            juneViewContainer.isHidden = false
            julyViewContainer.isHidden = false
        case 8:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
            aprilViewContainer.isHidden = false
            mayViewContainer.isHidden = false
            juneViewContainer.isHidden = false
            julyViewContainer.isHidden = false
            augustViewContainer.isHidden = false
        case 9:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
            aprilViewContainer.isHidden = false
            mayViewContainer.isHidden = false
            juneViewContainer.isHidden = false
            julyViewContainer.isHidden = false
            augustViewContainer.isHidden = false
            septemberViewContainer.isHidden = false
        case 10:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
            aprilViewContainer.isHidden = false
            mayViewContainer.isHidden = false
            juneViewContainer.isHidden = false
            julyViewContainer.isHidden = false
            augustViewContainer.isHidden = false
            septemberViewContainer.isHidden = false
            octoberViewContainer.isHidden = false
        case 11:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
            aprilViewContainer.isHidden = false
            mayViewContainer.isHidden = false
            juneViewContainer.isHidden = false
            julyViewContainer.isHidden = false
            augustViewContainer.isHidden = false
            septemberViewContainer.isHidden = false
            octoberViewContainer.isHidden = false
            novemberViewContainer.isHidden = false
        case 12:
            januaryViewContainer.isHidden = false
            februaryViewContainer.isHidden = false
            marchViewContainer.isHidden = false
            aprilViewContainer.isHidden = false
            mayViewContainer.isHidden = false
            juneViewContainer.isHidden = false
            julyViewContainer.isHidden = false
            augustViewContainer.isHidden = false
            septemberViewContainer.isHidden = false
            octoberViewContainer.isHidden = false
            novemberViewContainer.isHidden = false
            decemberViewContainer.isHidden = false
        default:
            break
        }
    }
    
    // MARK: - IBActions
    // MARK: Button
    @IBAction func januaryBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 00
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 1
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func februaryBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 01
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 2
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func marchBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 02
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 3
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func aprilBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 03
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 4
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func mayBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 04
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 5
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func juneBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 05
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 6
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func julyBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 06
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 7
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func augustBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 07
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 8
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func septemberBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 08
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 9
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func octoberBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 09
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 10
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func novemberBtnPressed(_ sender: UIButton) {
        
        selectedMonth    = 10
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 11
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    @IBAction func decemberBtnPressed(_ sender: Any) {
        
        selectedMonth    = 11
        SelectedYear = 2019
        GlobalVariable.CurrentMonthEditing = 12
        
        performSegue(withIdentifier: "monthCalendarSegue", sender: Any.self)
        
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = segue.destination as? historyPrevMonthCalendarViewController
        dest?.Year = SelectedYear
        dest?.Month = selectedMonth
    }
    

}
