//
//  HistoryViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/27/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit
import Charts

class HistoryViewController: UIViewController {
    
    
    var allButtonsPressed = false {
        didSet{
            if let items = tabBarController?.tabBar.items {
                items[0].isEnabled = allButtonsPressed
                items[1].isEnabled = allButtonsPressed
                items[2].isEnabled = allButtonsPressed
                items[3].isEnabled = allButtonsPressed
                items[4].isEnabled = allButtonsPressed
            }
        }
    }
    @IBOutlet weak var lineChartView1: LineChartView!
    @IBOutlet weak var lineChartView2: LineChartView!
    @IBOutlet weak var lineChartView3: LineChartView!
    
    var number1: [Double]!
    var numbers2: [Double]!
    var numbers3 = [Double]()
    var ppTips = Double()
    var ppWage =  Double()
    var ppHours =  Double()
    var mTips = Double()
    var mWage =  Double()
    var mHours =  Double()
    var yTips = Double()
    var yWage =  Double()
    var yHours =  Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allButtonsPressed = true
        showLineChartInfo()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        allButtonsPressed = true
        showLineChartInfo()
    }
    
    func updateGraph(){
        var lineChartEntry = [ChartDataEntry]()
        
        for i in 0..<number1.count {
            
            let value = ChartDataEntry(x: Double(i), y: number1[i])
            
            lineChartEntry.append(value)
        }
        
        let line1 = LineChartDataSet(values: lineChartEntry, label: "Pay Period Totals")
        
        line1.colors = [UIColor.blue]
        
        let data = LineChartData()
        
        data.addDataSet(line1)
        lineChartView1.data = data
        lineChartView1.chartDescription?.text = "Chart of Tips and Wage Totals"
    }
    
    func updateGraph2(){
        var lineChartEntry2 = [ChartDataEntry]()
        
        for i in 0..<numbers2.count {
            
            let value = ChartDataEntry(x: Double(i), y: numbers2[i])
            
            lineChartEntry2.append(value)
        }
        
        let line2 = LineChartDataSet(values: lineChartEntry2, label: "Month Totals")
        
        line2.colors = [UIColor.blue]
        
        let data = LineChartData()
        
        data.addDataSet(line2)
        lineChartView2.data = data
        lineChartView2.chartDescription?.text = "Chart of Tips and Wage Totals"
    }
    
    func updateGraph3(){
        var lineChartEntry3 = [ChartDataEntry]()
        
        for i in 0..<numbers3.count {
            
            let value = ChartDataEntry(x: Double(i), y: numbers3[i])
            
            lineChartEntry3.append(value)
        }
        
        let line3 = LineChartDataSet(values: lineChartEntry3, label: "Yearly Totals")
        
        line3.colors = [UIColor.orange]
        
        let data = LineChartData()
        
        data.addDataSet(line3)
        lineChartView3.data = data
        lineChartView3.chartDescription?.text = "Chart of Year to Date Monthly Totals"
    }
    
    
    // MARK: - Navigation
//    @IBAction func unwindFromAddVC(_ sender: UIStoryboardSegue){
//        if sender.source is historyMonthCalendarViewController {
//            updateGraph()
//            updateGraph2()
//            updateGraph3()
//        }
//        if sender.source is MonthsViewController {
//            updateGraph()
//            updateGraph2()
//            updateGraph3()
//        }
//    }
}

extension HistoryViewController {
    
    func showLineChartInfo() {
        
        CalculateSums.showFigures()
        
        ppWage = convertCurrencyToDouble(input: GlobalVariable.GrossWage ?? "$000.00")!
        ppTips = convertCurrencyToDouble(input: GlobalVariable.PayPeriodTips ?? "$000.00")!
        ppHours = (GlobalVariable.PayPeriodHours! as NSString).doubleValue
        number1 = [ppTips, ppWage, ppHours]
        updateGraph()
        
        mWage = convertCurrencyToDouble(input: GlobalVariable.MonthlyGross ?? "$000.00")!
        mTips = convertCurrencyToDouble(input: GlobalVariable.MonthTips ?? "$000.00")!
        mHours = (GlobalVariable.MonthHour! as NSString).doubleValue
        numbers2 = [mTips, mWage, mHours]
        updateGraph2()
        
        CalculateYearsTotals.showTotals()
        var January = String()
        var February = String()
        var March = String()
        var April = String()
        var May = String()
        var June = String()
        var July = String()
        var August = String()
        var September = String()
        var October = String()
        var November = String()
        var December = String()
        if GlobalVariable.JanuaryTotal.isEmpty {
            January = "$00.00"
        } else {
            January = GlobalVariable.JanuaryTotal
        }
        if GlobalVariable.FebruaryTotal.isEmpty {
            February = "$00.00"
        } else {
            February = GlobalVariable.FebruaryTotal
        }
        if GlobalVariable.MarchTotal.isEmpty {
            March = "$00.00"
        } else {
            March = GlobalVariable.MarchTotal
        }
        if GlobalVariable.AprilTotal.isEmpty {
            April = "$00.00"
        } else {
            April = GlobalVariable.AprilTotal
        }
        if GlobalVariable.MayTotal.isEmpty {
            May = "$00.00"
        } else {
            May = GlobalVariable.MayTotal
        }
        if GlobalVariable.JuneTotal.isEmpty {
            June = "$00.00"
        } else {
            June = GlobalVariable.JuneTotal
        }
        if GlobalVariable.JulyTotal.isEmpty {
            July = "$00.00"
        } else {
            July = GlobalVariable.JulyTotal
        }
        if GlobalVariable.AugustTotal.isEmpty {
            August = "$00.00"
        } else {
            August = GlobalVariable.AugustTotal
        }
        if GlobalVariable.SeptemberTotal.isEmpty {
            September = "$00.00"
        } else {
            September = GlobalVariable.SeptemberTotal
        }
        if GlobalVariable.OctoberTotal.isEmpty {
            October = "$00.00"
        } else {
            October = GlobalVariable.OctoberTotal
        }
        if GlobalVariable.NovemberTotal.isEmpty {
            November = "$00.00"
        } else {
            November = GlobalVariable.NovemberTotal
        }
        if GlobalVariable.DecemberTotal.isEmpty {
            December = "$00.00"
        } else {
            December = GlobalVariable.DecemberTotal
        }
        yearlyGrossArray = [January, February, March, April, May, June, July, August, September, October, November, December]
        let yearSumArray = yearlyGrossArray.map{convertCurrencyToDouble(input: $0)}
        numbers3 = yearSumArray as! [Double]
        updateGraph3()
        
    }
}
