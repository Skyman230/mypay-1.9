//
//  ShiftCalendarViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/21/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class ShiftCalendarViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    
    @IBOutlet weak var YearLabel: UILabel!
    @IBOutlet weak var Calendar: UICollectionView!
    @IBOutlet weak var MonthLabel: UILabel!
    
    
    let Months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    let DaysOfMonth = ["Monday","Thuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    
    var DaysInMonths = [31,28,31,30,31,30,31,31,30,31,30,31]
    
    var currentMonth = String()
    
    var NumberOfEmptyBox = Int()
    
    var NextNumberOfEmptyBox = Int()
    
    var PreviousNumberOfEmptyBox = 0
    
    var Direction = 0
    
    var PositionIndex = 0
    
    var LeapYearCounter = 2
    
    var dayCounter = 0
    
    var highlightdate = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentMonth = Months[month]
        YearLabel.text = "\(year)"
        MonthLabel.text = "\(currentMonth)"
        if weekday == 0 {
            weekday = 7
        }
        
        GetStartDateDayPosition()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Calendar.reloadData()
    }
    
    //-----------(Calculates the number of "empty" boxes at the start of every month")------------------------------------------------------
    
    func GetStartDateDayPosition() {
        switch Direction{
        case 0:
            NumberOfEmptyBox = weekday
            dayCounter = day
            while dayCounter>0 {
                NumberOfEmptyBox = NumberOfEmptyBox - 1
                dayCounter = dayCounter - 1
                if NumberOfEmptyBox == 0 {
                    NumberOfEmptyBox = 7
                }
            }
            if NumberOfEmptyBox == 7 {
                NumberOfEmptyBox = 0
            }
            PositionIndex = NumberOfEmptyBox
        case 1...:
            NextNumberOfEmptyBox = (PositionIndex + DaysInMonths[month])%7
            PositionIndex = NextNumberOfEmptyBox
            
        case -1:
            PreviousNumberOfEmptyBox = (7 - (DaysInMonths[month] - PositionIndex)%7)
            if PreviousNumberOfEmptyBox == 7 {
                PreviousNumberOfEmptyBox = 0
            }
            PositionIndex = PreviousNumberOfEmptyBox
        default:
            fatalError()
        }
    }
    
    //--------------------------------------------------(Next and back buttons)-------------------------------------------------------------
    
    @IBAction func Next(_ sender: Any) {
        highlightdate = -1
        switch currentMonth {
        case "December":
            Direction = 1
            
            month = 0
            year += 1
            
            if LeapYearCounter  < 5 {
                LeapYearCounter += 1
            }
            
            if LeapYearCounter == 4 {
                DaysInMonths[1] = 29
            }
            
            if LeapYearCounter == 5{
                LeapYearCounter = 1
                DaysInMonths[1] = 28
            }
            GetStartDateDayPosition()
            
            currentMonth = Months[month]
            YearLabel.text = "\(year)"
            MonthLabel.text = "\(currentMonth)"
            
            Calendar.reloadData()
        default:
            Direction = 1
            
            GetStartDateDayPosition()
            
            month += 1
            
            currentMonth = Months[month]
            MonthLabel.text = "\(currentMonth)"
            YearLabel.text = "\(year)"
            Calendar.reloadData()
        }
    }
    
    @IBAction func Back(_ sender: Any) {
        highlightdate = -1
        switch currentMonth {
        case "January":
            Direction = -1
            
            month = 11
            year -= 1
            
            if LeapYearCounter > 0{
                LeapYearCounter -= 1
            }
            if LeapYearCounter == 0{
                DaysInMonths[1] = 29
                LeapYearCounter = 4
            }else{
                DaysInMonths[1] = 28
            }
            
            GetStartDateDayPosition()
            
            currentMonth = Months[month]
            YearLabel.text = "\(year)"
            MonthLabel.text = "\(currentMonth)"
            Calendar.reloadData()
            
        default:
            Direction = -1
            
            month -= 1
            
            GetStartDateDayPosition()
            
            currentMonth = Months[month]
            YearLabel.text = "\(year)"
            MonthLabel.text = "\(currentMonth)"
            Calendar.reloadData()
        }
    }
    
    
    //----------------------------------(CollectionView)------------------------------------------------------------------------------------
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch Direction{
        case 0:
            return DaysInMonths[month] + NumberOfEmptyBox
        case 1...:
            return DaysInMonths[month] + NextNumberOfEmptyBox
        case -1:
            return DaysInMonths[month] + PreviousNumberOfEmptyBox
        default:
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Calendar", for: indexPath) as! DateCollectionViewCell
        cell.backgroundColor = UIColor.clear
        
        cell.DateLabel.textColor = UIColor.white
        
        cell.Circle.isHidden = true
        
        cell.eventDot.isHidden = true
        
        if cell.isHidden{
            cell.isHidden = false
        }
        
        switch Direction {      //the first cells that needs to be hidden (if needed) will be negative or zero so we can hide them
        case 0:
            cell.DateLabel.text = "\(indexPath.row + 1 - NumberOfEmptyBox)"
        case 1:
            cell.DateLabel.text = "\(indexPath.row + 1 - NextNumberOfEmptyBox)"
        case -1:
            cell.DateLabel.text = "\(indexPath.row + 1 - PreviousNumberOfEmptyBox)"
        default:
            fatalError()
        }
        
        if Int(cell.DateLabel.text!)! < 1{ //here we hide the negative numbers or zero
            cell.isHidden = true
        }
        
        switch indexPath.row { //weekend days color
        case 6,7,13,14,20,21,27,28,34,35:
            if Int(cell.DateLabel.text!)! > 0 {
                cell.DateLabel.textColor = UIColor.init(displayP3Red: 237/250, green: 214/250, blue: 192/250, alpha: 1)
            }
        default:
            break
        }
        if currentMonth == Months[calendar.component(.month, from: date) - 1] && year == calendar.component(.year, from: date) && indexPath.row + 1 - NumberOfEmptyBox == day{
            cell.Circle.isHidden = false
            cell.DrawCircle()
            
        }
        if highlightdate == indexPath.row {
            cell.backgroundColor = UIColor.init(red: 243.0/255.0, green: 162.0/255.0, blue: 81.0/255.0, alpha: 1.0)
            cell.layer.cornerRadius = cell.frame.width / 2
            cell.DateLabel.textColor = UIColor.black
        }
        switch currentMonth {
        case "January":
            selectedDates = selectedJanuaryDates
        case "February":
            selectedDates = selectedFebruaryDates
        case "March":
            selectedDates = selectedMarchDates
        case "April":
            selectedDates = selectedAprilDates
        case "May":
            selectedDates = selectedMayDates
        case "June":
            selectedDates = selectedJuneDates
        case "July":
            selectedDates = selectedJulyDates
        case "August":
            selectedDates = selectedAugustDates
        case "September":
            selectedDates = selectedSeptemberDates
        case "October":
            selectedDates = selectedOctoberDates
        case "November":
            selectedDates = selectedNovemberDates
        case "December":
            selectedDates = selectedDecemberDates
        default:
            break
        }
        
        let datesSelected = selectedDates.count
        if datesSelected > 0 {
            switch datesSelected {
            case 1:
                if selectedDates[0] == indexPath.row {
                    cell.eventDot.isHidden = false
                }
            case 2:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 3:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 4:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 5:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[4]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 6:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[5]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 7:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 8:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 9:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 10:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 11:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 12:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 13:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 14:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 15:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 16:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 17:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 18:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 19:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 20:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 21:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 22:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 23:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21],selectedDates[22]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 24:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21],selectedDates[22],selectedDates[23]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 25:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21],selectedDates[22],selectedDates[23],selectedDates[24]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 26:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21],selectedDates[22],selectedDates[23],selectedDates[24],selectedDates[25]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 27:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21],selectedDates[22],selectedDates[23],selectedDates[24],selectedDates[25],selectedDates[26]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 28:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21],selectedDates[22],selectedDates[23],selectedDates[24],selectedDates[25],selectedDates[26],selectedDates[27]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 29:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21],selectedDates[22],selectedDates[23],selectedDates[24],selectedDates[25],selectedDates[26],selectedDates[27],selectedDates[28]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 30:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21],selectedDates[22],selectedDates[23],selectedDates[24],selectedDates[25],selectedDates[26],selectedDates[27],selectedDates[28],selectedDates[29]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            case 31:
                switch indexPath.row {
                case selectedDates[0],selectedDates[1],selectedDates[2],selectedDates[3],selectedDates[6],selectedDates[7],selectedDates[8],selectedDates[9],selectedDates[10],selectedDates[11],selectedDates[12],selectedDates[13],selectedDates[14],selectedDates[15],selectedDates[16],selectedDates[17],selectedDates[18],selectedDates[19],selectedDates[20],selectedDates[21],selectedDates[22],selectedDates[23],selectedDates[24],selectedDates[25],selectedDates[26],selectedDates[27],selectedDates[28],selectedDates[29],selectedDates[30]:
                    cell.eventDot.isHidden = false
                default:
                    break
                }
            default:
                cell.eventDot.isHidden = true
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        GlobalVariable.currentShiftDate = "\(currentMonth) \(indexPath.row - PositionIndex + 1), \(year)"
        
        GlobalVariable.CurrentMonthEditing = (month + 1)
        performSegue(withIdentifier: "toShiftInfoSegue", sender: self)
        highlightdate = indexPath.row
        switch currentMonth {
        case "January":
            selectedJanuaryDates.append(indexPath.row)
        case "February":
            selectedFebruaryDates.append(indexPath.row)
        case "March":
            selectedMarchDates.append(indexPath.row)
        case "April":
            selectedAprilDates.append(indexPath.row)
        case "May":
            selectedMayDates.append(indexPath.row)
        case "June":
            selectedJuneDates.append(indexPath.row)
        case "July":
            selectedJulyDates.append(indexPath.row)
        case "August":
            selectedAugustDates.append(indexPath.row)
        case "September":
            selectedSeptemberDates.append(indexPath.row)
        case "October":
            selectedOctoberDates.append(indexPath.row)
        case "November":
            selectedNovemberDates.append(indexPath.row)
        case "December":
            selectedDecemberDates.append(indexPath.row)
        default:
            break
        }
        JobsFunctions.populateCalendarDates(at: GlobalVariable.jobIndex)
        collectionView.reloadData()
    }
    @IBAction func unwindFromAddVC(_ sender: UIStoryboardSegue){
        if sender.source is shiftInfoTableViewController {
            Calendar.reloadData()
            highlightdate = -1
        }
    }
    
}
