//
//  shiftInfoTableViewController.swift
//  myPay
//
//  Created by Donald Scott on 5/21/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class shiftInfoTableViewController: UITableViewController {

    // MARK: - UIOutlets
    // MARK: UIView
    @IBOutlet weak var contentView1: UIView!
    @IBOutlet weak var contentView2: UIView!
    @IBOutlet weak var contentView3: UIView!
    @IBOutlet weak var contentView4: UIView!
    @IBOutlet weak var contentView5: UIView!
    
    // MARK: UIImageView
    @IBOutlet weak var tipsDownArrow: UIImageView!
    @IBOutlet weak var tipsUpArrow: UIImageView!
    @IBOutlet weak var startTimeDownArrow: UIImageView!
    @IBOutlet weak var startTimeUpArrow: UIImageView!
    @IBOutlet weak var endTimeDownArrow: UIImageView!
    @IBOutlet weak var endTimeUpArrow: UIImageView!
    @IBOutlet weak var breakDownArrow: UIImageView!
    @IBOutlet weak var breakUpArrow: UIImageView!
    @IBOutlet weak var shiftNotesDownArrow: UIImageView!
    @IBOutlet weak var shiftNotesUpArrow: UIImageView!
    
    // MARK: UILabel
    @IBOutlet weak var shiftDateLabel: UILabel!
    @IBOutlet weak var shiftTotalLabel: UILabel!
    @IBOutlet weak var shiftTipTotalLabel: UILabel!
    @IBOutlet weak var shiftPositionLabel: UILabel!
    @IBOutlet weak var shiftHourlyRateLabel: UILabel!
    @IBOutlet weak var shiftStartTimeLabel: UILabel!
    @IBOutlet weak var shiftEndTimeLabel: UILabel!
    @IBOutlet weak var shiftBreakTimeLabel: UILabel!
    @IBOutlet weak var shiftHoursLabel: UILabel!
    @IBOutlet weak var shiftTotalHourlyWageLabel: UILabel!
    @IBOutlet weak var shiftNotesLabel: UILabel!
    @IBOutlet weak var tipsButtonLabel: UILabel!
    @IBOutlet weak var shiftNotesButtonLabel: UILabel!
    
    // MARK: UITableView
    @IBOutlet var shiftTabelView: UITableView!
    
    // MARK: UIButton
    @IBOutlet weak var addTipsBtn: UIButton!
    @IBOutlet weak var clockInBtn: UIButton!
    @IBOutlet weak var clockOutBtn: UIButton!
    @IBOutlet weak var addShiftNotesBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    // MARK: UIDatePicker
    @IBOutlet weak var shiftStartTimePicker: UIDatePicker!
    @IBOutlet weak var shiftEndTimePicker: UIDatePicker!
    
    // MARK: IndexPath
    let contentView1CellIndexPath = IndexPath(row: 4, section: 0)
    let contentView2CellIndexPath = IndexPath(row: 10, section: 0)
    let contentView3CellIndexPath = IndexPath(row: 12, section: 0)
    let contentView4CellIndexPath = IndexPath(row: 14, section: 0)
    let contentView5CellIndexPath = IndexPath(row: 19, section: 0)
    
    // MARK: - Vars
    // Mark: String
    var thisShiftNotes = "No Notes"
    var thisShiftChargeTip: String?
    var thisShiftsDate = GlobalVariable.currentShiftDate
    
    
    // MARK: UUID
    var jobID = GlobalVariable.UniqueID
    var payPeriodID = GlobalVariable.UniqueID
    var jobModel: JobsModel?
    
    // MARK: Int
    var addToPayPeriod = 1
    var jobIndex = GlobalVariable.jobIndex
    
    // MARK: DateFormatter
    let dateFormatter = DateFormatter()
    
    // MARK: Date
    let date = GlobalVariable.StartOfPayPeriod
    
    
    // MARK: Bool
    var isContentView1Shown: Bool = false {
        didSet{
            contentView1.isHidden = !isContentView1Shown
        }
    }
    var isContentView2Shown: Bool = false {
        didSet{
            contentView2.isHidden = !isContentView2Shown
        }
    }
    var isContentView3Shown: Bool = false {
        didSet{
            contentView3.isHidden = !isContentView3Shown
        }
    }
    var isContentView4Shown: Bool = false {
        didSet{
            contentView4.isHidden = !isContentView4Shown
        }
    }
    var isContentView5Shown: Bool = false {
        didSet{
            contentView5.isHidden = !isContentView5Shown
        }
    }
    
    // MARK: - Load Views
    override func viewDidLoad() {
        super.viewDidLoad()
        shiftDateLabel.text = GlobalVariable.currentShiftDate
        clockInBtn.alpha = 0.60
        clockOutBtn.alpha = 0.60
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (contentView1CellIndexPath.section, contentView1CellIndexPath.row):
            if isContentView1Shown {
                return 44
            } else {
                return 0
            }
        case (contentView2CellIndexPath.section, contentView2CellIndexPath.row):
            if isContentView2Shown {
                return 199
            } else {
                return 0
            }
        case (contentView3CellIndexPath.section, contentView3CellIndexPath.row):
            if isContentView3Shown {
                return 199
            } else {
                return 0
            }
        case (contentView4CellIndexPath.section, contentView4CellIndexPath.row):
            if isContentView4Shown {
                return 44
            } else {
                return 0
            }
        case (contentView5CellIndexPath.section, contentView5CellIndexPath.row):
            if isContentView5Shown {
                return 44
            } else {
                return 0
            }
        default:
            if indexPath.row == 2 {
                return 54
            }
            if indexPath.row == 5 {
                return 54
            }
            if indexPath.row == 8 {
                return 54
            }
            if indexPath.row == 17 {
                return 54
            }
            if indexPath.row == 20 {
                return 54
            }
        }
        return 44
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == contentView1CellIndexPath.section &&
            indexPath.row == contentView1CellIndexPath.row - 1 {
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            if isContentView1Shown == true {
                isContentView1Shown = false
                resetArrows()
                tipsDownArrow.isHidden = false
            } else {
                isContentView1Shown = true
                resetArrows()
                tipsUpArrow.isHidden = false
                tipsDownArrow.isHidden = true
                if GlobalVariable.isTipsEditing == true {
                    tipsButtonLabel.text = "Edit Tips"
                } else {
                    tipsButtonLabel.text = "Add Tips"
                }
            }
        }
        if indexPath.section == contentView2CellIndexPath.section &&
            indexPath.row == contentView2CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            if isContentView2Shown == true {
                isContentView2Shown = false
                resetArrows()
                startTimeDownArrow.isHidden = false
            } else {
                isContentView2Shown = true
                resetArrows()
                startTimeUpArrow.isHidden = false
                startTimeDownArrow.isHidden = true
                shiftStartTimeLabel.text = DateFormatter.localizedString(from: shiftStartTimePicker.date, dateStyle: .none, timeStyle: .short)
                if let clockInTime = shiftStartTimeLabel.text {
                    GlobalVariable.thisShiftClockIn = clockInTime
                    clockIn()
                }
            }
            
        }
        if indexPath.section == contentView3CellIndexPath.section &&
            indexPath.row == contentView3CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView4Shown = false
            isContentView5Shown = false
            if isContentView3Shown == true {
                isContentView3Shown = false
                resetArrows()
                endTimeDownArrow.isHidden = false
            } else {
                isContentView3Shown = true
                resetArrows()
                endTimeUpArrow.isHidden = false
                endTimeDownArrow.isHidden = true
            }
        }
        if indexPath.section == contentView4CellIndexPath.section &&
            indexPath.row == contentView4CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView5Shown = false
            if isContentView4Shown == true {
                isContentView4Shown = false
                resetArrows()
                breakDownArrow.isHidden = false
            } else {
                isContentView4Shown = true
                resetArrows()
                breakUpArrow.isHidden = false
                breakDownArrow.isHidden = true
            }
        }
        if indexPath.section == contentView5CellIndexPath.section &&
            indexPath.row == contentView5CellIndexPath.row - 1 {
            isContentView1Shown = false
            isContentView2Shown = false
            isContentView3Shown = false
            isContentView4Shown = false
            if isContentView5Shown == true {
                isContentView5Shown = false
                resetArrows()
                shiftNotesDownArrow.isHidden = false
            } else {
                isContentView5Shown = true
                resetArrows()
                if GlobalVariable.isShiftNotesEditing == true {
                    shiftNotesButtonLabel.text = "view Shift Notes"
                } else {
                    shiftNotesButtonLabel.text = "Add Shift Notes"
                }
                shiftNotesUpArrow.isHidden = false
                shiftNotesDownArrow.isHidden = true
            }
        }
        if indexPath.section == 0 &&
            indexPath.row == 6 {
            selectPayPeriod()
        }
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    
    // MARK: - func
    
    func resetArrows() {
        tipsUpArrow.isHidden = true
        startTimeUpArrow.isHidden = true
        endTimeUpArrow.isHidden = true
        breakUpArrow.isHidden = true
        shiftNotesUpArrow.isHidden = true
    }
    func calculateTotalOfShift() {
        let total = shiftTipTotalLabel.text ?? "$000.00"
        let wage = shiftTotalHourlyWageLabel.text ?? "$000.00"
        let shiftTotal = convertCurrencyToDouble(input: total)
        let shiftWage = convertCurrencyToDouble(input: wage)
        let x = calculateShiftTotal(tipTotal: shiftTotal!, wageTotal: shiftWage!)
        shiftTotalLabel.text = convertDoubleToCurrency(amount: x)
        thisShiftTotal = convertDoubleToCurrency(amount: x)
    }
    func calculateWage(hourlyRate: Double, hours: Double) -> Double {
        return (hourlyRate * hours)
    }
    func calculateShiftTotal(tipTotal: Double, wageTotal: Double) -> Double {
        return (tipTotal + wageTotal)
    }
    func calculateTotalWage() {
        let hours = (shiftHoursLabel.text! as NSString).doubleValue
        let rate = convertCurrencyToDouble(input: shiftHourlyRateLabel.text!)
        let x = calculateWage(hourlyRate: rate!, hours: hours)
        shiftTotalHourlyWageLabel.text = convertDoubleToCurrency(amount: x)
        GlobalVariable.thisShiftsHourlyWage = convertDoubleToCurrency(amount: x)
    }
    func closeAll(view: Int) {
        isContentView1Shown = false
        isContentView2Shown = false
        isContentView3Shown = false
        isContentView4Shown = false
        isContentView5Shown = false
        resetArrows()
        tableView.beginUpdates()
        tableView.endUpdates()
        switch view {
        case 1:
            tipsDownArrow.isHidden = false
        case 2:
            startTimeDownArrow.isHidden = false
        case 3:
            endTimeDownArrow.isHidden = false
        case 4:
            breakDownArrow.isHidden = false
        default:
            break
        }
    }
    func selectPayPeriod() {
        if GlobalVariable.jobPositionArrayIsPopulated == false {
            performSegue(withIdentifier: "toAddPositionSegue", sender: AnyClass.self)
        } else {
            performSegue(withIdentifier: "toSelectPositionSegue", sender: AnyClass.self)
        }
        print("jobPositionArrayIsPopulated", GlobalVariable.jobPositionArrayIsPopulated)
    }
    func clockIn() {
        let clockInTime = GlobalVariable.thisShiftClockIn
        GlobalVariable.formatter.dateFormat = "h:mma"
        
        let date1 = GlobalVariable.formatter.date(from: clockInTime!)
        GlobalVariable.clockInTime = date1!
        clockInBtn.isEnabled = true
        clockInBtn.alpha = 1
    }
    func clockOut() {
        let clockOutTime = GlobalVariable.thisShiftClockOut
        GlobalVariable.formatter.dateFormat = "h:mma"
        
        let date2 = GlobalVariable.formatter.date(from: clockOutTime!)
        GlobalVariable.newEndTime = date2!
        clockOutBtn.isEnabled = true
        clockOutBtn.alpha = 1
    }
    func timeClock() {
        if let startString = shiftStartTimeLabel.text, let endString = shiftEndTimeLabel.text {
            if startString != "" && endString != ""
            {
                updateTimeView(endTime: GlobalVariable.newEndTime)
            } else if startString == "" {
//                errorWarningMessage(warningMessIndex: 1)
            }
        }
    }
    func updateTimeView(endTime: Date) {
        
        let elapsedTime = endTime.timeIntervalSince(GlobalVariable.clockInTime)
        let hours = floor(elapsedTime / 60 / 60)
        let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)
        if hours < 0 {
            let hour = 24 + hours
            shiftHoursLabel.text = ("\(Int(hour)).\(Int(minutes))")
            thisShiftHours = ("\(Int(hour)).\(Int(minutes))")
        } else {
            shiftHoursLabel.text = ("\(Int(hours)).\(Int(minutes))")
            thisShiftHours = ("\(Int(hours)).\(Int(minutes))")
        }
//        GlobalVariable.job1ShiftTipTotal = jb1TotalShiftHoursWorked.text!
        calculateTotalWage()
        calculateTotalOfShift()
        isSaveBtnActive = true
        activateSaveBtn()
    }
    func insertBreakMin(bMin: Int) {
        
        if let startString = shiftStartTimeLabel.text, let endString = shiftEndTimeLabel.text {
            if startString != "" && endString != "" {
                switch bMin {
                case 0:
                    shiftBreakTimeLabel.text = "No Break"
                    let date = GlobalVariable.newEndTime.addingTimeInterval(TimeInterval(-0.0 * 60.0))
                    GlobalVariable.revisedEndTime = date
                case 15:
                    shiftBreakTimeLabel.text = "00:15"
                    let date = GlobalVariable.newEndTime.addingTimeInterval(TimeInterval(-15.0 * 60.0))
                    GlobalVariable.revisedEndTime = date
                case 30:
                    shiftBreakTimeLabel.text = "00:30"
                    let date = GlobalVariable.newEndTime.addingTimeInterval(TimeInterval(-30.0 * 60.0))
                    GlobalVariable.revisedEndTime = date
                case 45:
                    shiftBreakTimeLabel.text = "00.45"
                    let date = GlobalVariable.newEndTime.addingTimeInterval(TimeInterval(-45.0 * 60.0))
                    GlobalVariable.revisedEndTime = date
                case 60:
                    shiftBreakTimeLabel.text = "1:00"
                    let date = GlobalVariable.newEndTime.addingTimeInterval(TimeInterval(-60.0 * 60.0))
                    GlobalVariable.revisedEndTime = date
                default:
                    return ()
                }
                updateTimeView(endTime: GlobalVariable.revisedEndTime)
            }else {
                
//                errorWarningMessage(warningMessIndex: 2)
                
            }
        }
    }
    
    func activateSaveBtn() {
        if isSaveBtnActive == true {
            saveBtn.alpha = 1
            saveBtn.isEnabled = true
        }
    }
    
    func cancelShiftInfo() {
        switch GlobalVariable.CurrentMonthEditing {
        case 1:
            selectedJanuaryDates.removeLast()
        case 2:
            selectedFebruaryDates.removeLast()
        case 3:
            selectedMarchDates.removeLast()
        case 4:
            selectedAprilDates.removeLast()
        case 5:
            selectedMayDates.removeLast()
        case 6:
            selectedJuneDates.removeLast()
        case 7:
            selectedJulyDates.removeLast()
        case 8:
            selectedAugustDates.removeLast()
        case 9:
            selectedSeptemberDates.removeLast()
        case 10:
            selectedOctoberDates.removeLast()
        case 11:
            selectedNovemberDates.removeLast()
        case 12:
            selectedDecemberDates.removeLast()
        default:
            break
        }
        isSaveBtnActive = false
        thisShiftTotal = "$000.00"
        GlobalVariable.thisShiftChargeTip = "$000.00"
        GlobalVariable.thisShiftCashTip = "$000.00"
        GlobalVariable.thisShiftTipOut = "$000.00"
        GlobalVariable.isTipsEditing = false
        thisShiftsTipTotal = "$000.00"
        thisShiftRate = "$000.00"
        thisShiftHours = "00:00"
        GlobalVariable.thisShiftsHourlyWage = "$000.00"
        clockInTime = "00:00"
        clockOutTime = "00:00"
        GlobalVariable.thisShiftNotes = "No Notes Added"
        
    }
    
    
    
    // MARK: -  IBAction
    // MARK: Button
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        cancelShiftInfo()
    }
    
    @IBAction func tipTotalBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "toAddTipsSegue", sender: AnyClass.self)
    }
    @IBAction func clockInBtnPressed(_ sender: UIButton) {
        timeClock()
        isContentView2Shown = false
        view.endEditing(true)
        isContentView3Shown = true
        tableView.beginUpdates()
        tableView.endUpdates()
        if GlobalVariable.jobPositionArrayIsPopulated == false {
            performSegue(withIdentifier: "toAddPositionSegue", sender: Any?.self)
        }
    }
    @IBAction func saveBtnPressed(_ sender: UIButton) {
        GlobalVariable.isShiftNotesEditing = false
        updateJob()
        populatePayPeriod(Duration: GlobalVariable.PayPeriodLength)
    }
    @IBAction func addShiftNotesBtnPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "addShiftNotesSegue", sender: Any?.self)
    }
    @IBAction func clockOutBtnPressed(_ sender: UIButton) {
        timeClock()
        closeAll(view: 2)
        closeAll(view: 3)
    }
    @IBAction func break00MinBtnPressed(_ sender: UIButton) {
        insertBreakMin(bMin: 0)
        closeAll(view: 4)
    }
    @IBAction func break15MinPressed(_ sender: UIButton) {
        insertBreakMin(bMin: 15)
        closeAll(view: 4)
    }
    @IBAction func break30MinBtnPressed(_ sender: UIButton) {
        
        insertBreakMin(bMin: 30)
        closeAll(view: 4)
    }
    @IBAction func break45MinBtnPressed(_ sender: UIButton) {
        
        insertBreakMin(bMin: 45)
        closeAll(view: 4)
    }
    @IBAction func break60MinBtnPressed(_ sender: UIButton) {
        
        insertBreakMin(bMin: 60)
        closeAll(view: 4)
    }
    
    // MARK: DatePicker
    @IBAction func startTimePickerValueChanged(_ sender: UIDatePicker) {
        
        shiftStartTimeLabel.text = DateFormatter.localizedString(from: shiftStartTimePicker.date, dateStyle: .none, timeStyle: .short)
        if let clockInTime = shiftStartTimeLabel.text {
            GlobalVariable.thisShiftClockIn = clockInTime
            clockIn()
        }
    }
    @IBAction func endTimePickerValueChanged(_ sender: UIDatePicker) {
        shiftEndTimeLabel.text = DateFormatter.localizedString(from: shiftEndTimePicker.date, dateStyle: .none, timeStyle: .short)
        if let clockOutTime = shiftEndTimeLabel.text {
            GlobalVariable.thisShiftClockOut = clockOutTime
            clockOut()
        }
        
    }
    
    
     // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddPositionSegue" {
            if let addPositionVC = segue.destination as? addPositionsViewController {
                addPositionVC.setBtnTitle = 1
                addPositionVC.doneSaving = { [weak self] in
                    guard let self = self else { return }
                    self.shiftPositionLabel.text = addPositionVC.shiftPosition
                    self.shiftHourlyRateLabel.text = addPositionVC.shiftRate
                }
            }
        }
    }
    @IBAction func unwindFromAddVC(_ sender: UIStoryboardSegue) {
        if sender.source is shiftTipsTableViewController {
            if let senderVC = sender.source as? shiftTipsTableViewController {
                shiftTipTotalLabel.text = senderVC.thisShiftsTipTotal
                thisShiftsTipTotal = senderVC.thisShiftsTipTotal
                closeAll(view: 1)
                calculateTotalWage()
                calculateTotalOfShift()
                activateSaveBtn()
            }
        }
        
        if sender.source is changePositionViewController {
            if let senderVC = sender.source as? changePositionViewController {
                shiftPositionLabel.text = senderVC.selectedPosition
                shiftHourlyRateLabel.text = senderVC.selectedRate
                print("Position Added is", GlobalVariable.jobPositionArrayIsPopulated)
                calculateTotalWage()
                calculateTotalOfShift()
                closeAll(view: 0)
                
            }
        }
        
        if sender.source is shiftNotesTableViewController {
            if let senderVC = sender.source as? shiftNotesTableViewController {
                shiftNotesLabel.text = senderVC.shiftNotesStatus
                let notes = senderVC.shiftNotes
                if let shiftNotes = notes {
                    thisShiftNotes = shiftNotes
                } else {
                    thisShiftNotes = "No Notes Added This Shift"
                }
                closeAll(view: 5)
                updateJob()       
            }
        }
    }
}
