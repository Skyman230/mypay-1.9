//
//  CalculateStateIncomeTaxes.swift
//  myPay
//
//  Created by Donald Scott on 5/20/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation


struct SITaxEntry {
    var state: String
    var grossWage: Double
    var stateFilingStatus: Int
}

class CalculateStateIncomeTax {
    static func SITaxEntry(entry: SITaxEntry) -> String {
        switch entry.state {
        case "AL":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 500 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 6000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "AK":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "AZ":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0259)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10602.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0288)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 26501.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0336)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 53000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0424)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 158996.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0454)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0259)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 21202.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0288)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 53000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0336)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 105998.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0424)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 317990.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0454)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "AR":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.009)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 4299.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.025)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8499.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.035)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 12699.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 21199.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 35099.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.069)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.009)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 4299.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.025)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8499.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.035)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 12699.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 21199.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 35099.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.069)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "CA":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.01)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8223.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 19459.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 30769 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 42711.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.08)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 53980.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.093)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 275738.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0103)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 330884.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0113)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 551473.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0123)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0133)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.01)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 16446.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 38990.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 61538.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 85422.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.08)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 107960.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.093)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 551476.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0103)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 661768.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0113)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0123)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1074996.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0133)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "CO":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "CT":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 50000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.055)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 100000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 200000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.065)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 250000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.069)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 500000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0699)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 20000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 100000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.055)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 200000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 400000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.065)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 500000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.069)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0699)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "DE":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 2000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.022)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.039)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.048)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 20000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.052)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 25000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0555)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 60000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.066)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 2000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.022)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.039)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.048)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 20000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.052)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 25000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0555)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 60000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.066)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "DC":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 40000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.065)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 60000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.085)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 350000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0875)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0895)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 40000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.065)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 60000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.085)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 350000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0875)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0895)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "FL":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "GA":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.01)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 750.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2250.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3750.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5250.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 7000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.01)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 7000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "HI":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.014)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2400.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.032)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 4800.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.055)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 9600.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.064)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 14400.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.068)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 19200.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.072)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 24000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.076)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 36000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.079)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 48000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0825)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 150000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.09)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 175000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.10)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 200000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.11)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.014)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 4800.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.032)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 9600.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.055)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 19200.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.064)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 28800.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.068)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 38400.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.072)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 48000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.076)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 72000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.079)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 96000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0825)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 300000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.09)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 350000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.10)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 400000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.11)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "ID":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0112)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1472.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0312)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2945.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0362)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 4417.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0462)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5890.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0562)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 7362.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0662)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 11043.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0692)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0112)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2944.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0312)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5890.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0362)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8834.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0462)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 11780.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0562)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 14724.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0662)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 22086.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0692)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "IL":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "IN":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = 000.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "IA":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0036)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1598.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0072)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3196.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0243)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 6392.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 14328.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0612)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 23970.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0648)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 31960.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.068)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 47940.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0792)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 71910.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0898)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0036)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1598.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0072)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3196.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0243)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 6392.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 14328.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0612)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 23970.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0648)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 31960.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.068)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 47940.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0792)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 71910.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0898)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "KS":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 2500.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.031)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 15000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0525)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 30000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.057)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 5000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.031)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 30000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0525)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 60000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.057)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "KY":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "LA":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 12500.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 50000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 25000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 100000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "ME":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.058)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 21450.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0675)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 50750.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0715)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.058)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 42900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0675)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 101550.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0715)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "MD":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0475)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 100000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 125000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0525)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 150000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.055)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 250000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0575)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0475)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 150000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 175000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0525)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 225000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.055)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 300000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0575)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "MA":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "MI":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0425)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0425)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "MN":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0535)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 25890.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0705)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 85060.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0785)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 160020.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0985)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0535)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 37850.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0705)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 150380.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0785)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 266700.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0985)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "MS":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 1000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 1000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "MO":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 102.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.015)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1028.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2056.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.025)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3084.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 4113.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.035)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5141.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 6169.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 7197.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8225.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.055)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 9253.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.059)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 102.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.015)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1028.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2056.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.025)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3084.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 4113.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.035)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5141.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 6169.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 7197.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8225.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.055)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 9253.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.059)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "MT":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.01)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5200.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10800.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 13900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 17900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.069)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.01)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5200.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10800.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 13900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 17900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.069)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "NE":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0246)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3150.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0351)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 18880.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0501)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 30420.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0684)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0246)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 6290.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0351)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 37760.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0501)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 60480.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0684)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "NV":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "NH":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "NJ":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.014)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 20000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0175)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 35000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.035)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 40000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0553)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 75000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0637)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 500000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0897)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.014)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 20000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0175)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 50000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0245)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 70000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.035)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 80000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0553)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 150000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0637)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 500000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0897)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "NM":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.017)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5500.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.032)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 11000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.047)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 16000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.049)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.017)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.032)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 16000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.047)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 24000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.049)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "NY":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8500.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 11700.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.035)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 13900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.059)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 21400.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0633)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 80650.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0657)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2154000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0685)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1077550 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0882)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 17150.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 23600.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.035)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 27900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.059)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 43000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0633)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 161550.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0657)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 323200.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0685)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2155350 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0882)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "NC":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "ND":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.011)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 38700.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0204)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 93700.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0227)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 195450.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0264)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 424950.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.029)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.011)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 64650.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0204)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 156150.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0227)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 237950.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0264)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 424950.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.029)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "OH":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 10850.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0198)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 16300.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0248)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 21750.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0297)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 43459.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0346)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 86900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0396)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 108700.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.046)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 217400.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 10650.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0198)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 16000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0248)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 21350.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0297)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 42650.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0346)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 85300.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0396)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 106650.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.046)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 213350.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "OK":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 1000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.01)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2500.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3750.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 4900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 7200.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.01)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 7500.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 9800.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 12200.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "OR":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3450.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.07)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8700.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.09)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 125000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.099)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 6900.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.07)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 17400.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.09)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 250000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.099)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "PA":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0307)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0307)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "RI":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0375)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 62550.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0475)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 149150.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0599)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0375)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 62550.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0475)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 149150.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0599)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "SC":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2970.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5940.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8910.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 11880.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 14860.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.07)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 2970.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5940.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 8910.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 11880.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 14860.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.07)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "SD":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "TN":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "TX":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "UT":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "VT":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0335)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 38700.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.066)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 93700.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.076)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 195450.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0875)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0335)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 64600.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.066)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 156150.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.076)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 237950.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0875)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "VA":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 17000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0575)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.02)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 3000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 5000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.05)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 17000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0575)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "WA":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "WV":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 25000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 40000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 60000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.065)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 0.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.03)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 10000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 25000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.045)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 40000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.06)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 60000.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.065)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "WI":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 11230.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0584)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 22470.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0627)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 252150.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0765)
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.04)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 14980.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0584)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 29960.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0627)
                    return "\(stateTaxesWithheld)"
                    
                } else if entry.grossWage >= 336200.00 {
                    
                    let stateTaxesWithheld = rounded(entry.grossWage * 0.0765)
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "WY":
            switch entry.stateFilingStatus {
            case 1:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            case 2:
                if entry.grossWage >= 00.00 {
                    
                    let stateTaxesWithheld = 00.00
                    return "\(stateTaxesWithheld)"
                    
                }
            default:
                break
            }
        case "PR":
            print("Pyerto Rico")
        default:
            break
        }
        return ""
    }
    static func rounded(_ number: Double) -> Double {
        return round(100 * number) / 100
    }
}
