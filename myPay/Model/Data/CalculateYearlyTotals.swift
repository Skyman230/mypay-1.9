//
//  CalculateMonthlyTotals.swift
//  myPay
//
//  Created by Donald Scott on 5/28/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation
import UIKit


class CalculateYearsTotals {
     static func showTotals() {
        if JanuaryArray.count > 0 {
            let grossTipArray = JanuaryArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.JanuaryTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = JanuaryArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.JanuaryWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.JanuaryTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if FebruaryArray.count > 0 {
            let grossTipArray = FebruaryArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.FebruaryTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = FebruaryArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.FebruaryWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.FebruaryTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if MarchArray.count > 0 {
            let grossTipArray = MarchArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.MarchTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = MarchArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.MarchWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.MarchTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if AprilArray.count > 0 {
            let grossTipArray = AprilArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.AprilTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = AprilArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.AprilWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.AprilTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if MayArray.count > 0 {
            let grossTipArray = MayArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.MayTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = MayArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.MayWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.MayTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if JuneArray.count > 0 {
            let grossTipArray = JuneArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.JuneTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = JuneArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.JuneWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.JuneTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if JulyArray.count > 0 {
            let grossTipArray = JulyArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.JulyTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = JulyArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.JulyWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.JulyTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if AugustArray.count > 0 {
            let grossTipArray = AugustArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.AugustTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = AugustArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.AugustWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.AugustTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if SeptemberArray.count > 0 {
            let grossTipArray = SeptemberArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.SeptemberTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = SeptemberArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.SeptemberWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.SeptemberTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if OctoberArray.count > 0 {
            let grossTipArray = OctoberArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.OctoberTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = OctoberArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.OctoberWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.OctoberTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if NovemberArray.count > 0 {
            let grossTipArray = NovemberArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.NovemberTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = NovemberArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.NovemberWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.NovemberTotal = convertDoubleToCurrency(amount: totalSum)
        }
        if DecemberArray.count > 0 {
            let grossTipArray = DecemberArray.map {$0.tipTotal}
            let convertedTotaltips = grossTipArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedTotalTips = convertedTotaltips.compactMap{$0}
            let totalTipSum = mappedTotalTips.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.DecemberTips = convertDoubleToCurrency(amount: totalTipSum)
            let grossWageArray = DecemberArray.map {$0.totalHourlyWage}
            let convertedWage = grossWageArray.map{convertCurrencyToDouble(input: $0!)}
            let mappedWage = convertedWage.compactMap{$0}
            let totalWageSum = mappedWage.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.DecemberWages = convertDoubleToCurrency(amount: totalWageSum)
            let totalSum = totalTipSum + totalWageSum
            GlobalVariable.DecemberTotal = convertDoubleToCurrency(amount: totalSum)
        }
        let yearlyGrossTotal = [GlobalVariable.JanuaryTotal, GlobalVariable.FebruaryTotal, GlobalVariable.MarchTotal, GlobalVariable.AprilTotal, GlobalVariable.MayTotal, GlobalVariable.JuneTotal, GlobalVariable.JulyTotal, GlobalVariable.AugustTotal, GlobalVariable.SeptemberTotal, GlobalVariable.OctoberTotal, GlobalVariable.NovemberTotal, GlobalVariable.DecemberTotal]
        let wageTotalArray = yearlyGrossTotal.map{convertCurrencyToDouble(input: $0)}
        let grossTotalArray = wageTotalArray.compactMap {$0}
        let wageTotal = grossTotalArray.reduce(0.0, {$0 + (Double($1) )})
        GlobalVariable.yearlyTotal = convertDoubleToCurrency(amount: wageTotal)
        
        let yearlyGrossTips = [GlobalVariable.JanuaryTips, GlobalVariable.FebruaryTips, GlobalVariable.MarchTips, GlobalVariable.AprilTips, GlobalVariable.MayTips, GlobalVariable.JuneTips, GlobalVariable.JulyTips, GlobalVariable.AugustTips, GlobalVariable.SeptemberTips, GlobalVariable.OctoberTips, GlobalVariable.NovemberTips, GlobalVariable.DecemberTips]
        let wageTipsArray = yearlyGrossTips.map{convertCurrencyToDouble(input: $0)}
        let grossTipsArray = wageTipsArray.compactMap {$0}
        let wageTips = grossTipsArray.reduce(0.0, {$0 + (Double($1) )})
        GlobalVariable.yearlyTips = convertDoubleToCurrency(amount: wageTips)
        
        let yearlyGrossWage = [GlobalVariable.JanuaryWages, GlobalVariable.FebruaryWages, GlobalVariable.MarchWages, GlobalVariable.AprilWages, GlobalVariable.MayWages, GlobalVariable.JuneWages, GlobalVariable.JulyWages, GlobalVariable.AugustWages, GlobalVariable.SeptemberWages, GlobalVariable.OctoberWages, GlobalVariable.NovemberWages, GlobalVariable.DecemberWages]
        let wageSumArray = yearlyGrossWage.map{convertCurrencyToDouble(input: $0)}
        let grossWageArray = wageSumArray.compactMap {$0}
        let wageSum = grossWageArray.reduce(0.0, {$0 + (Double($1) )})
        GlobalVariable.yearlyWage = convertDoubleToCurrency(amount: wageSum)
    }
    static func convertCurrencyToDouble(input: String) -> Double? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale.current
        
        return numberFormatter.number(from: input)?.doubleValue
    }
    static func rounded(_ number: Double) -> Double {
        return round(100 * number) / 100
    }
    static func convertDoubleToCurrency(amount: Double) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale.current
        
        return numberFormatter.string(from: NSNumber(value: amount))!
    }}
