//
//  CalculateSocialSecurity.swift
//  myPay
//
//  Created by Donald Scott on 5/20/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

struct socialSecurityEntry {
    var grossWage: Double
}
class CalculateSocialSecurity {
    static func socialSecurityEntry(entry: socialSecurityEntry) -> String {
        let socialSecurity = rounded(entry.grossWage * 0.062)
        return "\(socialSecurity)"
    }
    static func rounded(_ number: Double) -> Double {
        return round(100 * number) / 100
    }
}
