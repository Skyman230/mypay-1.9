//
//  calculateSums.swift
//  myPay
//
//  Created by Donald Scott on 5/19/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation
import UIKit

enum CalculateError {
    case Error1234
}

class CalculateSums {
    
    static func showFigures() {
        var payPeriodGrossWageSum: Double?
        let jobIndex = GlobalVariable.jobIndex
        
        // MARK: - Overview
        
        if payPeriodGrossTipArray.count > 0 {
            let sumArray = payPeriodGrossTipArray.map{convertCurrencyToDouble(input: $0)}
            let grossTipArray = sumArray.compactMap {$0}
            let Sum = grossTipArray.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.PayPeriodTips = convertDoubleToCurrency(amount: Sum)
            IncomeArray.append(Sum)
            incomeSourceArray.append("Tips")
        }
        if payPeriodGrossWageArray.count > 0 {
            let wageSumArray = payPeriodGrossWageArray.map{convertCurrencyToDouble(input: $0)}
            let grossWageArray = wageSumArray.compactMap {$0}
            let wageSum = grossWageArray.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.PayPeriodWage = convertDoubleToCurrency(amount: wageSum)
            IncomeArray.append(wageSum)
            incomeSourceArray.append("Hourly Wage")
        }
        if payPeriodTotalHoursArray.count > 0 {
            let hoursSum = payPeriodTotalHoursArray.reduce(0.0, {$0 + (Double($1) ?? 00.00 )})
            let hours = String(format: "%.2f", hoursSum)
            GlobalVariable.PayPeriodHours = hours
            
        }
        if thisMonthsGrossTipArray.count > 0 {
            let monthSumArray = thisMonthsGrossTipArray.map{convertCurrencyToDouble(input: $0)}
            let monthGrossTipArray = monthSumArray.compactMap {$0}
            let monthGrossTipSum = monthGrossTipArray.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.MonthTips = convertDoubleToCurrency(amount: monthGrossTipSum)
            GlobalVariable.ArchiveMonthTips = convertDoubleToCurrency(amount: monthGrossTipSum)
        }
        if thisMonthsGrossWageArray.count > 0 {
            let monthWageSumArray = thisMonthsGrossWageArray.map{convertCurrencyToDouble(input: $0)}
            let monthGrossWageArray = monthWageSumArray.compactMap {$0}
            let monthGrossWageSum = monthGrossWageArray.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.MonthWage = convertDoubleToCurrency(amount: monthGrossWageSum)
        }
        if thisMonthsGrossWageArray.count > 0 {
            let mWage = convertCurrencyToDouble(input: GlobalVariable.MonthWage!)
            let mTips = convertCurrencyToDouble(input: GlobalVariable.MonthTips!)
            let monthlyGrossWageSum = Double(mTips! + mWage!)
            GlobalVariable.MonthlyGross = convertDoubleToCurrency(amount: monthlyGrossWageSum)
        }
        if thisMonthsTotalHoursArray.count > 0 {
            let thisMonthsHoursSum = thisMonthsTotalHoursArray.reduce(0.0, {$0 + (Double($1) ?? 00.00 )})
            let thisMonthsHours = String(format: "%.2f", thisMonthsHoursSum)
            GlobalVariable.MonthHour = thisMonthsHours
        }
        let payPeriodTotalGrossWageArray = payPeriodGrossTipArray + payPeriodGrossWageArray
        if payPeriodTotalGrossWageArray.count > 0 {
            let grossTotalWageSumArray = payPeriodTotalGrossWageArray.map{convertCurrencyToDouble(input: $0)}
            let payPeriodGrossWageTotal = grossTotalWageSumArray.compactMap {$0}
            payPeriodGrossWageSum = payPeriodGrossWageTotal.reduce(0.0, {$0 + (Double($1) )})
            GlobalVariable.GrossWage = convertDoubleToCurrency(amount: payPeriodGrossWageSum!)
        }
       
        // MARK: - Federal Income Tax
        if GlobalVariable.allowances > 0 {
            if GlobalVariable.filingStatus > 0 {
                
                var tax: Double
                var withholdings: Double
                var stateStatus = Int()
                let grossWageString = GlobalVariable.GrossWage
                let grossWage = convertCurrencyToDouble(input: grossWageString!)
                let Allowances = GlobalVariable.allowances
                let status = GlobalVariable.filingStatus
                let duration = GlobalVariable.PayPeriodLength
                let residence = GlobalVariable.stateOfResidence
                if status == 1 {
                    stateStatus = 1
                }
                if status == 4 {
                    stateStatus = 1
                }
                if status == 2 {
                    stateStatus = 2
                }
                if status == 3 {
                    stateStatus = 1
                }
                let FITentry = FITaxEntry(grossWage: grossWage!, Allowances: Double(Allowances), filingStatus: status, payrollPeriod: duration )
                let total = CalculateFederalIncomeTax.FITaxEntry(entry: FITentry)
                let myDouble = Double(total)
                if let taxes = myDouble {
                    tax = taxes
                } else {
                    tax = 000.00
                }
                let federalIncomeTax = convertDoubleToCurrency(amount: tax)
                GlobalVariable.federalIncomeTax = federalIncomeTax
                
                let SITentry = SITaxEntry(state: residence!, grossWage: grossWage!, stateFilingStatus: stateStatus)
                let total2 = CalculateStateIncomeTax.SITaxEntry(entry: SITentry)
                let myDouble2 = Double(total2)
                if let amount = myDouble2 {
                    withholdings = amount
                } else {
                    withholdings = 00.00
                }
                let stateIncomeTax = convertDoubleToCurrency(amount: withholdings)
                GlobalVariable.stateIncomeTax = stateIncomeTax
                
                let socSec = (grossWage! * 0.062)
                GlobalVariable.SocialSecurity = convertDoubleToCurrency(amount: socSec)
                
                let mediCare = (grossWage! * 0.0145)
                GlobalVariable.mediCare = convertDoubleToCurrency(amount: mediCare)
                
                if let wage = grossWage {
                    GlobalVariable.netGross = wage
                } else {
                    GlobalVariable.netGross = 000.00
                }
                let totalTaxes = convertCurrencyToDouble(input: GlobalVariable.totalWithholdings ?? "$000.00")
                if let taxes = totalTaxes {
                    GlobalVariable.netWithholdings = taxes
                } else {
                    GlobalVariable.netWithholdings = 000.00
                }
                let totalDeductions = convertCurrencyToDouble(input: GlobalVariable.totalDeductions ?? "$000.00")
                if let myDeductions = totalDeductions {
                    GlobalVariable.netDeductions = myDeductions
                } else {
                    GlobalVariable.netDeductions = 000.00
                }
                let withholdingSum = rounded((GlobalVariable.netGross) - (GlobalVariable.netWithholdings + GlobalVariable.netDeductions))
                GlobalVariable.totalNet = convertDoubleToCurrency(amount: withholdingSum)
                
            }
            
            let totalHours = (GlobalVariable.PayPeriodHours! as NSString).integerValue
            if totalHours > 80 {
                let overtimeHours = (totalHours - 80)
                let overtimeHrs = Double(overtimeHours)
                let payRate = convertCurrencyToDouble(input: GlobalVariable.thisShiftRate!)
                let payRate2 = (payRate!/2)
                let overtimePay1 = (overtimeHrs * payRate!)
                let overtimePay2 = (overtimeHrs * payRate2)
                let x = (overtimePay1 + overtimePay2)
                let overtimePay = convertDoubleToCurrency(amount: x)
                GlobalVariable.Overtime = overtimePay
            } else {
                GlobalVariable.Overtime = "$000.00"
            }
            
            // MARK: - Total Withholdings
            
            if !GlobalVariable.GrossWage!.isEmpty {
                if !GlobalVariable.federalIncomeTax!.isEmpty {
                    let FITString = GlobalVariable.federalIncomeTax!
                    let socSecString = GlobalVariable.SocialSecurity
                    let mediCareString = GlobalVariable.mediCare
                    let SITString = GlobalVariable.stateIncomeTax
                    let FIT = convertCurrencyToDouble(input: FITString)
                    let socSec = convertCurrencyToDouble(input: socSecString!)
                    let mediCare = convertCurrencyToDouble(input: mediCareString!)
                    let SIT = convertCurrencyToDouble(input: SITString!)
                    let entry3 = withholdingEntry(FIT: FIT!, SS: socSec!, medC: mediCare!, SIT: SIT!)
                    let totalWithHoldings = CalculateTotalWithholding.withholdings(entry: entry3)
                    let myDouble3 = Double(totalWithHoldings)
                    let withHoldings = convertDoubleToCurrency(amount: myDouble3!)
                    GlobalVariable.totalWithholdings = withHoldings
                    print("FIT", GlobalVariable.federalIncomeTax!)
                    print("SIT", GlobalVariable.stateIncomeTax!)
                    print("MediCare", GlobalVariable.mediCare!)
                    print("SS", GlobalVariable.SocialSecurity!)
                    
                }
            }
            
            // MARK: show 401k deduction
            if GlobalVariable.retirement401Kadded == true {
                let Percentage = (GlobalVariable.deductionPercentage as NSString).doubleValue
                let deductPercentage = String(format: "%.2f", Percentage/100)
                print(deductPercentage)
                let percent = (deductPercentage as NSString).doubleValue
                let grossWage = GlobalVariable.GrossWage
                let wage = convertCurrencyToDouble(input: grossWage!)
                
                let entry = percentageEntry(percentage: percent, grossWages: wage!)
                let Amount = retirement401KPercentage.percentageEntry(entry: entry)
                GlobalVariable.retirement401KAmount = convertDoubleToCurrency(amount: Amount)
            }
            if GlobalVariable.retirement401KRothadded == true {
                let Percentage = (GlobalVariable.retirement401KRothPercentage as NSString).doubleValue
                let deductPercentage = String(format: "%.2f", Percentage/100)
                print(deductPercentage)
                let percent = (deductPercentage as NSString).doubleValue
                let grossWage = GlobalVariable.GrossWage
                let wage = convertCurrencyToDouble(input: grossWage!)
                let taxes = GlobalVariable.totalWithholdings
                let taxesWithheld = convertCurrencyToDouble(input: taxes!)
                
                let entry = percentage401KRothEntry(grossWages: wage!, taxesWithheld: taxesWithheld!, percent: percent)
                let Sum = retirement401KRothPercentage.percentageRothEntry(entry: entry)
                GlobalVariable.retirement401KRothAmount = convertDoubleToCurrency(amount: Sum)
            }
        }
        
        // MARK: - Settings
        // MARK: Total Deductions
        let retirement = GlobalVariable.totalRetirementSum ?? "$000.00"
        let insurance = GlobalVariable.totalInsuranceSum ?? "$000.00"
        let additionalDeductions = GlobalVariable.totalAdditionalDeductionSum ?? "$000.00"
        let totalDeductionArray = [retirement, insurance, additionalDeductions]
        let sumArray = totalDeductionArray.map{convertCurrencyToDouble(input: $0)}
        let deductionSumArray = sumArray.compactMap {$0}
        let Sum = deductionSumArray.reduce(0.0, {$0 + (Double($1) )})
        GlobalVariable.totalDeductions = convertDoubleToCurrency(amount: Sum)
        
        // MARK: Retirement
        let retirAmount1 = GlobalVariable.retirement401KAmount ?? "$000.00"
        let retirAmount2 = GlobalVariable.retirement401KRothAmount ?? "$000.00"
        let retirementArray = [retirAmount1, retirAmount2]
        let sumRetirementArray = retirementArray.map{convertCurrencyToDouble(input: $0)}
        let retirementSumArray = sumRetirementArray.compactMap {$0}
        let retirementSum = retirementSumArray.reduce(0.0, {$0 + (Double($1) )})
        GlobalVariable.totalRetirementSum = convertDoubleToCurrency(amount: retirementSum)
        
        let roth401kPercentage = GlobalVariable.retirement401KRothPercentage ?? "0.00"
        let percent = (roth401kPercentage as NSString).doubleValue
        let grossWage = GlobalVariable.GrossWage
        let wage = convertCurrencyToDouble(input: grossWage!)
        let taxes = GlobalVariable.totalWithholdings
        let taxesWithheld = convertCurrencyToDouble(input: taxes!)
        
        let entry = percentage401KRothEntry(grossWages: wage!, taxesWithheld: taxesWithheld!, percent: percent)
        let Sum401KRoth = retirement401KRothPercentage.percentageRothEntry(entry: entry)
        GlobalVariable.retirement401KRothAmount = convertDoubleToCurrency(amount: Sum401KRoth)
        
        let ret401kPercentage = GlobalVariable.retirement401KPercentage ?? "0.00"
        let percent401K = (ret401kPercentage as NSString).doubleValue
        let entry401K = percentageEntry(percentage: percent401K, grossWages: wage!)
        let Sum401K = retirement401KPercentage.percentageEntry(entry: entry401K)
        GlobalVariable.retirement401KAmount = convertDoubleToCurrency(amount: Sum401K)
        
        // MARK: Insurance
        let medical = GlobalVariable.medicalInsurance ?? "$000.00"
        let medicalSavings = GlobalVariable.medicalSavingsAccount ?? "$000.00"
        let dental = GlobalVariable.dentalInsurance ?? "$000.00"
        let vision = GlobalVariable.visionInsurance ?? "$000.00"
        let insuranceArray = [medical, medicalSavings, dental, vision]
        let sumInsuranceArray = insuranceArray.map{convertCurrencyToDouble(input: $0)}
        let insuranceSumArray = sumInsuranceArray.compactMap {$0}
        let insuranceSum = insuranceSumArray.reduce(0.0, {$0 + (Double($1) )})
        GlobalVariable.totalInsuranceSum = convertDoubleToCurrency(amount: insuranceSum)
        
        // MARK: Voluntary Deductions
        let additDeductionArray = Data.Jobs[jobIndex].voluntaryDeductions.map {$0.Amount}
        let sumDeductionArray = additDeductionArray.map{convertCurrencyToDouble(input: $0)}
        let AddiDeductSumArray = sumDeductionArray.compactMap {$0}
        let DeductionSum = AddiDeductSumArray.reduce(0.0, {$0 + (Double($1) )})
        GlobalVariable.totalAdditionalDeductionSum = convertDoubleToCurrency(amount: DeductionSum)
        
    }
    static func convertCurrencyToDouble(input: String) -> Double? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale.current
        
        return numberFormatter.number(from: input)?.doubleValue
    }
    static func rounded(_ number: Double) -> Double {
        return round(100 * number) / 100
    }
    static func convertDoubleToCurrency(amount: Double) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale.current
        
        return numberFormatter.string(from: NSNumber(value: amount))!
    }
}
