//
//  CalculateFederalIncomeTaxes.swift
//  myPay
//
//  Created by Donald Scott on 5/20/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

struct FITaxEntry {
    var grossWage: Double
    var Allowances: Double
    var filingStatus: Int
    var payrollPeriod: Int
}
class CalculateFederalIncomeTax {
    static func FITaxEntry(entry: FITaxEntry) -> String {
        let taxableIncome = rounded(entry.grossWage - (79.80 * entry.Allowances))
        print("Taxable Income is ", taxableIncome)
        switch entry.payrollPeriod {
        case 4:
            switch entry.filingStatus {
            case 1:
                if taxableIncome < 0 {
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 0 && taxableIncome < 73 {
                    let taxesWithheld = 000.00
                    
                    return "\(taxesWithheld)"
                }else if taxableIncome > 73 && taxableIncome < 260 {
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 73) * 0.10))
                    
                    return "\(taxesWithheld)"
                } else if taxableIncome > 260 && taxableIncome < 832 {
                    let taxesWithheld = rounded(18.70 + ((taxableIncome - 260) * 0.12))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 832 && taxableIncome < 1692 {
                    let taxesWithheld = rounded(87.34 + ((taxableIncome - 832) * 0.22))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1692 && taxableIncome < 3164 {
                    let taxesWithheld = rounded(276.54 + ((taxableIncome - 1692) * 0.24))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3164 && taxableIncome < 3998 {
                    let taxesWithheld = rounded(629.82 + ((taxableIncome - 3164) * 0.32))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3998 && taxableIncome < 9887 {
                    let taxesWithheld = rounded(896.70 + ((taxableIncome - 3998) * 0.35))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 9887 {
                    let taxesWithheld = rounded(2957.85 + ((taxableIncome - 9887) * 0.37))
                    
                    return "\(taxesWithheld)"
                    
                }
            case 2:
                if taxableIncome < 0 {
                    let taxesWithheld = 000.00
                    
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 0 && taxableIncome < 227 {
                    let taxesWithheld = 000.00
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 227 && taxableIncome < 600 {
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 227) * 0.10))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 600 && taxableIncome < 1745 {
                    let taxesWithheld = rounded(37.30 + ((taxableIncome - 600) * 0.12))
                    
                    return "\(taxesWithheld)"
                } else if taxableIncome > 1745 && taxableIncome < 3465 {
                    let taxesWithheld = rounded(174.70 + ((taxableIncome - 39475) * 0.22))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3465 && taxableIncome < 6409 {
                    let taxesWithheld = rounded(553.10 + ((taxableIncome - 3465) * 0.24))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 6409 && taxableIncome < 8077 {
                    let taxesWithheld = rounded(1259.66 + ((taxableIncome - 6409) * 0.32))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 8077 && taxableIncome < 12003 {
                    let taxesWithheld = rounded(1793.42 + ((taxableIncome - 8077) * 0.35))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 12003 {
                    let taxesWithheld = rounded(3167.52 + ((taxableIncome - 12003) * 0.37))
                    
                    return "\(taxesWithheld)"
                    
                }
            case 3:
                if taxableIncome < 0 {
                    let taxesWithheld = 000.00
                    
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 0 && taxableIncome < 227 {
                    let taxesWithheld = 000.00
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 227 && taxableIncome < 600 {
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 227) * 0.10))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 600 && taxableIncome < 1745 {
                    let taxesWithheld = rounded(37.30 + ((taxableIncome - 600) * 0.12))
                    
                    return "\(taxesWithheld)"
                } else if taxableIncome > 1745 && taxableIncome < 3465 {
                    let taxesWithheld = rounded(174.70 + ((taxableIncome - 39475) * 0.22))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3465 && taxableIncome < 6409 {
                    let taxesWithheld = rounded(553.10 + ((taxableIncome - 3465) * 0.24))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 6409 && taxableIncome < 8077 {
                    let taxesWithheld = rounded(1259.66 + ((taxableIncome - 6409) * 0.32))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 8077 && taxableIncome < 12003 {
                    let taxesWithheld = rounded(1793.42 + ((taxableIncome - 8077) * 0.35))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 12003 {
                    let taxesWithheld = rounded(3167.52 + ((taxableIncome - 12003) * 0.37))
                    
                    return "\(taxesWithheld)"
                    
                }
            case 4:
                if taxableIncome < 0 {
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 0 && taxableIncome < 73 {
                    let taxesWithheld = 000.00
                    
                    return "\(taxesWithheld)"
                }else if taxableIncome > 73 && taxableIncome < 260 {
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 73) * 0.10))
                    
                    return "\(taxesWithheld)"
                } else if taxableIncome > 260 && taxableIncome < 832 {
                    let taxesWithheld = rounded(18.70 + ((taxableIncome - 260) * 0.12))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 832 && taxableIncome < 1692 {
                    let taxesWithheld = rounded(87.34 + ((taxableIncome - 832) * 0.22))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1692 && taxableIncome < 3164 {
                    let taxesWithheld = rounded(276.54 + ((taxableIncome - 1692) * 0.24))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3164 && taxableIncome < 3998 {
                    let taxesWithheld = rounded(629.82 + ((taxableIncome - 3164) * 0.32))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3998 && taxableIncome < 9887 {
                    let taxesWithheld = rounded(896.70 + ((taxableIncome - 3998) * 0.35))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 9887 {
                    let taxesWithheld = rounded(2957.85 + ((taxableIncome - 9887) * 0.37))
                    
                    return "\(taxesWithheld)"
                    
                }
            default: break
            }
        case 3:
            switch entry.filingStatus {
            case 1:
                if taxableIncome < 0 {
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 0 && taxableIncome < 146 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 146 && taxableIncome < 519 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 146) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 519 && taxableIncome < 1664 {
                    let taxesWithheld = rounded(37.30 + ((taxableIncome - 519) * 0.12))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1664 && taxableIncome < 3385 {
                    let taxesWithheld = rounded(174.70 + ((taxableIncome - 1664) * 0.22))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3385 && taxableIncome < 6328 {
                    let taxesWithheld = rounded(553.32 + ((taxableIncome - 3385) * 0.24))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 6328 && taxableIncome < 7996 {
                    let taxesWithheld = rounded(1259.64 + ((taxableIncome - 6328) * 0.32))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 7996 && taxableIncome < 19773 {
                    let taxesWithheld = rounded(1793.40 + ((taxableIncome - 7996) * 0.35))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 19773 {
                    let taxesWithheld = rounded(5915.35 + ((taxableIncome - 19773) * 0.37))
                    
                    return "\(taxesWithheld)"
                    
                }
            case 2:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 0 && taxableIncome < 454 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }   else if taxableIncome > 454 && taxableIncome < 1200 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 454) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1200 && taxableIncome < 3490 {
                    
                    let taxesWithheld = rounded(74.60 + ((taxableIncome - 1200) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3490 && taxableIncome < 6931 {
                    
                    let taxesWithheld = rounded(349.40 + ((taxableIncome - 3490) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 6931 && taxableIncome < 12817 {
                    
                    let taxesWithheld = rounded(1106.42 + ((taxableIncome - 6931) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 12817 && taxableIncome < 16154 {
                    
                    let taxesWithheld = rounded(2519.06 + ((taxableIncome - 12817) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 16154 && taxableIncome < 24006 {
                    
                    let taxesWithheld = rounded(3586.90 + ((taxableIncome - 16154) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 24006 {
                    
                    let taxesWithheld = rounded(6335.10 + ((taxableIncome - 24006) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            case 3:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 0 && taxableIncome < 454 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }   else if taxableIncome > 454 && taxableIncome < 1200 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 454) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1200 && taxableIncome < 3490 {
                    
                    let taxesWithheld = rounded(74.60 + ((taxableIncome - 1200) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3490 && taxableIncome < 6931 {
                    
                    let taxesWithheld = rounded(349.40 + ((taxableIncome - 3490) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 6931 && taxableIncome < 12817 {
                    
                    let taxesWithheld = rounded(1106.42 + ((taxableIncome - 6931) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 12817 && taxableIncome < 16154 {
                    
                    let taxesWithheld = rounded(2519.06 + ((taxableIncome - 12817) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 16154 && taxableIncome < 24006 {
                    
                    let taxesWithheld = rounded(3586.90 + ((taxableIncome - 16154) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 24006 {
                    
                    let taxesWithheld = rounded(6335.10 + ((taxableIncome - 24006) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            case 4:
                if taxableIncome < 0 {
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 0 && taxableIncome < 146 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 146 && taxableIncome < 519 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 146) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 519 && taxableIncome < 1664 {
                    let taxesWithheld = rounded(37.30 + ((taxableIncome - 519) * 0.12))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1664 && taxableIncome < 3385 {
                    let taxesWithheld = rounded(174.70 + ((taxableIncome - 1664) * 0.22))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3385 && taxableIncome < 6328 {
                    let taxesWithheld = rounded(553.32 + ((taxableIncome - 3385) * 0.24))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 6328 && taxableIncome < 7996 {
                    let taxesWithheld = rounded(1259.64 + ((taxableIncome - 6328) * 0.32))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 7996 && taxableIncome < 19773 {
                    let taxesWithheld = rounded(1793.40 + ((taxableIncome - 7996) * 0.35))
                    
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 19773 {
                    let taxesWithheld = rounded(5915.35 + ((taxableIncome - 19773) * 0.37))
                    
                    return "\(taxesWithheld)"
                    
                }
            default: break
            }
        case 2:
            switch entry.filingStatus {
            case 1:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 0 && taxableIncome < 158 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 158 && taxableIncome < 563 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 158) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 563 && taxableIncome < 1803 {
                    
                    let taxesWithheld = rounded(40.50 + ((taxableIncome - 563) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1803 && taxableIncome < 3667 {
                    
                    let taxesWithheld = rounded(189.30 + ((taxableIncome - 1803) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3667 && taxableIncome < 6855 {
                    
                    let taxesWithheld = rounded(599.38 + ((taxableIncome - 3667) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 6855 && taxableIncome < 8663 {
                    
                    let taxesWithheld = rounded(1364.50 + ((taxableIncome - 6855) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 8663 && taxableIncome < 21421 {
                    
                    let taxesWithheld = rounded(1943.06 + ((taxableIncome - 8663) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 21421 {
                    
                    let taxesWithheld = rounded(6408.36 + ((taxableIncome - 21421) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            case 2:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 0 && taxableIncome < 492 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }   else if taxableIncome > 492 && taxableIncome < 1300 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 492) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1300 && taxableIncome < 3781 {
                    
                    let taxesWithheld = rounded(80.80 + ((taxableIncome - 1300) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3781 && taxableIncome < 7508 {
                    
                    let taxesWithheld = rounded(378.52 + ((taxableIncome - 3781) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 7508 && taxableIncome < 13885 {
                    
                    let taxesWithheld = rounded(1198.46 + ((taxableIncome - 7508) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 13885 && taxableIncome < 17500 {
                    
                    let taxesWithheld = rounded(2728.94 + ((taxableIncome - 13885) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 17500 && taxableIncome < 26006 {
                    
                    let taxesWithheld = rounded(3885.74 + ((taxableIncome - 17500) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 26006 {
                    
                    let taxesWithheld = rounded(6862.84 + ((taxableIncome - 26006) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            case 3:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 0 && taxableIncome < 492 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }   else if taxableIncome > 492 && taxableIncome < 1300 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 492) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1300 && taxableIncome < 3781 {
                    
                    let taxesWithheld = rounded(80.80 + ((taxableIncome - 1300) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3781 && taxableIncome < 7508 {
                    
                    let taxesWithheld = rounded(378.52 + ((taxableIncome - 3781) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 7508 && taxableIncome < 13885 {
                    
                    let taxesWithheld = rounded(1198.46 + ((taxableIncome - 7508) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 13885 && taxableIncome < 17500 {
                    
                    let taxesWithheld = rounded(2728.94 + ((taxableIncome - 13885) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 17500 && taxableIncome < 26006 {
                    
                    let taxesWithheld = rounded(3885.74 + ((taxableIncome - 17500) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 26006 {
                    
                    let taxesWithheld = rounded(6862.84 + ((taxableIncome - 26006) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            case 4:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 0 && taxableIncome < 158 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 158 && taxableIncome < 563 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 158) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 563 && taxableIncome < 1803 {
                    
                    let taxesWithheld = rounded(40.50 + ((taxableIncome - 563) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 1803 && taxableIncome < 3667 {
                    
                    let taxesWithheld = rounded(189.30 + ((taxableIncome - 1803) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3667 && taxableIncome < 6855 {
                    
                    let taxesWithheld = rounded(599.38 + ((taxableIncome - 3667) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 6855 && taxableIncome < 8663 {
                    
                    let taxesWithheld = rounded(1364.50 + ((taxableIncome - 6855) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 8663 && taxableIncome < 21421 {
                    
                    let taxesWithheld = rounded(1943.06 + ((taxableIncome - 8663) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 21421 {
                    
                    let taxesWithheld = rounded(6408.36 + ((taxableIncome - 21421) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            default: break
            }
        case 1:
            switch entry.filingStatus {
            case 1:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 0 && taxableIncome < 317 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 317 && taxableIncome < 1125 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 317) * 0.10))
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 1125 && taxableIncome < 3606 {
                    
                    let taxesWithheld = rounded(80.80 + ((taxableIncome - 1125) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3606 && taxableIncome < 7333 {
                    
                    let taxesWithheld = rounded(378.52 + ((taxableIncome - 3606) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 7333 && taxableIncome < 13710 {
                    
                    let taxesWithheld = rounded(1198.46 + ((taxableIncome - 7333) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 13710 && taxableIncome < 17325 {
                    
                    let taxesWithheld = rounded(2728.94 + ((taxableIncome - 13710) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 17325 && taxableIncome < 42842 {
                    
                    let taxesWithheld = rounded(3885.74 + ((taxableIncome - 17325) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 42842 {
                    
                    let taxesWithheld = rounded(12816.69 + ((taxableIncome - 42842) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            case 2:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 0 && taxableIncome < 983 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 983 && taxableIncome < 2600 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 983) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 2600 && taxableIncome < 7563 {
                    
                    let taxesWithheld = rounded(161.70 + ((taxableIncome - 2600) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 7563 && taxableIncome < 15017 {
                    
                    let taxesWithheld = rounded(757.26 + ((taxableIncome - 7563) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 15017 && taxableIncome < 27771 {
                    
                    let taxesWithheld = rounded(2397.14 + ((taxableIncome - 15017) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 27771 && taxableIncome < 35000 {
                    
                    let taxesWithheld = rounded(5458.10 + ((taxableIncome - 27771) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 35000 && taxableIncome < 52013 {
                    
                    let taxesWithheld = rounded(7771.38 + ((taxableIncome - 35000) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 52013 {
                    
                    let taxesWithheld = rounded(13725.93 + ((taxableIncome - 52013) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            case 3:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 0 && taxableIncome < 983 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 983 && taxableIncome < 2600 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 983) * 0.10))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 2600 && taxableIncome < 7563 {
                    
                    let taxesWithheld = rounded(161.70 + ((taxableIncome - 2600) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 7563 && taxableIncome < 15017 {
                    
                    let taxesWithheld = rounded(757.26 + ((taxableIncome - 7563) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 15017 && taxableIncome < 27771 {
                    
                    let taxesWithheld = rounded(2397.14 + ((taxableIncome - 15017) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 27771 && taxableIncome < 35000 {
                    
                    let taxesWithheld = rounded(5458.10 + ((taxableIncome - 27771) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 35000 && taxableIncome < 52013 {
                    
                    let taxesWithheld = rounded(7771.38 + ((taxableIncome - 35000) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 52013 {
                    
                    let taxesWithheld = rounded(13725.93 + ((taxableIncome - 52013) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            case 4:
                if taxableIncome < 0 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 0 && taxableIncome < 317 {
                    
                    let taxesWithheld = 000.00
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 317 && taxableIncome < 1125 {
                    
                    let taxesWithheld = rounded(0.00 + ((taxableIncome - 317) * 0.10))
                    return "\(taxesWithheld)"
                    
                }  else if taxableIncome > 1125 && taxableIncome < 3606 {
                    
                    let taxesWithheld = rounded(80.80 + ((taxableIncome - 1125) * 0.12))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 3606 && taxableIncome < 7333 {
                    
                    let taxesWithheld = rounded(378.52 + ((taxableIncome - 3606) * 0.22))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 7333 && taxableIncome < 13710 {
                    
                    let taxesWithheld = rounded(1198.46 + ((taxableIncome - 7333) * 0.24))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 13710 && taxableIncome < 17325 {
                    
                    let taxesWithheld = rounded(2728.94 + ((taxableIncome - 13710) * 0.32))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 17325 && taxableIncome < 42842 {
                    
                    let taxesWithheld = rounded(3885.74 + ((taxableIncome - 17325) * 0.35))
                    return "\(taxesWithheld)"
                    
                } else if taxableIncome > 42842 {
                    
                    let taxesWithheld = rounded(12816.69 + ((taxableIncome - 42842) * 0.37))
                    return "\(taxesWithheld)"
                    
                }
            default: break
            }
        default:
            break
        }
        return ""
    }
    static func rounded(_ number: Double) -> Double {
        return round(100 * number) / 100
    }
    
}
