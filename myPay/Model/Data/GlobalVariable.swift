//
//  GlobalVariable.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

struct GlobalVariable {
    
    // MARK: - Intro
    static var playInfo = true
    static var currentJobEditing: String?
    static var currentShiftDate: String?
    
    
    static var formatter = DateFormatter()

    //MARK: Int
    static var PayPeriodLength = 0
    static var jobIndex = 0
    static var popupWindowIndex = 0
    
    // MARK: Bool
    static var jobsArrayPopulated = false
    static var shiftTipsAdded = false
    static var isTipsEditing = false
    static var isShiftNotesEditing = false
    
    // MARK: Popups
    static var startOfPayPeriodSet = false
    static var PayPeriodLengthSet = false
    static var stateOfResidenceSet = false
    static var numberOfAllowancesSet = false
    static var filingStatusSet = false
    static var jobPositionArrayIsPopulated = false
    
    static var stateOfResidence: String?
    static var startOfPayPeriod = Date()
    static var filingStatus = Int()
    static var allowances = Int()
    
    // MARK: - OverView TabView
    
    static var UniqueID = UUID()
    static var payPeriodID = UUID().uuidString
    
    // MARK: String
    static var jobName: String?
    static var PayPeriodTips: String?
    static var PayPeriodWage: String?
    static var PayPeriodHours: String?
    static var MonthTips: String?
    static var MonthWage: String?
    static var MonthHour: String?
    static var ArchiveMonthTips: String?
    static var ArchiveMonthWage: String?
    static var ArchiveMonthHour: String?
    static var ArchiveMonthGrossWage: String?
    static var GrossWage: String?
    static var totalDeductions: String?
    static var totalNet: String?
    static var totalWithholdings: String?
    
    // MARK: - Shift Info TabView
    
    static var StartOfPayPeriod = Date()
    static var EndOfPayPeriod = Date()
    static var StartOfMonth = Date()
    static var StartOfWeekDate = Date()
    static var newEndTime = Date()
    static var clockInTime = Date()
    static var revisedEndTime = Date()
    
    
    static var thisShiftPosition: String?
    static var thisShiftRate: String?
    static var thisShiftClockIn: String?
    static var thisShiftClockOut: String?
    static var thisShiftHours: String?
    static var thisShiftChargeTip: String?
    static var thisShiftCashTip: String?
    static var thisShiftTipOut: String?
    static var thisShiftTotal: String?
    static var thisShiftsHourlyWage: String?
    static var thisShiftsTipTotal: String?
    static var ShiftTipsAdded: String?
    static var thisShiftNotes: String?
    
    static var ShiftNotesAdded = false
    static var clearSemiMonthlyArray = false
    static var clearBiWeeklyArray = false
    static var clearWeeklyArray = false
    static var firstPayPeriodAdded = false
    
    static var SemiMonthly = 1
    static var WhichBiWeeklyPayPeriod = 1
    static var Weekly = 1
    
    static var clearMonthlyTotals = false
    static var clearMonthlyPayPeriodTotals = false
    static var monthlyTotals = 1
    static var monthlyPayPeriodTotals = 1
    
    static var federalIncomeTax: String?
    static var stateIncomeTax: String?
    static var mediCare: String?
    static var SocialSecurity: String?
    
    static var netGross: Double!
    static var netDeductions: Double!
    static var netWithholdings:Double!
    
    static var loadNewJob = false
    
    
    
    // MARK: ShiftInfo Calendar
    
    static var CurrentMonthEditing = 1
    static var dateString = String()
    
    static var Overtime = String()

    static var selectedJanuaryDates = [Int]()
    static var selectedFebruaryDates = [Int]()
    static var selectedMarchDates = [Int]()
    static var selectedAprilDates = [Int]()
    static var selectedMayDates = [Int]()
    static var selectedJuneDates = [Int]()
    static var selectedJulyDates = [Int]()
    static var selectedAugustDates = [Int]()
    static var selectedSeptemberDates = [Int]()
    static var selectedOctoberDates = [Int]()
    static var selectedNovemberDates = [Int]()
    static var selectedDecemberDates = [Int]()
    static var selectedDates = [Int]()
    
    
    // MARK: History
    static var MonthlyTips: String?
    static var MonthlyWage: String?
    static var MonthlyGross: String?
    static var JanuaryTotal = String()
    static var JanuaryTips = String()
    static var JanuaryWages = String()
    static var FebruaryTotal = String()
    static var FebruaryTips = String()
    static var FebruaryWages = String()
    static var MarchTotal = String()
    static var MarchTips = String()
    static var MarchWages = String()
    static var AprilTotal = String()
    static var AprilTips = String()
    static var AprilWages = String()
    static var MayTotal = String()
    static var MayTips = String()
    static var MayWages = String()
    static var JuneTotal = String()
    static var JuneTips = String()
    static var JuneWages = String()
    static var JulyTotal = String()
    static var JulyTips = String()
    static var JulyWages = String()
    static var AugustTotal = String()
    static var AugustTips = String()
    static var AugustWages = String()
    static var SeptemberTotal = String()
    static var SeptemberTips = String()
    static var SeptemberWages = String()
    static var OctoberTotal = String()
    static var OctoberTips = String()
    static var OctoberWages = String()
    static var NovemberTotal = String()
    static var NovemberTips = String()
    static var NovemberWages = String()
    static var DecemberTotal = String()
    static var DecemberTips = String()
    static var DecemberWages = String()
    static var yearlyTotal = String()
    static var yearlyTips = String()
    static var yearlyWage = String()
    
    // MARK: Settings
    static var retirement401KPercentage: String!
    static var retirement401KAmount: String!
    static var retirement401KRothPercentage: String!
    static var retirement401KRothAmount: String!
    static var deductionPercentage: String!
    static var totalRetirementSum: String!
    static var totalInsuranceSum: String!
    static var totalAdditionalDeductionSum: String!
    static var medicalInsurance: String!
    static var medicalSavingsAccount: String!
    static var dentalInsurance: String!
    static var visionInsurance: String!
    
    static var retirement401Kadded = false
    static var retirement401KRothadded = false
    static var voluntaryDeductionArrayIsPopulated = false
    static var DeductionArrayIsPopulated = false
    static var positionHeight = 38
    static var taxesHeight = 214
    static var positionNum = 0
    static var retirementHeight = 0
    static var insuranceEditing = 0
    static var deductionHeight = 0
    
}
