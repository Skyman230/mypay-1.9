//
//  Calculate401KRothWithholdings.swift
//  myPay
//
//  Created by Donald Scott on 6/4/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation
import  UIKit

struct percentage401KRothEntry {
    var grossWages: Double
    var taxesWithheld: Double
    var percent:Double
}
class retirement401KRothPercentage {
    static func percentageRothEntry(entry: percentage401KRothEntry) -> Double {
        let subRothRetirement = rounded(entry.grossWages - entry.taxesWithheld)
        print(entry.grossWages)
        print(entry.taxesWithheld)
        print("this is subRothRetirement Amount" + "\(subRothRetirement)")
        let rothRetirement = rounded(subRothRetirement * entry.percent)
        return rothRetirement
    }
    static func rounded(_ number: Double) -> Double {
        return round(100 * number) / 100
    }
    
    
}

