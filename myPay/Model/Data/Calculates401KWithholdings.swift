//
//  Calculates401KWithholdings.swift
//  myPay
//
//  Created by Donald Scott on 6/3/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation
import  UIKit

struct percentageEntry {
    var percentage: Double
    var grossWages: Double
}
class retirement401KPercentage {
    static func percentageEntry(entry: percentageEntry) -> Double {
        let retirement = rounded(entry.grossWages * entry.percentage)
        return retirement
    }
    static func rounded(_ number: Double) -> Double {
        return round(100 * number) / 100
    }
    
}
