//
//  CalculateMediCare.swift
//  myPay
//
//  Created by Donald Scott on 5/20/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

struct mediCareEntry {
    var grossWages: Double
}
class CalculateMediCare {
    static func mediCareEntry(entry: mediCareEntry) -> String {
        let mediCare = rounded(entry.grossWages * 0.0145)
        return "\(mediCare)"
    }
    static func rounded(_ number: Double) -> Double {
        return round(100 * number) / 100
    }
}
