//
//  Arrays.swift
//  myPay
//
//  Created by Donald Scott on 5/19/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

var payPeriodArray = [ShiftModel]()
var WeeklyPayPeriodArray = [ShiftModel]()
var BiWeeklyPayPeriodWeek01Array = [ShiftModel]()
var BiWeeklyPayPeriodWeek02Array = [ShiftModel]()
var semiMonthlyPayPeriod1Array = [ShiftModel]()
var semiMonthlyPayPeriod2Array = [ShiftModel]()
var monthlyPayPeriodArray = [ShiftModel]()
var ArchiveArray = [ShiftModel]()
var MonthlyArchiveArray = [ShiftModel]()
var monthlyTotalArray = [ShiftModel]()
var JanuaryArray = [ShiftModel]()
var FebruaryArray = [ShiftModel]()
var MarchArray = [ShiftModel]()
var AprilArray = [ShiftModel]()
var MayArray = [ShiftModel]()
var JuneArray = [ShiftModel]()
var JulyArray = [ShiftModel]()
var AugustArray = [ShiftModel]()
var SeptemberArray = [ShiftModel]()
var OctoberArray = [ShiftModel]()
var NovemberArray = [ShiftModel]()
var DecemberArray = [ShiftModel]()
var YearlyMonthDetailArray = [ShiftModel]()

var voluntaryDeductionArray = [DeductionModel]()

var payPeriodGrossTipArray = [String]()
var payPeriodGrossWageArray = [String]()
var payPeriodTotalHoursArray = [String]()
var thisMonthsGrossTipArray = [String]()
var thisMonthsGrossWageArray = [String]()
var thisMonthsTotalHoursArray = [String]()
var incomeSourceArray = [String]()
var yearlyGrossArray = [String]()
var IncomeArray = [Double]()
var selectedJobName = [String]()

var selectedJanuaryDates = [Int]()
var selectedFebruaryDates = [Int]()
var selectedMarchDates = [Int]()
var selectedAprilDates = [Int]()
var selectedMayDates = [Int]()
var selectedJuneDates = [Int]()
var selectedJulyDates = [Int]()
var selectedAugustDates = [Int]()
var selectedSeptemberDates = [Int]()
var selectedOctoberDates = [Int]()
var selectedNovemberDates = [Int]()
var selectedDecemberDates = [Int]()
var selectedDates = [Int]()
