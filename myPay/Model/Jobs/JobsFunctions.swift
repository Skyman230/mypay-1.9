         //
//  JobsFunctions.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class JobsFunctions {
    
    static func createJobs(JobsModel: JobsModel) {
        Data.Jobs.append(JobsModel)
        Data.JobName.append(JobsModel.Name)
        print(Data.jobId)
    }
    static func populateMonthlyPayPeriod(at jobIndex: Int) {
        Data.Jobs[jobIndex].monthlypayPeriodModel = payPeriodArray
    }
    static func populateSemiMonthlyPayPeriod1(at jobIndex: Int) {
        Data.Jobs[jobIndex].semiMonthlyPayPeriod1Model = semiMonthlyPayPeriod1Array
    }
    static func populateSemiMonthlyPayPeriod2(at jobIndex: Int) {
        Data.Jobs[jobIndex].semiMonthlyPayPeriod2Model = semiMonthlyPayPeriod2Array
    }
    static func populateBiWeeklyPayPeriod1(at jobIndex: Int) {
        Data.Jobs[jobIndex].biWeeklyPayPeriod1Model = BiWeeklyPayPeriodWeek01Array
        
    }
    static func populateBiWeeklyPayPeriod2(at jobIndex: Int) {
        Data.Jobs[jobIndex].biWeeklyPayPeriod2Model = BiWeeklyPayPeriodWeek02Array
        
    }
    static func populateWeeklyPayPeriod2(at jobIndex: Int) {
        Data.Jobs[jobIndex].weeklyPayPeriodModel = WeeklyPayPeriodArray
        
    }
    static func populatePayPeriodTotals(at jobIndex: Int) {
        Data.Jobs[jobIndex].payPeriodGrossTip = payPeriodGrossTipArray
        Data.Jobs[jobIndex].payPeriodGrossWage = payPeriodGrossWageArray
        Data.Jobs[jobIndex].payPeriodHours = payPeriodTotalHoursArray
        
    }
    static func populateThisMonthsTotals(at jobIndex: Int) {
        Data.Jobs[jobIndex].thisMonthsGrossTip = thisMonthsGrossTipArray
        Data.Jobs[jobIndex].thisMonthsGrossWage = thisMonthsGrossWageArray
        Data.Jobs[jobIndex].thisMonthsTotalHours = thisMonthsTotalHoursArray
    }
    static func populateMonthlyTotals(at jobIndex: Int) {
        Data.Jobs[jobIndex].JanuaryArray = JanuaryArray
        Data.Jobs[jobIndex].FebruaryArray = FebruaryArray
        Data.Jobs[jobIndex].MarchArray = MarchArray
        Data.Jobs[jobIndex].AprilArray = AprilArray
        Data.Jobs[jobIndex].MayArray = MayArray
        Data.Jobs[jobIndex].JuneArray = JuneArray
        Data.Jobs[jobIndex].JulyArray = JulyArray
        Data.Jobs[jobIndex].AugustArray = AugustArray
        Data.Jobs[jobIndex].SeptemberArray = SeptemberArray
        Data.Jobs[jobIndex].OctoberArray = OctoberArray
        Data.Jobs[jobIndex].NovemberArray = NovemberArray
        Data.Jobs[jobIndex].DecemberArray = DecemberArray
    }
    
    static func populateCalendarDates(at jobIndex: Int) {
         Data.Jobs[jobIndex].selectedJanuaryDates = selectedJanuaryDates
         Data.Jobs[jobIndex].selectedFebruaryDates = selectedFebruaryDates
         Data.Jobs[jobIndex].selectedMarchDates = selectedMarchDates
         Data.Jobs[jobIndex].selectedAprilDates = selectedAprilDates
         Data.Jobs[jobIndex].selectedMayDates = selectedMayDates
         Data.Jobs[jobIndex].selectedJuneDates = selectedJuneDates
         Data.Jobs[jobIndex].selectedJulyDates = selectedJulyDates
         Data.Jobs[jobIndex].selectedAugustDates = selectedAugustDates
         Data.Jobs[jobIndex].selectedSeptemberDates = selectedSeptemberDates
         Data.Jobs[jobIndex].selectedOctoberDates = selectedOctoberDates
         Data.Jobs[jobIndex].selectedNovemberDates = selectedNovemberDates
         Data.Jobs[jobIndex].selectedDecemberDates = selectedDecemberDates
    }
    
    static func readJobNames(completion: @escaping () -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {

            DispatchQueue.main.async {
                completion()
            }
        }
    }
    static func readJob(by id:UUID, completion: @escaping (JobsModel?) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async {
            let Job = Data.Jobs.first(where: {$0.id == id})
            
            DispatchQueue.main.async {
                completion(Job)
            }
        }
    }
    static func updateJobs(at index: Int, Name: String) {
        Data.Jobs[index].Name = Name
        Data.JobName[index] = Name
        
    }
    static func updateJob(at index: Int, playInfo: Bool, currentShiftDate: String, jobsArrayPopulated: Bool, shiftTipsAdded: Bool, isTipsEditing: Bool, jobPositionArrayIsPopulated: Bool, PayPeriodTips: String, PayPeriodWage: String, PayPeriodHours: String, MonthTips: String, MonthWage: String, MonthHour: String, GrossWage: String, TotalDeductions: String, TotalWithholdings: String, TotalNet: String, PayPeriodLengthSet: Bool, PayPeriodLength: Int, startOfPayPeriodSet: Bool, stateOfResidenceSet: Bool, numberOfAllowancesSet: Bool, filingStatusSet: Bool, stateOfResidence: String, startOfPayPeriod: Date, EndOfPayPeriod: Date, StartOfWeekDate: Date, newEndTime: Date, clockInTime: Date, filingStatus: Int, allowances: Int, thisShiftPosition: String, thisShiftRate: String, thisShiftClockIn: String, thisShiftClockOut: String, thisShiftHours: String, thisShiftChargeTip: String, thisShiftCashTip: String, thisShiftTipOut: String, thisShiftTotal: String, thisShiftsHourlyWage: String, thisShiftsTipTotal: String, ShiftTipsAdded: String, clearSemiMonthlyArray: Bool, clearBiWeeklyArray: Bool, clearWeeklyArray: Bool, firstPayPeriodAdded: Bool, SemiMonthly: Int, WhichBiWeeklyPayPeriod: Int, Weekly: Int, startOfMonth: Date, clearMonthlyTotals: Bool, monthlyTotals: Int, monthlyPayPeriodTotals: Int, federalIncomeTax: String, stateIncomeTax: String, mediCare: String, SocialSecurity: String, netGross: Double, netDeductions: Double, netWithholdings:Double, CurrentMonthEditing: Int, dateString: String, revisedEndTime: Date, thisShiftNotes: String, ShiftNotesAdded: Bool, isShiftNotesEditing: Bool, Overtime: String,ArchiveMonthTips: String, ArchiveMonthWage: String, ArchiveMonthHour: String, ArchiveMonthGrossWage: String, yearlyTotal: String, yearlyTips: String, yearlyWage: String, retirement401KPercentage: String, retirement401KAmount: String, retirement401KRothPercentage: String, retirement401KRothAmount: String, deductionPercentage: String, totalRetirementSum: String, totalInsuranceSum: String, totalAdditionalDeductionSum: String, medicalInsurance: String, medicalSavingsAccount: String, dentalInsurance: String, visionInsurance: String, retirement401Kadded: Bool, retirement401KRothadded: Bool, voluntaryDeductionArrayIsPopulated: Bool, DeductionArrayIsPopulated: Bool, positionHeight: Int, taxesHeight: Int, positionNum: Int, retirementHeight: Int, insuranceEditing: Int, deductionHeight: Int) {
        Data.Jobs[index].playInfo = playInfo
        Data.Jobs[index].currentShiftDate = currentShiftDate
        Data.Jobs[index].jobsArrayPopulated = jobsArrayPopulated
        Data.Jobs[index].shiftTipsAdded = shiftTipsAdded
        Data.Jobs[index].isTipsEditing = isTipsEditing
        Data.Jobs[index].jobPositionArrayIsPopulated = jobPositionArrayIsPopulated
        Data.Jobs[index].PayPeriodTips = PayPeriodTips
        Data.Jobs[index].PayPeriodWage = PayPeriodWage
        Data.Jobs[index].PayPeriodHours = PayPeriodHours
        Data.Jobs[index].MonthTips = MonthTips
        Data.Jobs[index].MonthWage = MonthWage
        Data.Jobs[index].MonthHour = MonthHour
        Data.Jobs[index].GrossWage = GrossWage
        Data.Jobs[index].TotalDeductions = TotalDeductions
        Data.Jobs[index].TotalWithholdings = TotalWithholdings
        Data.Jobs[index].TotalNet = TotalNet
        Data.Jobs[index].PayPeriodLengthSet = PayPeriodLengthSet
        Data.Jobs[index].PayPeriodLength = PayPeriodLength
        Data.Jobs[index].startOfPayPeriodSet = startOfPayPeriodSet
        Data.Jobs[index].stateOfResidenceSet = stateOfResidenceSet
        Data.Jobs[index].numberOfAllowancesSet = numberOfAllowancesSet
        Data.Jobs[index].filingStatusSet = filingStatusSet
        Data.Jobs[index].stateOfResidence = stateOfResidence
        Data.Jobs[index].startOfPayPeriod = startOfPayPeriod
        Data.Jobs[index].EndOfPayPeriod = EndOfPayPeriod
        Data.Jobs[index].StartOfWeekDate = StartOfWeekDate
        Data.Jobs[index].newEndTime = newEndTime
        Data.Jobs[index].clockInTime = clockInTime
        Data.Jobs[index].filingStatus = filingStatus
        Data.Jobs[index].allowances = allowances
        Data.Jobs[index].thisShiftPosition = thisShiftPosition
        Data.Jobs[index].thisShiftRate = thisShiftRate
        Data.Jobs[index].thisShiftClockIn = thisShiftClockIn
        Data.Jobs[index].thisShiftClockOut = thisShiftClockOut
        Data.Jobs[index].thisShiftHours = thisShiftHours
        Data.Jobs[index].thisShiftChargeTip = thisShiftChargeTip
        Data.Jobs[index].thisShiftCashTip = thisShiftCashTip
        Data.Jobs[index].thisShiftTipOut = thisShiftTipOut
        Data.Jobs[index].thisShiftTotal = thisShiftTotal
        Data.Jobs[index].thisShiftsHourlyWage = thisShiftsHourlyWage
        Data.Jobs[index].thisShiftsTipTotal = thisShiftsTipTotal
        Data.Jobs[index].ShiftTipsAdded = ShiftTipsAdded
        Data.Jobs[index].clearSemiMonthlyArray = clearSemiMonthlyArray
        Data.Jobs[index].clearBiWeeklyArray = clearBiWeeklyArray
        Data.Jobs[index].clearWeeklyArray = clearWeeklyArray
        Data.Jobs[index].firstPayPeriodAdded = firstPayPeriodAdded
        Data.Jobs[index].SemiMonthly = SemiMonthly
        Data.Jobs[index].WhichBiWeeklyPayPeriod = WhichBiWeeklyPayPeriod
        Data.Jobs[index].Weekly = Weekly
        Data.Jobs[index].startOfMonth = startOfMonth
        Data.Jobs[index].clearMonthlyTotals = clearMonthlyTotals
        Data.Jobs[index].monthlyTotals = monthlyTotals
        Data.Jobs[index].monthlyPayPeriodTotals = monthlyPayPeriodTotals
        Data.Jobs[index].federalIncomeTax = federalIncomeTax
        Data.Jobs[index].stateIncomeTax = stateIncomeTax
        Data.Jobs[index].mediCare = mediCare
        Data.Jobs[index].SocialSecurity = SocialSecurity
        Data.Jobs[index].netGross = netGross
        Data.Jobs[index].netDeductions = netDeductions
        Data.Jobs[index].netWithholdings = netWithholdings
        Data.Jobs[index].CurrentMonthEditing = CurrentMonthEditing
        Data.Jobs[index].dateString = dateString
        Data.Jobs[index].revisedEndTime = revisedEndTime
        Data.Jobs[index].thisShiftNotes = thisShiftNotes
        Data.Jobs[index].ShiftNotesAdded = ShiftNotesAdded
        Data.Jobs[index].isShiftNotesEditing = isShiftNotesEditing
        Data.Jobs[index].Overtime = Overtime
        Data.Jobs[index].ArchiveMonthTips = ArchiveMonthTips
        Data.Jobs[index].ArchiveMonthWage = ArchiveMonthWage
        Data.Jobs[index].ArchiveMonthHour = ArchiveMonthHour
        Data.Jobs[index].ArchiveMonthGrossWage = ArchiveMonthGrossWage
        Data.Jobs[index].yearlyTotal = yearlyTotal
        Data.Jobs[index].yearlyTips = yearlyTips
        Data.Jobs[index].yearlyWage = yearlyWage
        Data.Jobs[index].retirement401KRothPercentage = retirement401KRothPercentage
        Data.Jobs[index].retirement401KRothAmount = retirement401KRothAmount
        Data.Jobs[index].retirement401KPercentage = retirement401KPercentage
        Data.Jobs[index].retirement401KAmount = retirement401KAmount
        Data.Jobs[index].deductionPercentage = deductionPercentage
        Data.Jobs[index].totalRetirementSum = totalRetirementSum
        Data.Jobs[index].totalInsuranceSum = totalInsuranceSum
        Data.Jobs[index].totalAdditionalDeductionSum = totalAdditionalDeductionSum
        Data.Jobs[index].medicalInsurance = medicalInsurance
        Data.Jobs[index].medicalSavingsAccount = medicalSavingsAccount
        Data.Jobs[index].dentalInsurance = dentalInsurance
        Data.Jobs[index].visionInsurance = visionInsurance
        Data.Jobs[index].retirement401Kadded = retirement401Kadded
        Data.Jobs[index].retirement401KRothadded = retirement401KRothadded
        Data.Jobs[index].voluntaryDeductionArrayIsPopulated = voluntaryDeductionArrayIsPopulated
        Data.Jobs[index].DeductionArrayIsPopulated = DeductionArrayIsPopulated
        Data.Jobs[index].positionHeight = positionHeight
        Data.Jobs[index].taxesHeight = taxesHeight
        Data.Jobs[index].positionNum = positionNum
        Data.Jobs[index].retirementHeight = retirementHeight
        Data.Jobs[index].insuranceEditing = insuranceEditing
        Data.Jobs[index].deductionHeight = deductionHeight
        
        
    }
    static func deleteJobs(index: Int) {
        Data.Jobs.remove(at: index)
        Data.JobName.remove(at: index)
        print(Data.jobId)
    }
    static func deletePosition(index: Int) {
        let jobIndex = GlobalVariable.jobIndex
        Data.Jobs[jobIndex].positionModel.remove(at: index)
    }
    static func deleteDeduction(index: Int) {
        let jobIndex = GlobalVariable.jobIndex
        Data.Jobs[jobIndex].voluntaryDeductions.remove(at: index)
    }
    
    static func loadSelectedJob(index: Int) {
        GlobalVariable.jobIndex = index
        GlobalVariable.UniqueID = Data.Jobs[index].id
        GlobalVariable.jobName = Data.Jobs[index].Name
        GlobalVariable.playInfo = Data.Jobs[index].playInfo
        GlobalVariable.currentShiftDate = Data.Jobs[index].currentShiftDate
        GlobalVariable.PayPeriodLength = Data.Jobs[index].PayPeriodLength
        GlobalVariable.jobsArrayPopulated = Data.Jobs[index].jobsArrayPopulated
        GlobalVariable.ShiftTipsAdded = Data.Jobs[index].ShiftTipsAdded
        GlobalVariable.isTipsEditing = Data.Jobs[index].isTipsEditing
        GlobalVariable.startOfPayPeriodSet = Data.Jobs[index].startOfPayPeriodSet
        GlobalVariable.PayPeriodLengthSet = Data.Jobs[index].PayPeriodLengthSet
        GlobalVariable.stateOfResidenceSet = Data.Jobs[index].stateOfResidenceSet
        GlobalVariable.numberOfAllowancesSet = Data.Jobs[index].numberOfAllowancesSet
        GlobalVariable.filingStatusSet = Data.Jobs[index].filingStatusSet
        GlobalVariable.jobPositionArrayIsPopulated = Data.Jobs[index].jobPositionArrayIsPopulated
        GlobalVariable.stateOfResidence = Data.Jobs[index].stateOfResidence
        GlobalVariable.startOfPayPeriod = Data.Jobs[index].startOfPayPeriod
        GlobalVariable.filingStatus = Data.Jobs[index].filingStatus
        GlobalVariable.allowances = Data.Jobs[index].allowances
        GlobalVariable.PayPeriodTips = Data.Jobs[index].PayPeriodTips
        GlobalVariable.PayPeriodWage = Data.Jobs[index].PayPeriodWage
        GlobalVariable.PayPeriodHours = Data.Jobs[index].PayPeriodHours
        GlobalVariable.MonthTips = Data.Jobs[index].MonthTips
        GlobalVariable.MonthWage = Data.Jobs[index].MonthWage
        GlobalVariable.MonthHour = Data.Jobs[index].MonthHour
        GlobalVariable.GrossWage = Data.Jobs[index].GrossWage
        GlobalVariable.totalDeductions = Data.Jobs[index].TotalDeductions
        GlobalVariable.totalNet = Data.Jobs[index].TotalNet
        GlobalVariable.totalWithholdings = Data.Jobs[index].TotalWithholdings
        GlobalVariable.startOfPayPeriod = Data.Jobs[index].startOfPayPeriod
        GlobalVariable.EndOfPayPeriod = Data.Jobs[index].EndOfPayPeriod
        GlobalVariable.StartOfMonth = Data.Jobs[index].startOfMonth
        GlobalVariable.StartOfWeekDate = Data.Jobs[index].StartOfWeekDate
        GlobalVariable.newEndTime = Data.Jobs[index].newEndTime
        GlobalVariable.clockInTime = Data.Jobs[index].clockInTime
        GlobalVariable.thisShiftPosition = Data.Jobs[index].thisShiftPosition
        GlobalVariable.thisShiftRate = Data.Jobs[index].thisShiftRate
        GlobalVariable.thisShiftClockIn = Data.Jobs[index].thisShiftClockIn
        GlobalVariable.thisShiftClockOut = Data.Jobs[index].thisShiftClockOut
        GlobalVariable.thisShiftHours = Data.Jobs[index].thisShiftHours
        GlobalVariable.thisShiftChargeTip = Data.Jobs[index].thisShiftChargeTip
        GlobalVariable.thisShiftCashTip = Data.Jobs[index].thisShiftCashTip
        GlobalVariable.thisShiftTipOut = Data.Jobs[index].thisShiftTipOut
        GlobalVariable.thisShiftTotal = Data.Jobs[index].thisShiftTotal
        GlobalVariable.thisShiftsHourlyWage = Data.Jobs[index].thisShiftsHourlyWage
        GlobalVariable.thisShiftsTipTotal = Data.Jobs[index].thisShiftsTipTotal
        GlobalVariable.ShiftTipsAdded = Data.Jobs[index].ShiftTipsAdded
        GlobalVariable.clearSemiMonthlyArray = Data.Jobs[index].clearSemiMonthlyArray
        GlobalVariable.clearBiWeeklyArray = Data.Jobs[index].clearBiWeeklyArray
        GlobalVariable.clearWeeklyArray = Data.Jobs[index].clearWeeklyArray
        GlobalVariable.firstPayPeriodAdded = Data.Jobs[index].firstPayPeriodAdded
        GlobalVariable.SemiMonthly = Data.Jobs[index].SemiMonthly
        GlobalVariable.WhichBiWeeklyPayPeriod = Data.Jobs[index].WhichBiWeeklyPayPeriod
        GlobalVariable.Weekly = Data.Jobs[index].Weekly
        GlobalVariable.StartOfMonth = Data.Jobs[index].startOfMonth
        GlobalVariable.clearWeeklyArray = Data.Jobs[index].clearMonthlyTotals
        GlobalVariable.monthlyPayPeriodTotals = Data.Jobs[index].monthlyPayPeriodTotals
        GlobalVariable.federalIncomeTax = Data.Jobs[index].federalIncomeTax
        GlobalVariable.stateIncomeTax = Data.Jobs[index].stateIncomeTax
        GlobalVariable.mediCare = Data.Jobs[index].mediCare
        GlobalVariable.SocialSecurity = Data.Jobs[index].SocialSecurity
        GlobalVariable.netGross = Data.Jobs[index].netGross
        GlobalVariable.netDeductions = Data.Jobs[index].netDeductions
        GlobalVariable.netWithholdings = Data.Jobs[index].netWithholdings
        GlobalVariable.CurrentMonthEditing = Data.Jobs[index].CurrentMonthEditing
        GlobalVariable.dateString = Data.Jobs[index].dateString
        GlobalVariable.revisedEndTime = Data.Jobs[index].revisedEndTime
        GlobalVariable.thisShiftNotes = Data.Jobs[index].thisShiftNotes
        GlobalVariable.ShiftNotesAdded = Data.Jobs[index].ShiftNotesAdded
        GlobalVariable.isShiftNotesEditing = Data.Jobs[index].isShiftNotesEditing
        GlobalVariable.Overtime = Data.Jobs[index].Overtime
        GlobalVariable.ArchiveMonthTips = Data.Jobs[index].ArchiveMonthTips
        GlobalVariable.ArchiveMonthWage = Data.Jobs[index].ArchiveMonthWage
        GlobalVariable.ArchiveMonthHour = Data.Jobs[index].ArchiveMonthHour
        GlobalVariable.ArchiveMonthGrossWage = Data.Jobs[index].ArchiveMonthGrossWage
        GlobalVariable.yearlyTotal = Data.Jobs[index].yearlyTotal ?? "$000.00"
        GlobalVariable.yearlyTips = Data.Jobs[index].yearlyTips ?? "$000.00"
        GlobalVariable.yearlyWage = Data.Jobs[index].yearlyWage ?? "$000.00"
        GlobalVariable.retirement401KPercentage = Data.Jobs[index].retirement401KPercentage
        GlobalVariable.retirement401KAmount = Data.Jobs[index].retirement401KAmount
        GlobalVariable.retirement401KRothPercentage = Data.Jobs[index].retirement401KRothPercentage
        GlobalVariable.retirement401KRothAmount = Data.Jobs[index].retirement401KRothAmount
        GlobalVariable.deductionPercentage = Data.Jobs[index].deductionPercentage
        GlobalVariable.totalRetirementSum = Data.Jobs[index].totalRetirementSum
        GlobalVariable.totalInsuranceSum = Data.Jobs[index].totalInsuranceSum
        GlobalVariable.totalAdditionalDeductionSum = Data.Jobs[index].totalAdditionalDeductionSum
        GlobalVariable.medicalInsurance = Data.Jobs[index].medicalInsurance
        GlobalVariable.medicalSavingsAccount = Data.Jobs[index].medicalSavingsAccount
        GlobalVariable.dentalInsurance = Data.Jobs[index].dentalInsurance
        GlobalVariable.visionInsurance = Data.Jobs[index].visionInsurance
        GlobalVariable.retirement401Kadded = Data.Jobs[index].retirement401Kadded
        GlobalVariable.retirement401KRothadded = Data.Jobs[index].retirement401KRothadded
        GlobalVariable.voluntaryDeductionArrayIsPopulated = Data.Jobs[index].voluntaryDeductionArrayIsPopulated
        GlobalVariable.positionHeight = Data.Jobs[index].positionHeight
        GlobalVariable.taxesHeight = Data.Jobs[index].taxesHeight
        GlobalVariable.positionNum = Data.Jobs[index].positionNum
        GlobalVariable.retirementHeight = Data.Jobs[index].retirementHeight
        GlobalVariable.insuranceEditing = Data.Jobs[index].insuranceEditing
        GlobalVariable.deductionHeight = Data.Jobs[index].deductionHeight
        
        WeeklyPayPeriodArray = Data.Jobs[index].weeklyPayPeriodModel
        BiWeeklyPayPeriodWeek01Array = Data.Jobs[index].biWeeklyPayPeriod1Model
        BiWeeklyPayPeriodWeek02Array = Data.Jobs[index].biWeeklyPayPeriod2Model
        semiMonthlyPayPeriod1Array = Data.Jobs[index].semiMonthlyPayPeriod1Model
        semiMonthlyPayPeriod2Array = Data.Jobs[index].semiMonthlyPayPeriod2Model
        monthlyPayPeriodArray = Data.Jobs[index].monthlypayPeriodModel
        JanuaryArray = Data.Jobs[index].JanuaryArray
        FebruaryArray = Data.Jobs[index].FebruaryArray
        MarchArray = Data.Jobs[index].MarchArray
        AprilArray = Data.Jobs[index].AprilArray
        MayArray = Data.Jobs[index].MayArray
        JuneArray = Data.Jobs[index].JuneArray
        JulyArray = Data.Jobs[index].JulyArray
        AugustArray = Data.Jobs[index].AugustArray
        SeptemberArray = Data.Jobs[index].SeptemberArray
        OctoberArray = Data.Jobs[index].OctoberArray
        NovemberArray = Data.Jobs[index].NovemberArray
        DecemberArray = Data.Jobs[index].DecemberArray
        payPeriodGrossTipArray = Data.Jobs[index].payPeriodGrossTip
        payPeriodGrossWageArray = Data.Jobs[index].payPeriodGrossWage
        payPeriodTotalHoursArray = Data.Jobs[index].payPeriodHours
        thisMonthsGrossTipArray = Data.Jobs[index].thisMonthsGrossTip
        thisMonthsGrossWageArray = Data.Jobs[index].thisMonthsGrossWage
        thisMonthsTotalHoursArray = Data.Jobs[index].thisMonthsTotalHours
        incomeSourceArray = Data.Jobs[index].incomeSourceArray
        IncomeArray = Data.Jobs[index].IncomeArray
        voluntaryDeductionArray = Data.Jobs[index].voluntaryDeductions
        
        
        if Data.Jobs[index].selectedJanuaryDates.count == 0 {
            selectedJanuaryDates = []
        } else {
            selectedJanuaryDates = Data.Jobs[index].selectedJanuaryDates
        }
        if Data.Jobs[index].selectedFebruaryDates.count == 0 {
            selectedFebruaryDates = []
        } else {
            selectedFebruaryDates = Data.Jobs[index].selectedFebruaryDates
        }
        if Data.Jobs[index].selectedMarchDates.count == 0 {
            selectedMarchDates = []
        } else {
            selectedMarchDates = Data.Jobs[index].selectedMarchDates
        }
        if Data.Jobs[index].selectedAprilDates.count == 0 {
            selectedAprilDates = []
        } else {
            selectedAprilDates = Data.Jobs[index].selectedAprilDates
        }
        if Data.Jobs[index].selectedMayDates.count == 0 {
            selectedMayDates = []
        } else {
            selectedMayDates = Data.Jobs[index].selectedMayDates
        }
        if Data.Jobs[index].selectedJuneDates.count == 0 {
            selectedJuneDates = []
        } else {
            selectedJuneDates = Data.Jobs[index].selectedJuneDates
        }
        if Data.Jobs[index].selectedJulyDates.count == 0 {
            selectedJulyDates = []
        } else {
            selectedJulyDates = Data.Jobs[index].selectedJulyDates
        }
        if Data.Jobs[index].selectedAugustDates.count == 0 {
            selectedAugustDates = []
        } else {
            selectedAugustDates = Data.Jobs[index].selectedAugustDates
        }
        if Data.Jobs[index].selectedSeptemberDates.count == 0 {
            selectedSeptemberDates = []
        } else {
            selectedSeptemberDates = Data.Jobs[index].selectedSeptemberDates
        }
        if Data.Jobs[index].selectedOctoberDates.count == 0 {
            selectedOctoberDates = []
        } else {
            selectedOctoberDates = Data.Jobs[index].selectedOctoberDates
        }
        if Data.Jobs[index].selectedNovemberDates.count == 0 {
            selectedNovemberDates = []
        } else {
            selectedNovemberDates = Data.Jobs[index].selectedNovemberDates
        }
        if Data.Jobs[index].selectedDecemberDates.count == 0 {
            selectedDecemberDates = []
        } else {
            selectedDecemberDates = Data.Jobs[index].selectedDecemberDates
        }
    }
}


