//
//  JobsModel.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation


struct  JobsModel  {
    var id: UUID
    var Name: String!
    var playInfo: Bool!
    var currentShiftDate: String?
    var jobsArrayPopulated: Bool!
    var shiftTipsAdded: Bool!
    var isTipsEditing: Bool!
    var jobPositionArrayIsPopulated: Bool!
    var PayPeriodTips: String!
    var PayPeriodWage: String!
    var PayPeriodHours: String!
    var MonthTips: String!
    var MonthWage: String!
    var MonthHour: String!
    var GrossWage: String!
    var TotalDeductions: String!
    var TotalWithholdings: String!
    var TotalNet: String!
    var PayPeriodLengthSet: Bool!
    var PayPeriodLength: Int!
    var startOfPayPeriodSet: Bool!
    var stateOfResidenceSet: Bool!
    var numberOfAllowancesSet: Bool!
    var filingStatusSet: Bool!
    var stateOfResidence: String!
    var startOfPayPeriod: Date!
    var EndOfPayPeriod: Date!
    var StartOfWeekDate: Date!
    var newEndTime: Date!
    var clockInTime: Date
    var filingStatus: Int!
    var allowances: Int!
    var thisShiftPosition: String?
    var thisShiftRate: String?
    var thisShiftClockIn: String?
    var thisShiftClockOut: String?
    var thisShiftHours: String?
    var thisShiftChargeTip: String?
    var thisShiftCashTip: String?
    var thisShiftTipOut: String?
    var thisShiftTotal: String?
    var thisShiftsHourlyWage: String?
    var thisShiftsTipTotal: String?
    var ShiftTipsAdded: String?
    var clearSemiMonthlyArray: Bool!
    var clearBiWeeklyArray: Bool!
    var clearWeeklyArray: Bool!
    var firstPayPeriodAdded: Bool!
    var SemiMonthly: Int!
    var WhichBiWeeklyPayPeriod: Int!
    var Weekly: Int!
    var startOfMonth: Date!
    var clearMonthlyTotals: Bool!
    var monthlyTotals: Int!
    var monthlyPayPeriodTotals: Int!
    var federalIncomeTax: String?
    var stateIncomeTax: String?
    var mediCare: String?
    var SocialSecurity: String?
    var netGross: Double?
    var netDeductions: Double?
    var netWithholdings:Double?
    var CurrentMonthEditing: Int!
    var dateString: String!
    var revisedEndTime: Date!
    var thisShiftNotes: String?
    var ShiftNotesAdded: Bool!
    var isShiftNotesEditing: Bool!
    var Overtime: String!
    var ArchiveMonthTips: String?
    var ArchiveMonthWage: String?
    var ArchiveMonthHour: String?
    var ArchiveMonthGrossWage: String?
    var yearlyTotal: String?
    var yearlyTips: String?
    var yearlyWage: String?
    var retirement401KPercentage: String?
    var retirement401KAmount: String?
    var retirement401KRothPercentage: String?
    var retirement401KRothAmount: String?
    var deductionPercentage: String?
    var totalRetirementSum: String?
    var totalInsuranceSum: String?
    var totalAdditionalDeductionSum: String?
    var medicalInsurance: String?
    var medicalSavingsAccount: String?
    var dentalInsurance: String?
    var visionInsurance: String?
    
    var retirement401Kadded: Bool!
    var retirement401KRothadded: Bool!
    var voluntaryDeductionArrayIsPopulated: Bool!
    var DeductionArrayIsPopulated: Bool!
    var positionHeight: Int!
    var taxesHeight: Int!
    var positionNum: Int!
    var retirementHeight: Int!
    var insuranceEditing: Int!
    var deductionHeight: Int!
    
    
    var positionModel = [PositionModel]()
    var weeklyPayPeriodModel = [ShiftModel]()
    var biWeeklyPayPeriod1Model = [ShiftModel]()
    var biWeeklyPayPeriod2Model = [ShiftModel]()
    var semiMonthlyPayPeriod1Model = [ShiftModel]()
    var semiMonthlyPayPeriod2Model = [ShiftModel]()
    var monthlypayPeriodModel = [ShiftModel]()
    var payPeriodGrossTip = [String]()
    var payPeriodGrossWage = [String]()
    var payPeriodHours = [String]()
    var incomeSourceArray = [String]()
    var IncomeArray = [Double]()
    var thisMonthsGrossTip = [String]()
    var thisMonthsGrossWage = [String]()
    var thisMonthsTotalHours = [String]()
    var JanuaryArray = [ShiftModel]()
    var FebruaryArray = [ShiftModel]()
    var MarchArray = [ShiftModel]()
    var AprilArray = [ShiftModel]()
    var MayArray = [ShiftModel]()
    var JuneArray = [ShiftModel]()
    var JulyArray = [ShiftModel]()
    var AugustArray = [ShiftModel]()
    var SeptemberArray = [ShiftModel]()
    var OctoberArray = [ShiftModel]()
    var NovemberArray = [ShiftModel]()
    var DecemberArray = [ShiftModel]()
    var selectedJanuaryDates = [Int]()
    var selectedFebruaryDates = [Int]()
    var selectedMarchDates = [Int]()
    var selectedAprilDates = [Int]()
    var selectedMayDates = [Int]()
    var selectedJuneDates = [Int]()
    var selectedJulyDates = [Int]()
    var selectedAugustDates = [Int]()
    var selectedSeptemberDates = [Int]()
    var selectedOctoberDates = [Int]()
    var selectedNovemberDates = [Int]()
    var selectedDecemberDates = [Int]()
    var selectedDates = [Int]()
    var voluntaryDeductions = [DeductionModel]()
    
    init(Name: String, playInfo: Bool, currentShiftDate: String, jobsArrayPopulated: Bool, shiftTipsAdded: Bool, isTipsEditing: Bool, jobPositionArrayIsPopulated: Bool, PayPeriodTips: String, PayPeriodWage: String, PayPeriodHours: String, MonthTips: String, MonthWage: String, MonthHour: String, GrossWage: String, TotalDeductions: String, TotalWithholdings: String, TotalNet: String, PayPeriodLengthSet: Bool, PayPeriodLength: Int, startOfPayPeriodSet: Bool, stateOfResidenceSet: Bool, numberOfAllowancesSet: Bool, filingStatusSet: Bool, stateOfResidence: String, startOfPayPeriod: Date, EndOfPayPeriod: Date, StartOfWeekDate: Date, newEndTime: Date, clockInTime: Date, filingStatus: Int, allowances: Int, thisShiftPosition: String, thisShiftRate: String, thisShiftClockIn: String, thisShiftClockOut: String, thisShiftHours: String, thisShiftChargeTip: String, thisShiftCashTip: String, thisShiftTipOut: String, thisShiftTotal: String, thisShiftsHourlyWage: String, thisShiftsTipTotal: String, ShiftTipsAdded: String, clearSemiMonthlyArray: Bool, clearBiWeeklyArray: Bool, clearWeeklyArray: Bool, firstPayPeriodAdded: Bool, SemiMonthly: Int, WhichBiWeeklyPayPeriod: Int, Weekly: Int, startOfMonth: Date, clearMonthlyTotals: Bool, monthlyTotals: Int, monthlyPayPeriodTotals: Int, federalIncomeTax: String, stateIncomeTax: String, mediCare: String, SocialSecurity: String, netGross: Double, netDeductions: Double, netWithholdings:Double, CurrentMonthEditing: Int, dateString: String, revisedEndTime: Date, thisShiftNotes: String, ShiftNotesAdded: Bool, isShiftNotesEditing: Bool, Overtime: String, ArchiveMonthTips: String, ArchiveMonthWage: String, ArchiveMonthHour: String, ArchiveMonthGrossWage: String, yearlyTotal: String, yearlyTips: String, yearlyWage: String, retirement401KPercentage: String, retirement401KAmount: String, retirement401KRothPercentage: String, retirement401KRothAmount: String, deductionPercentage: String, totalRetirementSum: String, totalInsuranceSum: String, totalAdditionalDeductionSum: String, medicalInsurance: String, medicalSavingsAccount: String, dentalInsurance: String, visionInsurance: String, retirement401Kadded: Bool, retirement401KRothadded: Bool, voluntaryDeductionArrayIsPopulated: Bool, DeductionArrayIsPopulated: Bool, positionHeight: Int, taxesHeight: Int, positionNum: Int, retirementHeight: Int, insuranceEditing: Int, deductionHeight: Int, data:[PositionModel]? = nil, data2: [ShiftModel]? = nil, data3: [ShiftModel]? = nil, data4: [ShiftModel]? = nil, data5: [ShiftModel]? = nil, data6: [ShiftModel]? = nil, data7: [ShiftModel]? = nil, data8: [String]? = nil, data9: [String]? = nil, data10: [String]? = nil, data11: [String]? = nil, data12: [Double]? = nil, data13: [String]? = nil, data14: [String]? = nil, data15: [String]? = nil, data16: [ShiftModel]? = nil, data17: [ShiftModel]? = nil, data18: [ShiftModel]? = nil, data19: [ShiftModel]? = nil, data20: [ShiftModel]? = nil, data21: [ShiftModel]? = nil, data22: [ShiftModel]? = nil, data23: [ShiftModel]? = nil, data24: [ShiftModel]? = nil, data25: [ShiftModel]? = nil, data26: [ShiftModel]? = nil, data27: [ShiftModel]? = nil, data28: [Int]? = nil, data29: [Int]? = nil, data30: [Int]? = nil, data31: [Int]? = nil, data32: [Int]? = nil, data33: [Int]? = nil, data34: [Int]? = nil, data35: [Int]? = nil, data36: [Int]? = nil, data37: [Int]? = nil, data38: [Int]? = nil, data39: [Int]? = nil, data40: [DeductionModel]? = nil) {
        
        id = UUID()
        self.Name = Name
        self.playInfo = playInfo
        self.currentShiftDate = currentShiftDate
        self.jobsArrayPopulated = jobsArrayPopulated
        self.shiftTipsAdded = shiftTipsAdded
        self.isTipsEditing = isTipsEditing
        self.jobPositionArrayIsPopulated = jobPositionArrayIsPopulated
        self.PayPeriodTips = PayPeriodTips
        self.PayPeriodWage = PayPeriodWage
        self.PayPeriodHours = PayPeriodHours
        self.MonthTips = MonthTips
        self.MonthWage = MonthWage
        self.MonthHour = MonthHour
        self.GrossWage = GrossWage
        self.TotalDeductions = TotalDeductions
        self.TotalWithholdings = TotalWithholdings
        self.TotalNet = TotalNet
        self.PayPeriodLengthSet = PayPeriodLengthSet
        self.PayPeriodLength = PayPeriodLength
        self.startOfPayPeriodSet = startOfPayPeriodSet
        self.stateOfResidenceSet = stateOfResidenceSet
        self.numberOfAllowancesSet = numberOfAllowancesSet
        self.filingStatusSet = filingStatusSet
        self.stateOfResidence = stateOfResidence
        self.startOfPayPeriod = startOfPayPeriod
        self.EndOfPayPeriod = EndOfPayPeriod
        self.StartOfWeekDate = StartOfWeekDate
        self.newEndTime = newEndTime
        self.clockInTime = clockInTime
        self.filingStatus = filingStatus
        self.allowances = allowances
        self.thisShiftPosition = thisShiftPosition
        self.thisShiftRate = thisShiftRate
        self.thisShiftClockIn = thisShiftClockIn
        self.thisShiftClockOut = thisShiftClockOut
        self.thisShiftHours = thisShiftHours
        self.thisShiftChargeTip = thisShiftChargeTip
        self.thisShiftCashTip = thisShiftCashTip
        self.thisShiftTipOut = thisShiftTipOut
        self.thisShiftTotal = thisShiftTotal
        self.thisShiftsHourlyWage = thisShiftsHourlyWage
        self.thisShiftsTipTotal = thisShiftsTipTotal
        self.ShiftTipsAdded = ShiftTipsAdded
        self.clearSemiMonthlyArray = clearSemiMonthlyArray
        self.clearBiWeeklyArray = clearBiWeeklyArray
        self.clearWeeklyArray = clearWeeklyArray
        self.firstPayPeriodAdded = firstPayPeriodAdded
        self.SemiMonthly = SemiMonthly
        self.WhichBiWeeklyPayPeriod = WhichBiWeeklyPayPeriod
        self.Weekly = Weekly
        self.startOfMonth = startOfMonth
        self.clearMonthlyTotals = clearMonthlyTotals
        self.monthlyTotals = monthlyTotals
        self.monthlyPayPeriodTotals = monthlyPayPeriodTotals
        self.federalIncomeTax = federalIncomeTax
        self.stateIncomeTax = stateIncomeTax
        self.mediCare = mediCare
        self.SocialSecurity = SocialSecurity
        self.netGross = netGross
        self.netDeductions = netDeductions
        self.netWithholdings = netWithholdings
        self.CurrentMonthEditing = CurrentMonthEditing
        self.dateString = dateString
        self.revisedEndTime = revisedEndTime
        self.thisShiftNotes = thisShiftNotes
        self.ShiftNotesAdded = ShiftNotesAdded
        self.isShiftNotesEditing = isShiftNotesEditing
        self.Overtime = Overtime
        self.ArchiveMonthTips = ArchiveMonthTips
        self.ArchiveMonthWage = ArchiveMonthWage
        self.ArchiveMonthHour = ArchiveMonthHour
        self.ArchiveMonthGrossWage = ArchiveMonthGrossWage
        self.yearlyTotal = yearlyTotal
        self.yearlyTips = yearlyTips
        self.yearlyWage = yearlyWage
        self.retirement401KAmount = retirement401KAmount
        self.retirement401KPercentage = retirement401KPercentage
        self.retirement401KRothAmount = retirement401KRothAmount
        self.retirement401KRothPercentage = retirement401KRothPercentage
        self.deductionPercentage = deductionPercentage
        self.totalRetirementSum = totalRetirementSum
        self.totalInsuranceSum = totalInsuranceSum
        self.totalAdditionalDeductionSum = totalAdditionalDeductionSum
        self.medicalInsurance = medicalInsurance
        self.medicalSavingsAccount = medicalSavingsAccount
        self.dentalInsurance = dentalInsurance
        self.visionInsurance = visionInsurance
        self.retirement401Kadded = retirement401Kadded
        self.retirement401KRothadded = retirement401KRothadded
        self.voluntaryDeductionArrayIsPopulated = voluntaryDeductionArrayIsPopulated
        self.DeductionArrayIsPopulated = DeductionArrayIsPopulated
        self.positionHeight = positionHeight
        self.taxesHeight = taxesHeight
        self.positionNum = positionNum
        self.retirementHeight = retirementHeight
        self.insuranceEditing = insuranceEditing
        self.deductionHeight = deductionHeight
        
        if let data = data {
            self.positionModel = data
        }
        if let data2 = data2 {
            self.weeklyPayPeriodModel = data2
        }
        if let data3 = data3 {
            self.biWeeklyPayPeriod1Model = data3
        }
        if let data4 = data4 {
            self.biWeeklyPayPeriod2Model = data4
        }
        if let data5 = data5 {
            self.semiMonthlyPayPeriod1Model = data5
        }
        if let data6 = data6 {
            self.semiMonthlyPayPeriod2Model = data6
        }
        if let data7 = data7 {
            self.monthlypayPeriodModel = data7
        }
        if let data8 = data8 {
            self.payPeriodGrossTip = data8
        }
        if let data9 = data9 {
            self.payPeriodGrossWage = data9
        }
        if let data10 = data10 {
            self.payPeriodHours = data10
        }
        if let data11 = data11 {
            self.incomeSourceArray = data11
        }
        if let data12 = data12 {
            self.IncomeArray = data12
        }
        if let data13 = data13 {
            self.thisMonthsGrossTip = data13
        }
        if let data14 = data14 {
            self.thisMonthsGrossWage = data14
        }
        if let data15 = data15 {
            self.thisMonthsTotalHours = data15
        }
        if let data16 = data16 {
            self.JanuaryArray = data16
        }
        if let data17 = data17 {
            self.FebruaryArray = data17
        }
        if let data18 = data18 {
            self.MarchArray = data18
        }
        if let data19 = data19 {
            self.AprilArray = data19
        }
        if let data20 = data20 {
            self.MayArray = data20
        }
        if let data21 = data21 {
            self.JuneArray = data21
        }
        if let data22 = data22 {
            self.JulyArray = data22
        }
        if let data23 = data23 {
            self.AugustArray = data23
        }
        if let data24 = data24 {
            self.SeptemberArray = data24
        }
        if let data25 = data25 {
            self.OctoberArray = data25
        }
        if let data26 = data26 {
            self.NovemberArray = data26
        }
        if let data27 = data27 {
            self.DecemberArray = data27
        }
        if let data28 = data28 {
            self.selectedJanuaryDates = data28
        }
        if let data29 = data29 {
            self.selectedFebruaryDates = data29
        }
        if let data30 = data30 {
            self.selectedMarchDates = data30
        }
        if let data31 = data31 {
            self.selectedAprilDates = data31
        }
        if let data32 = data32 {
            self.selectedMayDates = data32
        }
        if let data33 = data33 {
            self.selectedJuneDates = data33
        }
        if let data34 = data34 {
            self.selectedJulyDates = data34
        }
        if let data35 = data35 {
            self.selectedAugustDates = data35
        }
        if let data36 = data36 {
            self.selectedSeptemberDates = data36
        }
        if let data37 = data37 {
            self.selectedOctoberDates = data37
        }
        if let data38 = data38 {
            self.selectedNovemberDates = data38
        }
        if let data39 = data39 {
            self.selectedDecemberDates = data39
        }
        if let data40 = data40 {
            self.voluntaryDeductions = data40
        }
        
    }
}

