//
//  ShiftModel.swift
//  myPay
//
//  Created by Donald Scott on 5/13/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

struct ShiftModel {
    var id: String!
    var date: String!
    var shiftTotal: String!
    var cashTip: String!
    var chargeTip: String!
    var tipOut: String!
    var tipTotal: String!
    var position: String!
    var hourlyRate: String!
    var Hours: String!
    var totalHourlyWage: String!
    var clockIn: String!
    var clockOut: String!
    var shiftNotes: String!
    
    init(date: String, shiftTotal: String, cashTip: String, chargeTip: String, tipOut: String, tipTotal: String, position: String, hourlyRate: String, Hours: String, totalHourlyWage: String!, clockIn: String, clockOut: String, shiftNotes: String) {
        
        id = UUID().uuidString
        self.date = date
        self.shiftTotal = shiftTotal
        self.cashTip = cashTip
        self.chargeTip = chargeTip
        self.tipOut = tipOut
        self.tipTotal = tipTotal
        self.position = position
        self.hourlyRate = hourlyRate
        self.Hours = Hours
        self.totalHourlyWage = totalHourlyWage
        self.clockIn = clockIn
        self.clockOut = clockOut
        self.shiftNotes = shiftNotes
        
        
    }
    
}

