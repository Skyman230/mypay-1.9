//
//  DeductionFunctions.swift
//  myPay
//
//  Created by Donald Scott on 6/5/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

class DeductionFunction {
    
    static func createDeductions(at jobIndex: Int, using voluntaryDeductions: DeductionModel) {
        
        Data.Jobs[jobIndex].voluntaryDeductions.append(voluntaryDeductions)
    }
    
    static func updateDeduction(at index: Int, deduction: String, amount: String) {
        Data.Jobs[GlobalVariable.jobIndex].voluntaryDeductions[index].Name = deduction
        Data.Jobs[GlobalVariable.jobIndex].voluntaryDeductions[index].Amount = amount
    }
}
