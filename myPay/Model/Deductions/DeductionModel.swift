//
//  DeductionModel.swift
//  myPay
//
//  Created by Donald Scott on 6/5/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

struct DeductionModel {
    var id: String!
    var Name = ""
    var Amount = ""
    
    init(Name: String, Amount: String) {
        id = UUID.init().uuidString
        self.Name = Name
        self.Amount = Amount
    }
}
