//
//  PositionModel.swift
//  myPay
//
//  Created by Donald Scott on 5/12/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

struct PositionModel {
    var id: String!
    var position = ""
    var hourlyRate = ""
    
    init(position: String, hourlyRate: String) {
        id = UUID.init().uuidString
        self.position = position
        self.hourlyRate = hourlyRate
    }
}

