//
//  PositionFunctions.swift
//  myPay
//
//  Created by Donald Scott on 5/12/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

class PositionFunctions {
    
    static func createPositions(at jobIndex: Int, using positionModel: PositionModel){
        
        Data.Jobs[jobIndex].positionModel.append(positionModel)
    }
    
    static func updatePosition(at index: Int, position: String, rate: String) {
        Data.Jobs[GlobalVariable.jobIndex].positionModel[index].position = position
        Data.Jobs[GlobalVariable.jobIndex].positionModel[index].hourlyRate = rate
    }
    
}

