//
//  PayPeriodModel.swift
//  myPay
//
//  Created by Donald Scott on 5/13/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

struct PayPeriodModel {
    var id: String!
    var weeklyModel = [ShiftModel]()
    var biWeeklyModel = [ShiftModel]()
    var semiMonthlyModel = [ShiftModel]()
    var monthlyModel = [ShiftModel]()
    var grossTip = [String?]()
    var grossWage = [String?]()
    var totalHours = [String?]()
    
    init(data1: [ShiftModel]? = nil, data2: [ShiftModel]? = nil, data3: [ShiftModel]? = nil, data4: [ShiftModel]? = nil, data5: [String]? = nil, data6: [String]? = nil, data7: [String]? = nil) {
        
        id = UUID().uuidString
        if let data1 = data1 {
            self.weeklyModel = data1
        }
        if let data2 = data2 {
            self.biWeeklyModel = data2
        }
        if let data3 = data3 {
            self.semiMonthlyModel = data3
        }
        if let data4 = data4 {
            self.monthlyModel = data4
        }
        if let  data5 = data5 {
            self.grossTip = data5
        }
        if let data6 = data6 {
            self.grossWage = data6
        }
        if let data7 = data7 {
            self.totalHours = data7 
        }
    }
}
