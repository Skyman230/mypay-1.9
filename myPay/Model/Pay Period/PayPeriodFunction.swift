//
//  PayPeriodFunction.swift
//  myPay
//
//  Created by Donald Scott on 5/14/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import Foundation

class PayPeriodFunctions {
    
    static func createWeeklyPayPeriod(at jobIndex: Int, using weeklyModel: PayPeriodModel) {
        Data.Jobs[jobIndex].payPeriodModel.append(weeklyModel)
    }
    
    static func createBiWeeklyPayPeriod(at jobIndex: Int, using biWeeklyModel: PayPeriodModel) {
        Data.Jobs[jobIndex].payPeriodModel.append(biWeeklyModel)
    }
    
    static func createSemiMonthlyPayPeriod(at jobIndex: Int, using semiMonthlyModel: PayPeriodModel) {
        Data.Jobs[jobIndex].payPeriodModel.append(semiMonthlyModel)
    }
    
    static func createMonthlyPayPeriod(at jobIndex: Int, using monthlyModel: ShiftModel) {
        Data.Jobs[jobIndex].monthlyModel.append(monthlyModel)
    }
    
}
