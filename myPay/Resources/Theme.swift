//
//  Theme.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class Theme {
    
    static let mainFontName = "Arial"
    static let arrowFont = "Mishafi Regular"
    static let Warning = UIColor(named: "Accent3")
    static let Accent = UIColor(named: "Accent1")
    static let Buttons = UIColor(named: "Button")
    static let Accent3 = UIColor(named: "Accent2")
    
    
}

