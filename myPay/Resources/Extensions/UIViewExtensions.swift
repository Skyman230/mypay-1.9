//
//  UIViewExtensions.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

extension UIView {
    func addOutline() {
        
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.3333011568, green: 0.3333539069, blue: 0.3332896829, alpha: 1)
        
    }
    
    func addWhiteOutline() {
        
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    func addLtGrayOutline() {
        
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        
    }
    
    func addAccentColorOutline() {
        
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 1, green: 0.6156862745, blue: 0.231372549, alpha: 1)
        
    }
    
    func addDkGrayOutline() {
        
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.3877506589, green: 0.3877506589, blue: 0.3877506589, alpha: 1)
        
    }
    
}
