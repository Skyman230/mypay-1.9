//
//  UITableViewControllerExtensions.swift
//  myPay
//
//  Created by Donald Scott on 5/12/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

extension UITableViewController {
    
    func updateJob(){
        JobsFunctions.updateJob(at: GlobalVariable.jobIndex, playInfo: GlobalVariable.playInfo, currentShiftDate: GlobalVariable.currentShiftDate!, jobsArrayPopulated: GlobalVariable.jobsArrayPopulated, shiftTipsAdded: GlobalVariable.shiftTipsAdded, isTipsEditing: GlobalVariable.isTipsEditing, jobPositionArrayIsPopulated: GlobalVariable.jobPositionArrayIsPopulated, PayPeriodTips: GlobalVariable.PayPeriodTips!, PayPeriodWage: GlobalVariable.PayPeriodWage!, PayPeriodHours: GlobalVariable.PayPeriodHours!, MonthTips: GlobalVariable.MonthTips!, MonthWage: GlobalVariable.MonthWage!, MonthHour: GlobalVariable.MonthHour!, GrossWage: GlobalVariable.GrossWage!, TotalDeductions: GlobalVariable.totalDeductions!, TotalWithholdings: GlobalVariable.totalWithholdings!, TotalNet: GlobalVariable.totalNet!, PayPeriodLengthSet: GlobalVariable.PayPeriodLengthSet, PayPeriodLength: GlobalVariable.PayPeriodLength, startOfPayPeriodSet: GlobalVariable.startOfPayPeriodSet, stateOfResidenceSet: GlobalVariable.stateOfResidenceSet, numberOfAllowancesSet: GlobalVariable.numberOfAllowancesSet, filingStatusSet: GlobalVariable.filingStatusSet, stateOfResidence: GlobalVariable.stateOfResidence!, startOfPayPeriod: GlobalVariable.startOfPayPeriod, EndOfPayPeriod: GlobalVariable.EndOfPayPeriod, StartOfWeekDate: GlobalVariable.StartOfWeekDate, newEndTime: GlobalVariable.newEndTime, clockInTime: GlobalVariable.clockInTime, filingStatus: GlobalVariable.filingStatus, allowances: GlobalVariable.allowances, thisShiftPosition: GlobalVariable.thisShiftPosition!, thisShiftRate: GlobalVariable.thisShiftRate!, thisShiftClockIn: GlobalVariable.thisShiftClockIn ?? "", thisShiftClockOut: GlobalVariable.thisShiftClockOut!, thisShiftHours: GlobalVariable.thisShiftHours ?? "00.00", thisShiftChargeTip: GlobalVariable.thisShiftChargeTip ?? "$000.00", thisShiftCashTip: GlobalVariable.thisShiftCashTip ?? "$000.00", thisShiftTipOut: GlobalVariable.thisShiftTipOut ?? "$000.00", thisShiftTotal: GlobalVariable.thisShiftTotal!, thisShiftsHourlyWage: GlobalVariable.thisShiftsHourlyWage!, thisShiftsTipTotal: GlobalVariable.thisShiftsTipTotal!, ShiftTipsAdded: GlobalVariable.ShiftTipsAdded!, clearSemiMonthlyArray: GlobalVariable.clearSemiMonthlyArray, clearBiWeeklyArray: GlobalVariable.clearBiWeeklyArray, clearWeeklyArray: GlobalVariable.clearWeeklyArray, firstPayPeriodAdded: GlobalVariable.firstPayPeriodAdded, SemiMonthly: GlobalVariable.SemiMonthly, WhichBiWeeklyPayPeriod: GlobalVariable.WhichBiWeeklyPayPeriod, Weekly: GlobalVariable.Weekly, startOfMonth: GlobalVariable.StartOfMonth, clearMonthlyTotals: GlobalVariable.clearMonthlyTotals, monthlyTotals: GlobalVariable.monthlyTotals, monthlyPayPeriodTotals: GlobalVariable.monthlyPayPeriodTotals, federalIncomeTax: GlobalVariable.federalIncomeTax!, stateIncomeTax: GlobalVariable.stateIncomeTax!, mediCare: GlobalVariable.mediCare!, SocialSecurity: GlobalVariable.SocialSecurity!, netGross: GlobalVariable.netGross, netDeductions: GlobalVariable.netDeductions, netWithholdings: GlobalVariable.netWithholdings, CurrentMonthEditing: GlobalVariable.CurrentMonthEditing, dateString: GlobalVariable.dateString, revisedEndTime: GlobalVariable.revisedEndTime, thisShiftNotes: GlobalVariable.thisShiftNotes!, ShiftNotesAdded: GlobalVariable.ShiftNotesAdded, isShiftNotesEditing: GlobalVariable.isShiftNotesEditing, Overtime: GlobalVariable.Overtime, ArchiveMonthTips: GlobalVariable.ArchiveMonthTips ?? "$000.00", ArchiveMonthWage: GlobalVariable.ArchiveMonthWage ?? "$000.00", ArchiveMonthHour: GlobalVariable.ArchiveMonthHour ?? "00.00", ArchiveMonthGrossWage: GlobalVariable.ArchiveMonthGrossWage ?? "$000.00", yearlyTotal: GlobalVariable.yearlyTotal , yearlyTips: GlobalVariable.yearlyTips, yearlyWage: GlobalVariable.AprilWages, retirement401KPercentage: GlobalVariable.retirement401KPercentage ?? "0.00", retirement401KAmount: GlobalVariable.retirement401KAmount ?? "$000.00", retirement401KRothPercentage: GlobalVariable.retirement401KRothPercentage ?? "0.00", retirement401KRothAmount: GlobalVariable.retirement401KRothAmount ?? "$000.00", deductionPercentage: GlobalVariable.deductionPercentage ?? "0.00", totalRetirementSum: GlobalVariable.totalRetirementSum ?? "$000.00", totalInsuranceSum: GlobalVariable.totalInsuranceSum ?? "$000.00", totalAdditionalDeductionSum: GlobalVariable.totalAdditionalDeductionSum ?? "$000.00", medicalInsurance: GlobalVariable.medicalInsurance ?? "$000.00", medicalSavingsAccount: GlobalVariable.medicalSavingsAccount ?? "$000.00", dentalInsurance: GlobalVariable.dentalInsurance ?? "$000.00", visionInsurance: GlobalVariable.visionInsurance ?? "$000.00", retirement401Kadded: GlobalVariable.retirement401Kadded , retirement401KRothadded: GlobalVariable.retirement401KRothadded , voluntaryDeductionArrayIsPopulated: GlobalVariable.voluntaryDeductionArrayIsPopulated , DeductionArrayIsPopulated: GlobalVariable.DeductionArrayIsPopulated , positionHeight: GlobalVariable.positionHeight , taxesHeight: GlobalVariable.taxesHeight, positionNum: GlobalVariable.positionNum, retirementHeight: GlobalVariable.retirementHeight, insuranceEditing: GlobalVariable.insuranceEditing, deductionHeight: GlobalVariable.deductionHeight)
    }
    
    func payPeriod(_ shiftDate: String?, _ Total: String, _ CashTip: String?, _ ChargeTip: String?, _ TipOut: String?, _ TipTotal: String?, _ Position: String, _ PayRate: String, _ Hours: String, _ Wage: String?, _ StartTime: String, _ EndTime: String, _ ShiftNotes: String) {
        payPeriodArray.append(ShiftModel(date: shiftDate!, shiftTotal: Total, cashTip: CashTip!, chargeTip: ChargeTip!, tipOut: TipOut!, tipTotal: TipTotal ?? "$000.00", position: Position, hourlyRate: PayRate, Hours: Hours, totalHourlyWage: Wage, clockIn: StartTime, clockOut: EndTime, shiftNotes: ShiftNotes))
    }
    
    func populatePayPeriod(Duration: Int) {
        
        let jobIndex = GlobalVariable.jobIndex
        let shiftDate = GlobalVariable.currentShiftDate
        let Total = thisShiftTotal ?? "$000.00"
        let CashTip = GlobalVariable.thisShiftCashTip
        let ChargeTip = GlobalVariable.thisShiftChargeTip
        let TipOut = GlobalVariable.thisShiftTipOut
        let TipTotal = thisShiftsTipTotal
        let Position = thisShiftPosition ?? ""
        let PayRate = thisShiftRate ?? "$000.00"
        let Hours = thisShiftHours ?? "00:00"
        let Wage = GlobalVariable.thisShiftsHourlyWage
        let StartTime = clockInTime
        let EndTime = clockOutTime
        let ShiftNotes = thisShiftNotes
        
        switch GlobalVariable.PayPeriodLength {
        case 1:
            let midMonth = Date().midMonth()
            if midMonth.compare(Date()) == .orderedDescending {
                GlobalVariable.monthlyPayPeriodTotals = 1
            }
            else if midMonth.compare(Date()) == .orderedAscending {
                GlobalVariable.monthlyPayPeriodTotals = 2
                GlobalVariable.clearMonthlyPayPeriodTotals = true
            }
            switch GlobalVariable.monthlyPayPeriodTotals {
            case 1:
                if GlobalVariable.clearMonthlyPayPeriodTotals == true {
                    monthlyPayPeriodArray.removeAll()
                    payPeriodArray.removeAll()
                    payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                    monthlyPayPeriodArray = payPeriodArray
                    GlobalVariable.clearMonthlyPayPeriodTotals = false
                } else {
                    payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                    monthlyPayPeriodArray = payPeriodArray
                }
            case 2:
                payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                monthlyPayPeriodArray = payPeriodArray
            default:
                break
            }
            JobsFunctions.populateMonthlyPayPeriod(at: jobIndex)
        case 2:
            let comp: DateComponents = Calendar.current.dateComponents([.year, .month], from: date)
            let startOfMonth = Calendar.current.date(from: comp)!
            
            let startDate = startOfMonth
            let midPayPeriod = (startDate.addingTimeInterval(14 * 24 * 60 * 60))
            
            if startDate.compare(Date()) == .orderedAscending {
                GlobalVariable.SemiMonthly = 1
            }
            if midPayPeriod.compare(Date()) == .orderedAscending {
                GlobalVariable.SemiMonthly = 2
                GlobalVariable.clearSemiMonthlyArray = true
            }
            switch GlobalVariable.SemiMonthly {
            case 1:
                if GlobalVariable.clearSemiMonthlyArray == true {
                    ArchiveArray = semiMonthlyPayPeriod1Array
                    //                    archivePayPeriods(PayPer: addToPayPeriod)
                    ArchiveArray = semiMonthlyPayPeriod2Array
                    //                    archivePayPeriods(PayPer: addToPayPeriod)
                    MonthlyArchiveArray = semiMonthlyPayPeriod1Array + semiMonthlyPayPeriod2Array
                    //                    archiveMonths()
                    semiMonthlyPayPeriod1Array.removeAll()
                    JobsFunctions.populateSemiMonthlyPayPeriod1(at: jobIndex)
                    semiMonthlyPayPeriod2Array.removeAll()
                    payPeriodArray.removeAll()
                    JobsFunctions.populateSemiMonthlyPayPeriod2(at: jobIndex)
                    payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                    print(payPeriodArray)
                    semiMonthlyPayPeriod1Array = payPeriodArray
                    JobsFunctions.populateSemiMonthlyPayPeriod1(at: jobIndex)
                    GlobalVariable.clearSemiMonthlyArray = false
                } else {
                    payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                    print(payPeriodArray)
                    semiMonthlyPayPeriod1Array = payPeriodArray
                    JobsFunctions.populateSemiMonthlyPayPeriod1(at: jobIndex)
                }
            case 2:
                payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                print(payPeriodArray)
                semiMonthlyPayPeriod2Array = payPeriodArray
                JobsFunctions.populateSemiMonthlyPayPeriod2(at: jobIndex)
                if GlobalVariable.firstPayPeriodAdded == false {
                    ArchiveArray = semiMonthlyPayPeriod1Array
                    //                    archivePayPeriods(PayPer: addToPayPeriod)
                    GlobalVariable.firstPayPeriodAdded = true
                }
            default:
                break
                
            }
        case 3:
            var startDate = GlobalVariable.StartOfPayPeriod
            let midDate = (startDate.addingTimeInterval(7 * 24 * 60 * 60))
            let endDate = (midDate.addingTimeInterval(6 * 24 * 60 * 60))
            if startDate.compare(Date()) == .orderedAscending {
                GlobalVariable.WhichBiWeeklyPayPeriod = 1
            }
            if midDate.compare(Date()) == .orderedAscending {
                GlobalVariable.WhichBiWeeklyPayPeriod = 2
                GlobalVariable.clearBiWeeklyArray = true
            }
            if endDate.compare(Date()) == .orderedAscending {
                GlobalVariable.WhichBiWeeklyPayPeriod = 1
            }
            switch GlobalVariable.WhichBiWeeklyPayPeriod {
            case 1:
                if GlobalVariable.clearBiWeeklyArray == true {
                    BiWeeklyPayPeriodWeek01Array.removeAll()
                    BiWeeklyPayPeriodWeek02Array.removeAll()
                    payPeriodArray.removeAll()
                    startDate = (endDate.addingTimeInterval(1 * 24 * 60 * 60))
                    GlobalVariable.clearBiWeeklyArray = false
                    payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                    print(payPeriodArray)
                    BiWeeklyPayPeriodWeek01Array = payPeriodArray
                    JobsFunctions.populateBiWeeklyPayPeriod1(at: jobIndex)
                    MonthlyArchiveArray = BiWeeklyPayPeriodWeek01Array
                } else {
                    payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                    print(payPeriodArray)
                    BiWeeklyPayPeriodWeek01Array = payPeriodArray
                    JobsFunctions.populateBiWeeklyPayPeriod1(at: jobIndex)
                    MonthlyArchiveArray = BiWeeklyPayPeriodWeek01Array
                }
            case 2:
                payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                print(payPeriodArray)
                BiWeeklyPayPeriodWeek02Array = payPeriodArray
                JobsFunctions.populateBiWeeklyPayPeriod2(at: jobIndex)
            default:
                break
            }
        case 4:
            //            let startOfWeek = Date().startOfWeek
            let midWeek = Date().midWeek
            let endOfWeek = Date().endOfWeek
            if midWeek!.compare(Date()) == .orderedDescending {
                GlobalVariable.Weekly = 1
            }
            if midWeek!.compare(Date()) == .orderedAscending {
                GlobalVariable.Weekly = 2
                GlobalVariable.clearWeeklyArray = true
            }
            if endOfWeek!.compare(Date()) == .orderedAscending {
                GlobalVariable.Weekly = 1
            }
            switch GlobalVariable.Weekly {
            case 1:
                if GlobalVariable.clearWeeklyArray == true {
                    WeeklyPayPeriodArray.removeAll()
                    payPeriodArray.removeAll()
                    payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                    print(payPeriodArray)
                    WeeklyPayPeriodArray = payPeriodArray
                    JobsFunctions.populateWeeklyPayPeriod2(at: jobIndex)
                    GlobalVariable.clearWeeklyArray = false
                } else {
                    payPeriodArray.append(ShiftModel(date: shiftDate!, shiftTotal: Total, cashTip: CashTip!, chargeTip: ChargeTip!, tipOut: TipOut!, tipTotal: TipTotal!, position: Position, hourlyRate: PayRate, Hours: Hours, totalHourlyWage: Wage, clockIn: StartTime, clockOut: EndTime, shiftNotes: ShiftNotes))
                    print(payPeriodArray)
                    WeeklyPayPeriodArray = payPeriodArray
                    JobsFunctions.populateWeeklyPayPeriod2(at: jobIndex)
                }
            case 2:
                payPeriod(shiftDate, Total, CashTip, ChargeTip, TipOut, TipTotal, Position, PayRate, Hours, Wage, StartTime, EndTime, ShiftNotes)
                print(payPeriodArray)
                WeeklyPayPeriodArray = payPeriodArray
                JobsFunctions.populateWeeklyPayPeriod2(at: jobIndex)
            default:
                break
            }
        default:
            break
        }
        payPeriodGrossTipArray = payPeriodArray.map { $0.tipTotal}
        payPeriodGrossWageArray = payPeriodArray.map { $0.totalHourlyWage}
        payPeriodTotalHoursArray = payPeriodArray.map { $0.Hours}
        JobsFunctions.populatePayPeriodTotals(at: jobIndex)
        monthlyTotals()
    }
    
    func monthlyTotals() {
        let jobIndex = GlobalVariable.jobIndex
        let shiftDate = GlobalVariable.currentShiftDate
        let Total = thisShiftTotal ?? "$000.00"
        let CashTip = GlobalVariable.thisShiftCashTip
        let ChargeTip = GlobalVariable.thisShiftChargeTip
        let TipOut = GlobalVariable.thisShiftTipOut
        let TipTotal = thisShiftsTipTotal
        let Position = thisShiftPosition ?? ""
        let PayRate = thisShiftRate ?? "$000.00"
        let Hours = thisShiftHours ?? "00:00"
        let Wage = GlobalVariable.thisShiftsHourlyWage
        let StartTime = clockInTime
        let EndTime = clockOutTime
        let ShiftNotes = thisShiftNotes
        //        let startOfMonth = Date().startOfMonth()
        let midMonth = Date().midMonth()
        let endOfMonth = Date().endOfMonth()
        if midMonth.compare(Date()) == .orderedDescending {
            GlobalVariable.monthlyTotals = 1
        }
        else if midMonth.compare(Date()) == .orderedAscending {
            GlobalVariable.monthlyTotals = 2
            GlobalVariable.clearMonthlyTotals = true
        }
        else if endOfMonth.compare(Date()) == .orderedAscending {
            GlobalVariable.monthlyTotals = 1
        }
        switch GlobalVariable.monthlyTotals {
        case 1:
            if GlobalVariable.clearMonthlyTotals == true {
                ArchiveArray.removeAll()
                monthlyTotalArray.removeAll()
                monthlyTotalArray.append(ShiftModel(date: shiftDate!, shiftTotal: Total, cashTip: CashTip!, chargeTip: ChargeTip!, tipOut: TipOut!, tipTotal: TipTotal ?? "$000.00", position: Position, hourlyRate: PayRate, Hours: Hours, totalHourlyWage: Wage, clockIn: StartTime, clockOut: EndTime, shiftNotes: ShiftNotes))
                ArchiveArray.append(ShiftModel(date: shiftDate!, shiftTotal: Total, cashTip: CashTip!, chargeTip: ChargeTip!, tipOut: TipOut!, tipTotal: TipTotal ?? "$000.00", position: Position, hourlyRate: PayRate, Hours: Hours, totalHourlyWage: Wage, clockIn: StartTime, clockOut: EndTime, shiftNotes: ShiftNotes))
                
                print("monthlyTotalArray1", monthlyTotalArray)
                GlobalVariable.clearMonthlyTotals = false
            } else {
                monthlyTotalArray.append(ShiftModel(date: shiftDate!, shiftTotal: Total, cashTip: CashTip!, chargeTip: ChargeTip!, tipOut: TipOut!, tipTotal: TipTotal ?? "$000.00", position: Position, hourlyRate: PayRate, Hours: Hours, totalHourlyWage: Wage, clockIn: StartTime, clockOut: EndTime, shiftNotes: ShiftNotes))
                ArchiveArray.append(ShiftModel(date: shiftDate!, shiftTotal: Total, cashTip: CashTip!, chargeTip: ChargeTip!, tipOut: TipOut!, tipTotal: TipTotal ?? "$000.00", position: Position, hourlyRate: PayRate, Hours: Hours, totalHourlyWage: Wage, clockIn: StartTime, clockOut: EndTime, shiftNotes: ShiftNotes))
                print("monthlyTotalArray1", monthlyTotalArray)
            }
        case 2:
            monthlyTotalArray.append(ShiftModel(date: shiftDate!, shiftTotal: Total, cashTip: CashTip!, chargeTip: ChargeTip!, tipOut: TipOut!, tipTotal: TipTotal ?? "$000.00", position: Position, hourlyRate: PayRate, Hours: Hours, totalHourlyWage: Wage, clockIn: StartTime, clockOut: EndTime, shiftNotes: ShiftNotes))
            ArchiveArray.append(ShiftModel(date: shiftDate!, shiftTotal: Total, cashTip: CashTip!, chargeTip: ChargeTip!, tipOut: TipOut!, tipTotal: TipTotal ?? "$000.00", position: Position, hourlyRate: PayRate, Hours: Hours, totalHourlyWage: Wage, clockIn: StartTime, clockOut: EndTime, shiftNotes: ShiftNotes))
            print("monthlyTotalArray2", monthlyTotalArray)
        default:
            break
        }
        thisMonthsGrossTipArray = monthlyTotalArray.map {$0.tipTotal}
        thisMonthsGrossWageArray = monthlyTotalArray.map {$0.totalHourlyWage}
        thisMonthsTotalHoursArray = monthlyTotalArray.map {$0.Hours}
        JobsFunctions.populateThisMonthsTotals(at: jobIndex)
        CalculateSums.showFigures()
        archiveMonths()
        print(GlobalVariable.monthlyTotals)
        print("thisMonthsGrossTipArray", thisMonthsGrossTipArray)
        print("thisMonthsGrossWageArray", thisMonthsGrossWageArray)
        print("thisMonthsTotalHoursArray", thisMonthsTotalHoursArray)
        GlobalVariable.isTipsEditing = false
        dismiss(animated: true)
    }
    
    
    func archiveMonths() {
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "L"
        let nameOfMonth = dateFormatter.string(from: now)
        let monthNum = Int(nameOfMonth)
        switch monthNum {
        case 1:
            JanuaryArray = ArchiveArray
            print("January Array", JanuaryArray)
        case 2:
            FebruaryArray = ArchiveArray
            print("February Array", FebruaryArray)
        case 3:
            MarchArray = ArchiveArray
            print("March Array", MarchArray)
        case 4:
            AprilArray = ArchiveArray
            print("April Array", AprilArray)
        case 5:
            MayArray = ArchiveArray
            print("May Array", MayArray)
        case 6:
            JuneArray = ArchiveArray
            print("June Array", JuneArray)
        case 7:
            JulyArray = ArchiveArray
            print("July Array", JulyArray)
        case 8:
            AugustArray = ArchiveArray
            print("August Array", AugustArray)
        case 9:
            SeptemberArray = ArchiveArray
            print("September Array", SeptemberArray)
        case 10:
            OctoberArray = ArchiveArray
            print("October Array", OctoberArray)
        case 11:
            NovemberArray = ArchiveArray
            print("November Array", NovemberArray)
        case 12:
            DecemberArray = ArchiveArray
            print("December Array", DecemberArray)
        default:
            break
        }
    }
    
}
