//
//  UIViewControllerExtensions.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit


extension UIViewController {
    
    func convertCurrencyToDouble(input: String) -> Double? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale.current
        
        return numberFormatter.number(from: input)?.doubleValue
    }
    func convertDoubleToCurrency(amount: Double) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale.current
        
        return numberFormatter.string(from: NSNumber(value: amount))!
    }
    
    
    func updateJobView() {
        JobsFunctions.updateJob(at: GlobalVariable.jobIndex, playInfo: GlobalVariable.playInfo, currentShiftDate: GlobalVariable.currentShiftDate!, jobsArrayPopulated: GlobalVariable.jobsArrayPopulated, shiftTipsAdded: GlobalVariable.shiftTipsAdded, isTipsEditing: GlobalVariable.isTipsEditing, jobPositionArrayIsPopulated: GlobalVariable.jobPositionArrayIsPopulated, PayPeriodTips: GlobalVariable.PayPeriodTips!, PayPeriodWage: GlobalVariable.PayPeriodWage!, PayPeriodHours: GlobalVariable.PayPeriodHours!, MonthTips: GlobalVariable.MonthTips!, MonthWage: GlobalVariable.MonthWage!, MonthHour: GlobalVariable.MonthHour!, GrossWage: GlobalVariable.GrossWage!, TotalDeductions: GlobalVariable.totalDeductions!, TotalWithholdings: GlobalVariable.totalWithholdings!, TotalNet: GlobalVariable.totalNet!, PayPeriodLengthSet: GlobalVariable.PayPeriodLengthSet, PayPeriodLength: GlobalVariable.PayPeriodLength, startOfPayPeriodSet: GlobalVariable.startOfPayPeriodSet, stateOfResidenceSet: GlobalVariable.stateOfResidenceSet, numberOfAllowancesSet: GlobalVariable.numberOfAllowancesSet, filingStatusSet: GlobalVariable.filingStatusSet, stateOfResidence: GlobalVariable.stateOfResidence!, startOfPayPeriod: GlobalVariable.startOfPayPeriod, EndOfPayPeriod: GlobalVariable.EndOfPayPeriod, StartOfWeekDate: GlobalVariable.StartOfWeekDate, newEndTime: GlobalVariable.newEndTime, clockInTime: GlobalVariable.clockInTime, filingStatus: GlobalVariable.filingStatus, allowances: GlobalVariable.allowances, thisShiftPosition: GlobalVariable.thisShiftPosition!, thisShiftRate: GlobalVariable.thisShiftRate ?? "$000.00", thisShiftClockIn: GlobalVariable.thisShiftClockIn ?? "", thisShiftClockOut: GlobalVariable.thisShiftClockOut ?? "", thisShiftHours: GlobalVariable.thisShiftHours ?? "00.00", thisShiftChargeTip: GlobalVariable.thisShiftChargeTip ?? "$000.00", thisShiftCashTip: GlobalVariable.thisShiftCashTip ?? "$000.00", thisShiftTipOut: GlobalVariable.thisShiftTipOut ?? "$000.00", thisShiftTotal: GlobalVariable.thisShiftTotal!, thisShiftsHourlyWage: GlobalVariable.thisShiftsHourlyWage!, thisShiftsTipTotal: GlobalVariable.thisShiftsTipTotal!, ShiftTipsAdded: GlobalVariable.ShiftTipsAdded!, clearSemiMonthlyArray: GlobalVariable.clearSemiMonthlyArray, clearBiWeeklyArray: GlobalVariable.clearBiWeeklyArray, clearWeeklyArray: GlobalVariable.clearWeeklyArray, firstPayPeriodAdded: GlobalVariable.firstPayPeriodAdded, SemiMonthly: GlobalVariable.SemiMonthly, WhichBiWeeklyPayPeriod: GlobalVariable.WhichBiWeeklyPayPeriod, Weekly: GlobalVariable.Weekly, startOfMonth: GlobalVariable.StartOfMonth, clearMonthlyTotals: GlobalVariable.clearMonthlyTotals, monthlyTotals: GlobalVariable.monthlyTotals, monthlyPayPeriodTotals: GlobalVariable.monthlyPayPeriodTotals, federalIncomeTax: GlobalVariable.federalIncomeTax!, stateIncomeTax: GlobalVariable.stateIncomeTax!, mediCare: GlobalVariable.mediCare!, SocialSecurity: GlobalVariable.SocialSecurity!, netGross: GlobalVariable.netGross, netDeductions: GlobalVariable.netDeductions, netWithholdings: GlobalVariable.netWithholdings, CurrentMonthEditing: GlobalVariable.CurrentMonthEditing, dateString: GlobalVariable.dateString, revisedEndTime: GlobalVariable.revisedEndTime, thisShiftNotes: GlobalVariable.thisShiftNotes!, ShiftNotesAdded: GlobalVariable.ShiftNotesAdded, isShiftNotesEditing: GlobalVariable.isShiftNotesEditing, Overtime: GlobalVariable.Overtime, ArchiveMonthTips: GlobalVariable.ArchiveMonthTips ?? "$000.00", ArchiveMonthWage: GlobalVariable.ArchiveMonthWage ?? "$000.00", ArchiveMonthHour: GlobalVariable.ArchiveMonthHour ?? "00.00", ArchiveMonthGrossWage: GlobalVariable.ArchiveMonthGrossWage ?? "$000.00", yearlyTotal: GlobalVariable.yearlyTotal , yearlyTips: GlobalVariable.yearlyTips, yearlyWage: GlobalVariable.AprilWages, retirement401KPercentage: GlobalVariable.retirement401KPercentage ?? "0.00", retirement401KAmount: GlobalVariable.retirement401KAmount ?? "$000.00", retirement401KRothPercentage: GlobalVariable.retirement401KRothPercentage ?? "0.00", retirement401KRothAmount: GlobalVariable.retirement401KRothAmount ?? "$000.00", deductionPercentage: GlobalVariable.deductionPercentage ?? "0.00", totalRetirementSum: GlobalVariable.totalRetirementSum ?? "$000.00", totalInsuranceSum: GlobalVariable.totalInsuranceSum ?? "$000.00", totalAdditionalDeductionSum: GlobalVariable.totalAdditionalDeductionSum ?? "$000.00", medicalInsurance: GlobalVariable.medicalInsurance ?? "$000.00", medicalSavingsAccount: GlobalVariable.medicalSavingsAccount ?? "$000.00", dentalInsurance: GlobalVariable.dentalInsurance ?? "$000.00", visionInsurance: GlobalVariable.visionInsurance ?? "$000.00", retirement401Kadded: GlobalVariable.retirement401Kadded , retirement401KRothadded: GlobalVariable.retirement401KRothadded , voluntaryDeductionArrayIsPopulated: GlobalVariable.voluntaryDeductionArrayIsPopulated , DeductionArrayIsPopulated: GlobalVariable.DeductionArrayIsPopulated , positionHeight: GlobalVariable.positionHeight , taxesHeight: GlobalVariable.taxesHeight, positionNum: GlobalVariable.positionNum, retirementHeight: GlobalVariable.retirementHeight, insuranceEditing: GlobalVariable.insuranceEditing, deductionHeight: GlobalVariable.deductionHeight)
    }
    
}
