//
//  whiteLine_Top.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class whiteLine_Top: UIView {
    
    var line = UIBezierPath()
    func Line() {
        line.move(to: .init(x: 0, y: 0))
        line.addLine(to: .init(x: bounds.width, y: 0))
        UIColor.white.setStroke()
        line.lineWidth = 2
        line.stroke()
    }
    override func draw(_ rect: CGRect) {
        Line()
    }
}
