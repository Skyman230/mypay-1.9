//
//  gray_SubLine.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class gray_SubLine: UIView {
    
    var line = UIBezierPath()
    func Line() {
        line.move(to: .init(x: 0, y: 22))
        line.addLine(to: .init(x: bounds.width, y: 22))
        UIColor.lightGray.setStroke()
        line.lineWidth = 0.5
        line.stroke()
    }
    override func draw(_ rect: CGRect) {
        Line()
    }
    
}
