//
//  grayLine_Bottom.swift
//  myPay
//
//  Created by Donald Scott on 5/8/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class grayLine_Bottom: UIView {
    
    var line = UIBezierPath()
    func Line() {
        line.move(to: .init(x: 0, y: bounds.height))
        line.addLine(to: .init(x: bounds.width, y: bounds.height))
        UIColor.lightGray.setStroke()
        line.lineWidth = 1
        line.stroke()
    }
    override func draw(_ rect: CGRect) {
        Line()
    }
    
}
