//
//  Accent1colorLine_Top_Bottom.swift
//  myPay
//
//  Created by Donald Scott on 5/28/19.
//  Copyright © 2019 SWDesign. All rights reserved.
//

import UIKit

class Accent1colorLine_Top_Bottom: UIView {
    var line = UIBezierPath()
    func Line() {
        line.move(to: .init(x: 0, y: 5))
        line.addLine(to: .init(x: bounds.width, y: 5))
        #colorLiteral(red: 1, green: 0.6156862745, blue: 0.231372549, alpha: 1).setStroke()
        line.lineWidth = 1
        line.stroke()
    }
    
    func Line2(){
        line.move(to: .init(x: 0, y: bounds.height - 4))
        line.addLine(to: .init(x: bounds.width, y: bounds.height - 4))
        #colorLiteral(red: 1, green: 0.6156862745, blue: 0.231372549, alpha: 1).setStroke()
        line.lineWidth = 1
        line.stroke()
    }
    override func draw(_ rect: CGRect) {
        Line(); Line2()
    }
}
